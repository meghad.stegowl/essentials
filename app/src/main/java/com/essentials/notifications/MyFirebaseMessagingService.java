package com.essentials.notifications;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.essentials.R;
import com.essentials.activities.AppointmentHistory;
import com.essentials.activities.Chat;
import com.essentials.activities.SplashScreen;
import com.essentials.application.Essentials;
import com.essentials.data.models.chat.MessageHistoryData;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onNewToken(@NotNull String token) {
        Prefs.getPrefInstance().setValue(this, Const.FCM_ID, token);
    }

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d("mytag", "  noti is : " + remoteMessage.getData().toString());

            if (remoteMessage.getData().containsKey("notiType") && remoteMessage.getData().get("notiType").equals("3")) {
                if (Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.LOGIN_ACCESS, "").equals(Essentials.applicationContext.getResources().getString(R.string.logged_in))) {
                    ComponentName topActivity = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1).get(0).topActivity;
                    if (topActivity != null) {
                        String topActivityName = topActivity.getShortClassName();
                        if (topActivityName.equals(".activities.ChatListing")) {
                            if (remoteMessage.getData().containsKey("sender_id") &&
                                    !Objects.equals(remoteMessage.getData().get("sender_id"),
                                            Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.USER_ID, ""))) {
                                MessageHistoryData data = new MessageHistoryData();
                                data.setMId(remoteMessage.getData().get("message_id"));
                                data.setMessage(remoteMessage.getData().get("body"));
                                data.setDate(remoteMessage.getData().get("date"));
                                data.setDisplayDate(remoteMessage.getData().get("date"));
                                data.setDisplayTime(remoteMessage.getData().get("time"));
                                data.setSendUserId(remoteMessage.getData().get("sender_id"));
                                data.setReadStatus(remoteMessage.getData().get("read_status"));
                                com.essentials.notifications.NotificationManager.getInstance().postNotificationName(com.essentials.notifications.NotificationManager.didReceivedNewMessages, data);
                            }
                        } else if (topActivityName.equals(".activities.Chat")) {
                            if (remoteMessage.getData().containsKey("sender_id") && !Objects.equals(remoteMessage.getData().get("sender_id"), Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.USER_ID, ""))) {
                                MessageHistoryData data = new MessageHistoryData();
                                data.setMId(remoteMessage.getData().get("message_id"));
                                data.setMessage(remoteMessage.getData().get("body"));
                                data.setDate(remoteMessage.getData().get("date"));
                                data.setDisplayDate(remoteMessage.getData().get("date"));
                                data.setDisplayTime(remoteMessage.getData().get("time"));
                                data.setSendUserId(remoteMessage.getData().get("sender_id"));
                                data.setReadStatus(remoteMessage.getData().get("read_status"));
                                com.essentials.notifications.NotificationManager.getInstance().postNotificationName(com.essentials.notifications.NotificationManager.didReceivedNewMessages, data);
                            }
                        } else {
                            if (remoteMessage.getData().containsKey("sender_id") && !Objects.equals(remoteMessage.getData().get("sender_id"), Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.USER_ID, ""))) {
                                showChatNotification(remoteMessage.getData().get("sender_name"), remoteMessage.getData().get("body"), null, remoteMessage.getData().get("sender_id"));
                            }
                        }
                    } else {
                        if (remoteMessage.getData().containsKey("sender_id") && !Objects.equals(remoteMessage.getData().get("sender_id"), Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.USER_ID, ""))) {
                            showChatNotification(remoteMessage.getData().get("sender_name"), remoteMessage.getData().get("body"), null, remoteMessage.getData().get("sender_id"));
                        }
                    }
                }
            } else if (remoteMessage.getData().containsKey("notiType") && remoteMessage.getData().get("notiType").equals("2")) {
                if (Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.LOGIN_ACCESS, "").equals(Essentials.applicationContext.getResources().getString(R.string.logged_in))) {
                    ComponentName topActivity = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1)
                            .get(0).topActivity;
                    if (topActivity != null) {
                        String topActivityName = topActivity.getShortClassName();
                        if (topActivityName.equals(".activities.AppointmentHistory")) {
                            String notiType = remoteMessage.getData().get("notiType");
                            String path = "pending";
                            boolean isCustomer = Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1");
                            com.essentials.notifications.NotificationManager.getInstance().postNotificationName
                                    (com.essentials.notifications.NotificationManager.didAppointmentStatusChanged, notiType, path, isCustomer);
                        }
                    }
                    showNotificationAppointment(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), null);
                }
            } else if (remoteMessage.getData().containsKey("notiType") && remoteMessage.getData().get("notiType").equals("4")) {
                if (Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.LOGIN_ACCESS, "").equals(Essentials.applicationContext.getResources().getString(R.string.logged_in))) {
                    ComponentName topActivity = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1).get(0).topActivity;
                    if (topActivity != null) {
                        String topActivityName = topActivity.getShortClassName();
                        if (topActivityName.equals(".activities.AppointmentHistory")) {
                            String notiType = remoteMessage.getData().get("notiType");
                            String path = "canceled";
                            boolean isCustomer = Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1");
                            com.essentials.notifications.NotificationManager.getInstance().postNotificationName(com.essentials.notifications.NotificationManager.didAppointmentCancelled, notiType, path, isCustomer);
                        }
                    }
                    showNotificationCanceled(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), null);
                }
            } else if (remoteMessage.getData().containsKey("notiType") && remoteMessage.getData().get("notiType").equals("5")) {
                if (Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.LOGIN_ACCESS, "").equals(Essentials.applicationContext.getResources().getString(R.string.logged_in))) {
                    ComponentName topActivity = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1).get(0).topActivity;
                    if (topActivity != null) {
                        String topActivityName = topActivity.getShortClassName();
                        if (topActivityName.equals(".activities.AppointmentHistory")) {
                            String notiType = remoteMessage.getData().get("notiType");
                            String path = "completed";
                            boolean isCustomer = Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1");
                            com.essentials.notifications.NotificationManager.getInstance().postNotificationName(com.essentials.notifications.NotificationManager.didAppointmentCompleted, notiType, path, isCustomer);
                        }
                    }
                    showNotificationCompleted(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), null);
                }
            } else if (remoteMessage.getData().containsKey("notiType") && remoteMessage.getData().get("notiType").equals("6")) {
                if (Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.LOGIN_ACCESS, "").equals(Essentials.applicationContext.getResources().getString(R.string.logged_in))) {
                    ComponentName topActivity = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1).get(0).topActivity;
                    if (topActivity != null) {
                        String topActivityName = topActivity.getShortClassName();
                        if (topActivityName.equals(".activities.AppointmentHistory")) {
                            String notiType = remoteMessage.getData().get("notiType");
                            String path = "rateReview";
                            boolean isCustomer = Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1");
                            com.essentials.notifications.NotificationManager.getInstance().postNotificationName(com.essentials.notifications.NotificationManager.didAppointmentReviewed, notiType, path, isCustomer);
                        }
                    }
                    showNotificationRateReview(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), null);
                }
            } else {
                showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), null, remoteMessage.getData());
            }
        }
    }

    private void showNotification(String title, String message, String imageUrl, Map<String, String> data) {
        NotificationCompat.Builder mBuilder;
        String CHANNEL_ID = getString(R.string.app_name);// The id of the channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setShowBadge(false);
            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
            mBuilder.setChannelId(CHANNEL_ID);
            mBuilder.setNumber(0);

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        } else {
            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        }
        Intent resultIntent = new Intent(getApplicationContext(), SplashScreen.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        if (data.containsKey("notiType") && data.get("notiType").equals("1"))
        resultIntent.putExtra("isFromNotification", true);
        resultIntent.putExtra("isFromNotificationValue", "saumil");
//        resultIntent.putExtra("type", type);
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setSmallIcon(R.drawable.logo);
        if (title != null && !title.isEmpty())
            mBuilder.setContentTitle(title);
        else
            mBuilder.setContentTitle(getString(R.string.app_name));
        if (imageUrl != null && !imageUrl.isEmpty()) {
            FutureTarget<Bitmap> ft = Glide.with(this).asBitmap().load(imageUrl).submit();
            try {
                Bitmap bitmap = ft.get();
                mBuilder.setLargeIcon(bitmap);
                mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
            } catch (ExecutionException | InterruptedException e) {
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                e.printStackTrace();
            }
        } else {
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        }
        mBuilder.setContentText(message);
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.notify(1, mBuilder.build());
        }
    }

    private void showChatNotification(String title, String message, String imageUrl, String data) {
        NotificationCompat.Builder mBuilder;
        String CHANNEL_ID = getString(R.string.app_name);// The id of the channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setShowBadge(false);
            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
            mBuilder.setChannelId(CHANNEL_ID);
            mBuilder.setNumber(0);

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        } else {
            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        }
        Intent resultIntent = new Intent(getApplicationContext(), Chat.class);
        Bundle bundle = new Bundle();
        bundle.putString("path", data);
        bundle.putBoolean("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtras(bundle);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.putExtra("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtra("path", data);
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setSmallIcon(R.drawable.logo);
        if (title != null && !title.isEmpty())
            mBuilder.setContentTitle(title);
        else
            mBuilder.setContentTitle(getString(R.string.app_name));
        if (imageUrl != null && !imageUrl.isEmpty()) {
            FutureTarget<Bitmap> ft = Glide.with(this).asBitmap().load(imageUrl).submit();
            try {
                Bitmap bitmap = ft.get();
                mBuilder.setLargeIcon(bitmap);
                mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
            } catch (ExecutionException | InterruptedException e) {
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                e.printStackTrace();
            }
        } else {
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        }
        mBuilder.setContentText(message);
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.notify(1, mBuilder.build());
        }
    }

    private void showNotificationAppointment(String title, String message, String imageUrl) {
        NotificationCompat.Builder mBuilder;
        String CHANNEL_ID = getString(R.string.app_name);// The id of the channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setShowBadge(false);

            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
            mBuilder.setChannelId(CHANNEL_ID);
            mBuilder.setNumber(0);

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        } else {
            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        }
        Intent resultIntent = new Intent(getApplicationContext(), AppointmentHistory.class);
        Bundle bundle = new Bundle();
        bundle.putString("path", "pending");
        bundle.putBoolean("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtras(bundle);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.putExtra("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtra("path", "pending");
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setSmallIcon(R.drawable.logo);
        if (title != null && !title.isEmpty())
            mBuilder.setContentTitle(title);
        else
            mBuilder.setContentTitle(getString(R.string.app_name));
        if (imageUrl != null && !imageUrl.isEmpty()) {
            FutureTarget<Bitmap> ft = Glide.with(this).asBitmap().load(imageUrl).submit();
            try {
                Bitmap bitmap = ft.get();
                mBuilder.setLargeIcon(bitmap);
                mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
            } catch (ExecutionException | InterruptedException e) {
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                e.printStackTrace();
            }
        } else {
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        }
        mBuilder.setContentText(message);
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.notify(1, mBuilder.build());
        }
    }

    private void showNotificationCompleted(String title, String message, String imageUrl) {
        NotificationCompat.Builder mBuilder;
        String CHANNEL_ID = getString(R.string.app_name);// The id of the channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setShowBadge(false);

            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
            mBuilder.setChannelId(CHANNEL_ID);
            mBuilder.setNumber(0);

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        } else {
            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        }
        Intent resultIntent = new Intent(getApplicationContext(), AppointmentHistory.class);
        Bundle bundle = new Bundle();
        bundle.putString("path", "completed");
        bundle.putBoolean("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtras(bundle);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.putExtra("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtra("path", "completed");
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setSmallIcon(R.drawable.logo);
        if (title != null && !title.isEmpty())
            mBuilder.setContentTitle(title);
        else
            mBuilder.setContentTitle(getString(R.string.app_name));
        if (imageUrl != null && !imageUrl.isEmpty()) {
            FutureTarget<Bitmap> ft = Glide.with(this).asBitmap().load(imageUrl).submit();
            try {
                Bitmap bitmap = ft.get();
                mBuilder.setLargeIcon(bitmap);
                mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
            } catch (ExecutionException | InterruptedException e) {
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                e.printStackTrace();
            }
        } else {
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        }
        mBuilder.setContentText(message);
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.notify(1, mBuilder.build());
        }
    }

    private void showNotificationCanceled(String title, String message, String imageUrl) {
        NotificationCompat.Builder mBuilder;
        String CHANNEL_ID = getString(R.string.app_name);// The id of the channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setShowBadge(false);

            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
            mBuilder.setChannelId(CHANNEL_ID);
            mBuilder.setNumber(0);

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        } else {
            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        }
        Intent resultIntent = new Intent(getApplicationContext(), AppointmentHistory.class);
        Bundle bundle = new Bundle();
        bundle.putString("path", "canceled");
        bundle.putBoolean("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtras(bundle);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.putExtra("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtra("path", "canceled");
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setSmallIcon(R.drawable.logo);
        if (title != null && !title.isEmpty())
            mBuilder.setContentTitle(title);
        else
            mBuilder.setContentTitle(getString(R.string.app_name));
        if (imageUrl != null && !imageUrl.isEmpty()) {
            FutureTarget<Bitmap> ft = Glide.with(this).asBitmap().load(imageUrl).submit();
            try {
                Bitmap bitmap = ft.get();
                mBuilder.setLargeIcon(bitmap);
                mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
            } catch (ExecutionException | InterruptedException e) {
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                e.printStackTrace();
            }
        } else {
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        }
        mBuilder.setContentText(message);
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.notify(1, mBuilder.build());
        }
    }

    private void showNotificationRateReview(String title, String message, String imageUrl) {
        NotificationCompat.Builder mBuilder;
        String CHANNEL_ID = getString(R.string.app_name);// The id of the channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setShowBadge(false);

            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
            mBuilder.setChannelId(CHANNEL_ID);
            mBuilder.setNumber(0);

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        } else {
            mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        }
        Intent resultIntent = new Intent(getApplicationContext(), AppointmentHistory.class);
        Bundle bundle = new Bundle();
        bundle.putString("path", "rateReview");
        bundle.putBoolean("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtras(bundle);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        resultIntent.putExtra("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
        resultIntent.putExtra("path", "rateReview");
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setSmallIcon(R.drawable.logo);
        if (title != null && !title.isEmpty())
            mBuilder.setContentTitle(title);
        else
            mBuilder.setContentTitle(getString(R.string.app_name));
        if (imageUrl != null && !imageUrl.isEmpty()) {
            FutureTarget<Bitmap> ft = Glide.with(this).asBitmap().load(imageUrl).submit();
            try {
                Bitmap bitmap = ft.get();
                mBuilder.setLargeIcon(bitmap);
                mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
            } catch (ExecutionException | InterruptedException e) {
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                e.printStackTrace();
            }
        } else {
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        }
        mBuilder.setContentText(message);
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        mBuilder.setContentIntent(intent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.notify(1, mBuilder.build());
        }
    }
}