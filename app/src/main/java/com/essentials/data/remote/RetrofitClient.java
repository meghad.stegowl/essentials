package com.essentials.data.remote;

import com.essentials.BuildConfig;
import com.essentials.util.Const;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

class RetrofitClient {

    private static Retrofit retrofit = null;

    static Retrofit getClient() {
        if (retrofit == null) {

            OkHttpClient.Builder http = new OkHttpClient.Builder();

//            http.addInterceptor(new HeaderInterceptor());
//            http.addInterceptor(new DecryptionInterceptor());

            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor Logging = new HttpLoggingInterceptor();
                Logging.level(HttpLoggingInterceptor.Level.BODY);
                http.addInterceptor(Logging);
            }

            http.connectTimeout(1, TimeUnit.MINUTES);
            http.readTimeout(1, TimeUnit.MINUTES);
            http.writeTimeout(1, TimeUnit.MINUTES);

            retrofit = new Retrofit.Builder()
                    .baseUrl(Const.ESSENTIALS_HOST)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                    .client(http.build())
                    .build();
        }
        return retrofit;
    }

}