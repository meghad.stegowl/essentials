package com.essentials.data.remote;



/*
  Created by Saumil on 1/11/2018.
*/

import com.essentials.data.models.asset.AssetDetailsResponse;
import com.essentials.data.models.category.CategoryResponse;
import com.essentials.data.models.asset.AssetListingResponse;
import com.essentials.data.models.chat.ChatListingResponse;
import com.essentials.data.models.chat.MessageHistoryResponse;
import com.essentials.data.models.chat.SendMessageResponse;
import com.essentials.data.models.common.CommonResponse;
import com.essentials.data.models.notifications.NotificationsResponse;
import com.essentials.data.models.payment.OrderResponse;
import com.essentials.data.models.payment.PaymentResponse;
import com.essentials.data.models.search.RecentSearchResponse;
import com.essentials.data.models.search.SearchResponse;
import com.essentials.data.models.social_media.SocialMediaResponse;
import com.essentials.data.models.location.GEOCoderResponse;
import com.essentials.data.models.user.ProfileResponse;
import com.essentials.data.models.user.UploadImageResponse;
import com.essentials.data.models.intro_slider.IntroSliderResponse;
import com.essentials.data.models.location.LocationResponse;
import com.essentials.data.models.user.LoginUserResponse;
import com.essentials.data.models.webview.WebViewResponse;
import com.essentials.util.Const;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIService {

    @GET(Const.GET_INTRO_SLIDER)
    Call<IntroSliderResponse> get_intro_slider(@Query("type") String type);

    @GET(Const.GET_LOCATION)
    Call<LocationResponse> get_country(@Query("type") String type);

    @GET(Const.GET_LOCATION)
    Call<LocationResponse> get_state(@Query("type") String type, @Query("country_id") int country_id);

    @GET(Const.GET_GEOLOCATION)
    Call<GEOCoderResponse> get_geo_location(@Query("address") String address, @Query("key") String key);

    @GET(Const.GET_LOCATION)
    Call<LocationResponse> get_city(@Query("type") String type, @Query("state_id") int state_id);

    @GET(Const.GET_CATEGORY)
    Call<CategoryResponse> get_category();

    @GET(Const.GET_ASSET_LISTING)
    Call<AssetListingResponse> get_listing(@Query("healthcarecategory_id") String healthcarecategory_id,
                                           @Query("latitude") String latitude, @Query("longitude") String longitude
            , @Query("limit") int limit, @Query("page") int page);

    @GET(Const.GET_ASSET_DETAILS)
    Call<AssetDetailsResponse> get_asset_details(@Query("latitude") String latitude, @Query("longitude") String longitude, @Query("provider_id") String provider_id, @Query("limit") int limit, @Query("page") int page);

    @GET(Const.LOGOUT)
    Call<CommonResponse> call_logout(@Query("user_id") String user_id);

    @POST(Const.RATE_REVIEW)
    Call<CommonResponse> get_rate_review(@Body RequestBody get_rate_review);

    @POST(Const.START_APPOINTMENT)
    Call<CommonResponse> start_appointment(@Query("status") String status, @Body RequestBody start_appointment);

    @POST(Const.ACCEPT_APPOINTMENT)
    Call<CommonResponse> accept_appointment(@Query("status") String status, @Body RequestBody accept_appointment);

    @POST(Const.CANCEL_APPOINTMENT)
    Call<CommonResponse> cancel_appointment(@Body RequestBody cancel_appointment);

    @GET(Const.HISTORY)
    Call<AssetListingResponse> call_history(@Query("role") String role, @Query("type") String type, @Query("user_id") String user_id, @Query("page") int page);

    @GET(Const.GET_CHAT_LISTING)
    Call<ChatListingResponse> get_chat_listing(@Query("user_id") String user_id, @Query("limit") int limit);

    @GET(Const.SOCIAL_MEDIA)
    Call<SocialMediaResponse> call_social_media(@Query("limit") int limit, @Query("page") int page);

    @GET(Const.GET_PAYMENT_AMOUNT)
    Call<PaymentResponse> get_payment_amount();

    @GET(Const.NOTIFICATIONS)
    Call<NotificationsResponse> call_notifications(@Query("role") String role, @Query("limit") int limit);

    @POST(Const.SUBMIT_APPOINTMENT)
    Call<CommonResponse> submit_appointment(@Body RequestBody submit_appointment);

    @POST(Const.FORGOT_PASSWORD)
    Call<CommonResponse> forgot_password(@Body RequestBody forgot_password);

    @POST(Const.GET_PROFILE)
    Call<ProfileResponse> update_profile(@Query("role") String role, @Query("type") String type, @Query("user_id") String user_id, @Body RequestBody update_profile);

    @POST(Const.GET_PROFILE)
    Call<ProfileResponse> get_profile(@Query("role") String role, @Query("type") String type, @Query("user_id") String user_id);

    @Multipart
    @POST(Const.UPLOAD_PICTURE)
    Call<UploadImageResponse> upload_image(@Part MultipartBody.Part image);

    @POST(Const.REGISTER_USER)
    Call<CommonResponse> user_registering(@Query("role") String role, @Body RequestBody user_registering);

    @POST(Const.SEND_MESSAGE)
    Call<SendMessageResponse> send_message(@Body RequestBody send_message);

    @POST(Const.CREATE_ORDER)
    Call<OrderResponse> create_order(@Body RequestBody create_order);

    @POST(Const.MESSAGE_HISTORY)
    Call<MessageHistoryResponse> get_message_history(@Query("limit") int limit, @Query("page") int page, @Body RequestBody get_message_history);

    @POST(Const.LOGIN_USER)
    Call<LoginUserResponse> user_login(@Query("role") String role, @Body RequestBody user_registering);

    @POST(Const.CHANGE_PASSWORD)
    Call<CommonResponse> change_password(@Body RequestBody change_password);

    @POST(Const.GET_SEARCH)
    Call<SearchResponse> get_search_results(@Query("type") String type, @Query("filter") String filter, @Query("user_id") String user_id, @Query("limit") int limit, @Query("page") int page, @Body RequestBody get_search_results);

    @POST(Const.GET_SEARCH)
    Call<RecentSearchResponse> get_recent_search_results(@Query("type") String type, @Query("user_id") String user_id, @Body RequestBody get_search_results);

    @GET(Const.WEB_VIEW)
    Call<WebViewResponse> call_web_view();
}

