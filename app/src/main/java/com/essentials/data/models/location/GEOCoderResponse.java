package com.essentials.data.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GEOCoderResponse {

    @SerializedName("results")
    @Expose
    private List<GEOCoderResult> results = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<GEOCoderResult> getResults() {
        return results;
    }

    public void setResults(List<GEOCoderResult> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
