package com.essentials.data.models.webview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TermsandconditionsData {

    @SerializedName("termsandconditions_id")
    @Expose
    private Integer termsandconditionsId;
    @SerializedName("description")
    @Expose
    private String description;

    public Integer getTermsandconditionsId() {
        return termsandconditionsId;
    }

    public void setTermsandconditionsId(Integer termsandconditionsId) {
        this.termsandconditionsId = termsandconditionsId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
