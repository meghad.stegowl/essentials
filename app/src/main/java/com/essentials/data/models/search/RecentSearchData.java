package com.essentials.data.models.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecentSearchData {

    @SerializedName("healthcarecategory_id")
    @Expose
    private String healthcarecategoryId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("recentsearch_id")
    @Expose
    private String recentsearchId;
    @SerializedName("text")
    @Expose
    private String text;

    public String getRecentsearchId() {
        return recentsearchId;
    }

    public void setRecentsearchId(String recentsearchId) {
        this.recentsearchId = recentsearchId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHealthcarecategoryId() {
        return healthcarecategoryId;
    }

    public void setHealthcarecategoryId(String healthcarecategoryId) {
        this.healthcarecategoryId = healthcarecategoryId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
