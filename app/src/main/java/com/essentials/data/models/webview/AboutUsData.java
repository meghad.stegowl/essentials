package com.essentials.data.models.webview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AboutUsData {
    @SerializedName("aboutus_id")
    @Expose
    private String aboutusId;
    @SerializedName("description")
    @Expose
    private String description;

    public String getAboutusId() {
        return aboutusId;
    }

    public void setAboutusId(String aboutusId) {
        this.aboutusId = aboutusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
