package com.essentials.data.models.webview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WebViewResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("contactus_data")
    @Expose
    private ArrayList<ContactUsData> contactusData = null;
    @SerializedName("aboutus_data")
    @Expose
    private ArrayList<AboutUsData> aboutusData = null;
    @SerializedName("shareourapp_data")
    @Expose
    private ArrayList<ShareOurAppData> shareourappData = null;
    @SerializedName("privacypolicy_data")
    @Expose
    private ArrayList<PrivacyPolicyData> privacypolicyData = null;
    @SerializedName("termsandconditions_data")
    @Expose
    private ArrayList<TermsandconditionsData> termsandconditionsData = null;
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ContactUsData> getContactusData() {
        return contactusData;
    }

    public void setContactusData(ArrayList<ContactUsData> contactusData) {
        this.contactusData = contactusData;
    }

    public ArrayList<AboutUsData> getAboutusData() {
        return aboutusData;
    }

    public void setAboutusData(ArrayList<AboutUsData> aboutusData) {
        this.aboutusData = aboutusData;
    }

    public ArrayList<ShareOurAppData> getShareourappData() {
        return shareourappData;
    }

    public void setShareourappData(ArrayList<ShareOurAppData> shareourappData) {
        this.shareourappData = shareourappData;
    }

    public ArrayList<PrivacyPolicyData> getPrivacypolicyData() {
        return privacypolicyData;
    }

    public void setPrivacypolicyData(ArrayList<PrivacyPolicyData> privacypolicyData) {
        this.privacypolicyData = privacypolicyData;
    }

    public ArrayList<TermsandconditionsData> getTermsandconditionsData() {
        return termsandconditionsData;
    }

    public void setTermsandconditionsData(ArrayList<TermsandconditionsData> termsandconditionsData) {
        this.termsandconditionsData = termsandconditionsData;
    }
}
