package com.essentials.data.models.webview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShareOurAppData {
    @SerializedName("shareourapp_id")
    @Expose
    private String shareourappId;
    @SerializedName("android_link")
    @Expose
    private String androidLink;
    @SerializedName("ios_link")
    @Expose
    private String iosLink;

    public String getShareourappId() {
        return shareourappId;
    }

    public void setShareourappId(String shareourappId) {
        this.shareourappId = shareourappId;
    }

    public String getAndroidLink() {
        return androidLink;
    }

    public void setAndroidLink(String androidLink) {
        this.androidLink = androidLink;
    }

    public String getIosLink() {
        return iosLink;
    }

    public void setIosLink(String iosLink) {
        this.iosLink = iosLink;
    }

}
