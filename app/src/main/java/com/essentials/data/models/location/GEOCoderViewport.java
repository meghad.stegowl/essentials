package com.essentials.data.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GEOCoderViewport {

    @SerializedName("northeast")
    @Expose
    private GEOCoderLocation northeast;
    @SerializedName("southwest")
    @Expose
    private GEOCoderLocation southwest;

    public GEOCoderLocation getNortheast() {
        return northeast;
    }

    public void setNortheast(GEOCoderLocation northeast) {
        this.northeast = northeast;
    }

    public GEOCoderLocation getSouthwest() {
        return southwest;
    }

    public void setSouthwest(GEOCoderLocation southwest) {
        this.southwest = southwest;
    }
}
