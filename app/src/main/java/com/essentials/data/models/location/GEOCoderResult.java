package com.essentials.data.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GEOCoderResult {

    @SerializedName("address_components")
    @Expose
    private List<GEOCoderAddressComponent> addressComponents = null;
    @SerializedName("formatted_address")
    @Expose
    private String formattedAddress;
    @SerializedName("geometry")
    @Expose
    private GEOCoderGeometry geometry;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @SerializedName("plus_code")
    @Expose
    private GEOCoderPlusCode plusCode;
    @SerializedName("types")
    @Expose
    private List<String> types = null;

    public List<GEOCoderAddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<GEOCoderAddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public GEOCoderGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(GEOCoderGeometry geometry) {
        this.geometry = geometry;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public GEOCoderPlusCode getPlusCode() {
        return plusCode;
    }

    public void setPlusCode(GEOCoderPlusCode plusCode) {
        this.plusCode = plusCode;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
}
