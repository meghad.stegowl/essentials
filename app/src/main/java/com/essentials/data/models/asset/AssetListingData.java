package com.essentials.data.models.asset;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssetListingData {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("businessname")
    @Expose
    private String businessname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("county")
    @Expose
    private String county;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("service_description")
    @Expose
    private String serviceDescription;
    @SerializedName("healthcarecategory_id")
    @Expose
    private String healthcarecategoryId;
    @SerializedName("healthcarecategory")
    @Expose
    private String healthcarecategory;
    @SerializedName("total_rateandreview")
    @Expose
    private String totalRateandreview;
    @SerializedName("rate_average")
    @Expose
    private float rateAverage;
    @SerializedName("history_id")
    @Expose
    private String historyId;
    @SerializedName("provider_id")
    @Expose
    private String providerId;
    @SerializedName("provider_name")
    @Expose
    private String providerName;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("healthcarecategory_name")
    @Expose
    private String healthcarecategoryName;
    @SerializedName("rateandreviews_id")
    @Expose
    private String rateandreviewsId;
    @SerializedName("rate")
    @Expose
    private float rate;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("rateandreviews_status")
    @Expose
    private Boolean rateandreviewsStatus;
    @SerializedName("appointment_status")
    @Expose
    private String appointmentStatus;
    @SerializedName("accept_status")
    @Expose
    private String acceptStatus;

    public String getAcceptStatus() {
        return acceptStatus;
    }

    public void setAcceptStatus(String acceptStatus) {
        this.acceptStatus = acceptStatus;
    }

    public String getHistoryId() {
        return historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getHealthcarecategoryName() {
        return healthcarecategoryName;
    }

    public void setHealthcarecategoryName(String healthcarecategoryName) {
        this.healthcarecategoryName = healthcarecategoryName;
    }

    public String getRateandreviewsId() {
        return rateandreviewsId;
    }

    public void setRateandreviewsId(String rateandreviewsId) {
        this.rateandreviewsId = rateandreviewsId;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getRateandreviewsStatus() {
        return rateandreviewsStatus;
    }

    public void setRateandreviewsStatus(Boolean rateandreviewsStatus) {
        this.rateandreviewsStatus = rateandreviewsStatus;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBusinessname() {
        return businessname;
    }

    public void setBusinessname(String businessname) {
        this.businessname = businessname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getHealthcarecategoryId() {
        return healthcarecategoryId;
    }

    public void setHealthcarecategoryId(String healthcarecategoryId) {
        this.healthcarecategoryId = healthcarecategoryId;
    }

    public String getHealthcarecategory() {
        return healthcarecategory;
    }

    public void setHealthcarecategory(String healthcarecategory) {
        this.healthcarecategory = healthcarecategory;
    }

    public String getTotalRateandreview() {
        return totalRateandreview;
    }

    public void setTotalRateandreview(String totalRateandreview) {
        this.totalRateandreview = totalRateandreview;
    }

    public float getRateAverage() {
        return rateAverage;
    }

    public void setRateAverage(float rateAverage) {
        this.rateAverage = rateAverage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
