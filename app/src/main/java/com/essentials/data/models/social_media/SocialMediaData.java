package com.essentials.data.models.social_media;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialMediaData {
    @SerializedName("socialmedia_id")
    @Expose
    private Integer socialmediaId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("link")
    @Expose
    private String link;

    public Integer getSocialmediaId() {
        return socialmediaId;
    }

    public void setSocialmediaId(Integer socialmediaId) {
        this.socialmediaId = socialmediaId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
