package com.essentials.data.models.asset;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AssetDetailsResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("provider_data")
    @Expose
    private ArrayList<AssetProviderData> providerData = null;
    @SerializedName("rateandreviews_data")
    @Expose
    private ArrayList<AssetRateAndReview> rateandreviewsData = null;
    @SerializedName("per_page")
    @Expose
    private String perPage;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("last_page")
    @Expose
    private Integer lastPage;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AssetProviderData> getProviderData() {
        return providerData;
    }

    public void setProviderData(ArrayList<AssetProviderData> providerData) {
        this.providerData = providerData;
    }

    public ArrayList<AssetRateAndReview> getRateandreviewsData() {
        return rateandreviewsData;
    }

    public void setRateandreviewsData(ArrayList<AssetRateAndReview> rateandreviewsData) {
        this.rateandreviewsData = rateandreviewsData;
    }

    public String getPerPage() {
        return perPage;
    }

    public void setPerPage(String perPage) {
        this.perPage = perPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

}
