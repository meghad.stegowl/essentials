package com.essentials.data.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GEOCoderGeometry {

    @SerializedName("location")
    @Expose
    private GEOCoderLocation location;
    @SerializedName("location_type")
    @Expose
    private String locationType;
    @SerializedName("viewport")
    @Expose
    private GEOCoderViewport viewport;

    public GEOCoderLocation getLocation() {
        return location;
    }

    public void setLocation(GEOCoderLocation location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public GEOCoderViewport getViewport() {
        return viewport;
    }

    public void setViewport(GEOCoderViewport viewport) {
        this.viewport = viewport;
    }
}
