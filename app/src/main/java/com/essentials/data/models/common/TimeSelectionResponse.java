package com.essentials.data.models.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeSelectionResponse {
String start_time;
String end_time;
Boolean isDaySelected;

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public Boolean getDaySelected() {
        return isDaySelected;
    }

    public void setDaySelected(Boolean daySelected) {
        isDaySelected = daySelected;
    }
}
