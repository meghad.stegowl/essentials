package com.essentials.data.models.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryResponseData {

    @SerializedName("healthcarecategory_id")
    @Expose
    private Integer healthcarecategoryId;
    @SerializedName("type")
    @Expose
    private String type;

    public Integer getHealthcarecategoryId() {
        return healthcarecategoryId;
    }

    public void setHealthcarecategoryId(Integer healthcarecategoryId) {
        this.healthcarecategoryId = healthcarecategoryId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
