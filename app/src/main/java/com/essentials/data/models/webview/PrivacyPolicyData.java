package com.essentials.data.models.webview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrivacyPolicyData {
    @SerializedName("privacypolicy_id")
    @Expose
    private String privacypolicyId;
    @SerializedName("description")
    @Expose
    private String description;

    public String getPrivacypolicyId() {
        return privacypolicyId;
    }

    public void setPrivacypolicyId(String privacypolicyId) {
        this.privacypolicyId = privacypolicyId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
