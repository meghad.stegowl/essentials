package com.essentials.data.models.chat;

public class ChatModal {
    String time;
    String id;
    String image;
    String name;
    String message;

    public ChatModal() {
    }

    public ChatModal(String time, String id, String image, String name, String message) {
        this.time = time;
        this.id = id;
        this.image = image;
        this.name = name;
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
