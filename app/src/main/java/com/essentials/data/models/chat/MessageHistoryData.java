package com.essentials.data.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageHistoryData {
    @SerializedName("m_id")
    @Expose
    private String mId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("send_user_id")
    @Expose
    private String sendUserId;
    @SerializedName("display_date")
    @Expose
    private String displayDate;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("display_time")
    @Expose
    private String displayTime;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("read_status")
    @Expose
    private String readStatus;

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public String getMId() {
        return mId;
    }

    public void setMId(String mId) {
        this.mId = mId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(String sendUserId) {
        this.sendUserId = sendUserId;
    }

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
