package com.essentials.data.models.asset;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssetRateAndReview {
    @SerializedName("rateandreviews_id")
    @Expose
    private String rateandreviewsId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("provider_id")
    @Expose
    private String providerId;
    @SerializedName("rate")
    @Expose
    private float rate;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("date")
    @Expose
    private String date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRateandreviewsId() {
        return rateandreviewsId;
    }

    public void setRateandreviewsId(String rateandreviewsId) {
        this.rateandreviewsId = rateandreviewsId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

}
