package com.essentials.data.models.intro_slider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IntroSliderData {

    @SerializedName("introductionslider_id")
    @Expose
    private String introductionsliderId;
    @SerializedName("homeslider_id")
    @Expose
    private String homesliderId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("description")
    @Expose
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIntroductionsliderId() {
        return introductionsliderId;
    }

    public void setIntroductionsliderId(String introductionsliderId) {
        this.introductionsliderId = introductionsliderId;
    }

    public String getHomesliderId() {
        return homesliderId;
    }

    public void setHomesliderId(String homesliderId) {
        this.homesliderId = homesliderId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
