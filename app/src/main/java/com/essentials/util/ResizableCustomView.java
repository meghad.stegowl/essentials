package com.essentials.util;


import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Saumil Shah on 16/3/18.
 */

public class ResizableCustomView {
    private ResizableCustomView() {
        throw new IllegalStateException("Utility class");
    }

    public static void doResizeTextView(Context context, final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeOnGlobalLayoutListener(this);
                if (maxLine == 0) {

                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(context, Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() > maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(context, Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    if (tv.getLineCount() != maxLine && tv.getLineCount() > maxLine) {
                        int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                        String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                        tv.setText(text);
                        tv.setMovementMethod(LinkMovementMethod.getInstance());
                        tv.setText(
                                addClickablePartTextViewResizable(context, Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                        viewMore), TextView.BufferType.SPANNABLE);
                    }
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(Context context, final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            int end = str.lastIndexOf(spanableText) + spanableText.length();
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(@NotNull View widget) {

                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        if (Prefs.getPrefInstance().getValue(context, Const.SELECTED_LANGUAGE, "en").equals("en")) {
                            doResizeTextView(context, tv, -1, "Less", false);
                        } else {
                            doResizeTextView(context, tv, -1, "लेस", false);
                        }
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        if (Prefs.getPrefInstance().getValue(context, Const.SELECTED_LANGUAGE, "en").equals("en")) {
                            doResizeTextView(context, tv, 2, "More", true);
                        } else {
                            doResizeTextView(context, tv, 2, "मोअर", true);
                        }
                    }

                }
            }, str.lastIndexOf(spanableText), end, 0);
            final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(0, 162, 247));
            ssb.setSpan(fcs, str.lastIndexOf(spanableText), end, 0);
        }
        return ssb;

    }
}