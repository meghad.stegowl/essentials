package com.essentials.util;


public class Const {

    public static final String PREFS_FILENAME = "Essentials";
    public static final String APP_STATUS = "appstatus";
    public static final String SELECTED_LANGUAGE = "selected_language";
    public static final String DEVICE_TYPE = "2"; // Android
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String EMAIL = "email";
    public static final String NOTIFICATION_COUNT = "notification_count";
    public static final String PASSWORD = "password";
    public static final String KEEP_USER_LOGGED_IN = "keepUserLoggedIn";
    public static final String USER_FULL_NAME = "user_full_name";
    public static final String USER_FIRST_NAME = "user_first_name";
    public static final String USER_LAST_NAME = "user_last_name";
    public static final String USER_ABOUT = "user_about";
    public static final String USER_GENDER = "user_gender";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String TIME = "time";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String USER_DOB = "user_dob";
    public static final String LOGIN_ACCESS = "login";
    public static final String LOGIN_STATUS = "login_status";
    public static final String LOGIN_ACCOUNT = "login_account";
    public static final String FCM_ID = "fcm_id";
    public static final String IS_ACTIVE_PLAN = "is_active_plan";
    public static final String IS_API_NOT_CALLED = "is_api_not_called";
    public static final String API_NOT_CALLED_FROM = "api_not_called_from";
    public static final String TOKEN  = "token";
    public static final String GUEST_TOKEN  = "guest_token";
    public static final String KEY = "key";
    public static final String SHUFFLE = "shuffle";
    public static final String ISAUTOPLAY = "isautoplay";
    public static final String REPEAT = "repeat";
    public static final String BANNER_ADS = "banner_ads";
    public static final String INTERSTITIAL_ADS = "Interstitial_ads";
    public static final String ADDRESS = "address";
    public static final String COUNTRY = "country";
    public static final String STATE = "state";
    public static final String CITY = "city";
    public static final String HEALTH_CARE_CATEGORY_ID = "health_care_category";
    public static final String ZIP_CODE = "zip_code";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String CUSTOMER_LOGIN = "customer_login";
    public static final String BUSINESS_NAME = "business_name";
    public static final String PAYMENT_AMOUNT = "payment_amount";
    public static final String SET_PROFILE_IMAGE = "profile_image";

//    Total API

//    private static final String HOST = "http://100.24.72.18/essentials/api/";
    private static final String HOST = "https://admin.essentialsmobileapp.com/api/";
    private static final String APPEND = "";
    private static final String GEO_APPEND = "";
    public static final String ESSENTIALS_HOST = HOST;
    public static final String LOCATION_HOST = "https://maps.googleapis.com/maps/api/";
    public static final String ESSENTIALS_HOST_GEO = HOST + GEO_APPEND;

    public static final String LOGIN_USER = ESSENTIALS_HOST + "login";
    public static final String GET_INTRO_SLIDER = ESSENTIALS_HOST + "sliders";
    public static final String GET_LOCATION = ESSENTIALS_HOST + "countrystatecity";
    public static final String GET_GEOLOCATION = LOCATION_HOST + "geocode/json";
    public static final String GET_CATEGORY = ESSENTIALS_HOST + "healthcarecategory";
    public static final String REGISTER_USER = ESSENTIALS_HOST + "signup";
    public static final String SEND_MESSAGE = ESSENTIALS_HOST + "messaging/sendmessage";
    public static final String MESSAGE_HISTORY = ESSENTIALS_HOST + "messaging/history";
    public static final String UPLOAD_PICTURE = ESSENTIALS_HOST + "uploads";
    public static final String LOGOUT = ESSENTIALS_HOST + "logout";
    public static final String RATE_REVIEW = ESSENTIALS_HOST + "giverateandreviews";
    public static final String START_APPOINTMENT = ESSENTIALS_HOST + "completeappointment";
    public static final String ACCEPT_APPOINTMENT = ESSENTIALS_HOST + "completeappointment";
    public static final String CANCEL_APPOINTMENT = ESSENTIALS_HOST + "cancelappointment";
    public static final String HISTORY = ESSENTIALS_HOST + "history";
    public static final String SOCIAL_MEDIA = ESSENTIALS_HOST + "socialmedia";
    public static final String GET_PAYMENT_AMOUNT = ESSENTIALS_HOST + "paymentamount";
    public static final String CREATE_ORDER = ESSENTIALS_HOST + "charge";
    public static final String GET_PROFILE = ESSENTIALS_HOST + "profile";
    public static final String GET_ASSET_LISTING = ESSENTIALS_HOST + "providerslisting";
    public static final String GET_ASSET_DETAILS = ESSENTIALS_HOST + "providersdetails";
    public static final String SUBMIT_APPOINTMENT = ESSENTIALS_HOST + "requestappointment";
    public static final String FORGOT_PASSWORD = ESSENTIALS_HOST + "forgotpassword";
    public static final String CHANGE_PASSWORD = ESSENTIALS_HOST + "changepassword";
    public static final String NOTIFICATIONS = ESSENTIALS_HOST + "notifications";
    public static final String WEB_VIEW = ESSENTIALS_HOST + "cms";
    public static final String GET_SEARCH = ESSENTIALS_HOST + "search";
    public static final String GET_CHAT_LISTING = ESSENTIALS_HOST + "messaging/userlist";

}