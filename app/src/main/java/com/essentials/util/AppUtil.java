package com.essentials.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.essentials.R;
import com.essentials.application.Essentials;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class AppUtil {

    private static final String TAG = "TAG --> " + AppUtil.class.getSimpleName();

    public static void Go_To_This(Context context, String social_handle, String name, String id) {
        String package_Name;
        String app_Link;
        String app_URL;

        switch (social_handle) {
            case "Facebook":
                package_Name = "com.facebook.katana";
                app_Link = "fb://page/" + id;
                app_URL = "https://www.facebook.com/" + name;
                break;
            case "Twitter":
                package_Name = "com.twitter.android";
                app_Link = "twitter://user?user_id=" + id;
                if (!name.equals(""))
                    app_URL = "https://twitter.com/" + name;
                else
                    app_URL = "https://twitter.com/" + id;
                break;
            case "Instagram":
                package_Name = "com.instagram.android";
                app_Link = "http://instagram.com/_u/" + name;
                app_URL = "http://instagram.com/" + name;
                break;
            case "Youtube":
                package_Name = "com.google.android.youtube";
                app_Link = "https://www.youtube.com/user/" + name;
                app_URL = "https://www.youtube.com/user/" + name;
                break;
            default:
                package_Name = "";
                app_Link = "";
                app_URL = "";
                break;
        }

        if (package_Name.equals("")) return;

        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(package_Name, PackageManager.GET_ACTIVITIES);
            if (packageInfo != null && packageInfo.packageName.equals(package_Name)) {
                boolean activated = context.getPackageManager().getApplicationInfo(package_Name, 0).enabled;
                if (!app_Link.equals("") && activated) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(app_Link)));
                    // return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
                } else {
                    if (app_URL.equals("")) return;
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(app_URL)));
                }
            } else {
                if (app_URL.equals("")) return;
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(app_URL)));
            }
        } catch (PackageManager.NameNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(app_URL)));
            e.printStackTrace();
        }
    }

    public static boolean isInternetAvailable(Context context) {
        NetworkInfo info = ((ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null) {
            Log.d(TAG, "no internet connection");
            return false;
        } else {
            if (info.isConnected()) {
                Log.d(TAG, "internet connection connected");
                return true;
            } else {
                Log.d(TAG, "internet connection available");
                return true;
            }
        }
    }

    public static int setLanguage(Context context, int layout) {
        Locale locale = new Locale(Prefs.getPrefInstance().getValue(context, Const.SELECTED_LANGUAGE, "en").toLowerCase());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        ((Activity) context).getBaseContext().getResources().updateConfiguration(config,
                ((Activity) context).getBaseContext().getResources().getDisplayMetrics());
        return layout;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void show_Snackbar(Context context, View view, String message, boolean canShowLong) {
        Snackbar snackbar = Snackbar.make(view, Html.fromHtml(message), canShowLong ? Snackbar.LENGTH_LONG : Snackbar.LENGTH_SHORT);
        View snackBar_View = snackbar.getView();
        snackBar_View.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
        TextView textView = snackBar_View.findViewById(com.google.android.material.R.id.snackbar_text);
//        textView.setTypeface(Typeface.createFromAsset(context.getResources().getAssets(), "fonts/dejaVuSans-Bold.ttf"));
        textView.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.text_snack_bar));
        textView.setMaxLines(5);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        snackbar.show();
    }

//    public static void show_Exit_Snackbar(Context context, View view, boolean canShowLong) {
//        Snackbar snackbar = Snackbar.make(view, Html.fromHtml(context.getResources().getString(R.string.press_back_again_to_exit)), canShowLong ? Snackbar.LENGTH_LONG : Snackbar.LENGTH_SHORT);
//        View snackBar_View = snackbar.getView();
//        snackBar_View.setBackgroundColor(context.getResources().getColor(R.color.white));
//        TextView textView = snackBar_View.findViewById(com.google.android.material.R.id.snackbar_text);
//        textView.setTypeface(Typeface.createFromAsset(context.getResources().getAssets(), "fonts/dejaVuSans-Bold.ttf"));
//        textView.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.text_snack_bar));
//        textView.setMaxLines(5);
//        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//        textView.setMovementMethod(LinkMovementMethod.getInstance());
//        textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//        snackbar.show();
//    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public static void setup_Fragment(FragmentManager fragmentManager, Fragment fragment, String path, String type, String title, String id, String tag, String backStack, boolean isAdd) {
        Bundle bundle = new Bundle();
        bundle.putString("path", path);
        bundle.putString("type", type);
        bundle.putString("title", title);
        bundle.putString("id", id);
        fragment.setArguments(bundle);
        //change
//        if (isAdd)
//            fragmentManager.beginTransaction().add(R.id.frameContainer, fragment, tag).addToBackStack(backStack).commit();
//        else
//            fragmentManager.beginTransaction().replace(R.id.frameContainer, fragment, tag).addToBackStack(backStack).commit();
    }


    public static void setup_Fragment_with_bundle(FragmentManager fragmentManager, Fragment fragment, String tag, String backStack, boolean isAdd) {
        //change
//        if (isAdd)
//            fragmentManager.beginTransaction().add(R.id.frameContainer, fragment, tag).addToBackStack(backStack).commit();
//        else
//            fragmentManager.beginTransaction().replace(R.id.frameContainer, fragment, tag).addToBackStack(backStack).commit();
    }

    public static int getDisplayWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        return (displayMetrics.widthPixels > displayMetrics.heightPixels) ? displayMetrics.heightPixels : displayMetrics.widthPixels;
    }

    public static File getAppDirectory(Context context) {
        /* Checks if external storage is available for read and write */
//        String state = Environment.getExternalStorageState();
//        if (Environment.MEDIA_MOUNTED.equals(state)) {
//            Log.e(TAG, "Application Directory is in External Drive");
//        File appFolder = new File(Environment.getExternalStorageDirectory(), context.getResources().getString(R.string.app_name));
        File appFolder = new File(Essentials.applicationContext.getFilesDir(), context.getResources().getString(R.string.app_name));
        if (!appFolder.mkdirs()) {
            appFolder.mkdirs();
        }
        return appFolder;
//        }
//        else {
//            Log.e(TAG, "Application Directory is in Internal Drive");
//            File appFolder = new File(context.getFilesDir(), context.getResources().getString(R.string.app_name));
//            if (!appFolder.mkdirs()) {
//                Log.e(TAG, "Application Directory is not created");
//                appFolder.mkdirs();
//            }
//            return appFolder;
//        }
    }

    public static int getDisplayHeight(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static boolean checkIsTablet(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        float widthInches = metrics.widthPixels / metrics.xdpi;
        float heightInches = metrics.heightPixels / metrics.ydpi;
        double diagonalInches = Math.sqrt(Math.pow(widthInches, 2) + Math.pow(heightInches, 2));
        return diagonalInches >= 7.0;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static int convertDpToPixel(int dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static int convertPixelsToDp(int px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = px / (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public boolean isConnected(Context context) {
        //
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static void showHashKey(Context context, String packageName) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo( packageName
                    , PackageManager.GET_SIGNATURES); //Your package name here
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    //                            // Check HomeScreen is foreground activity or not.
//                            if(((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1).get(0).topActivity.getShortClassName().equals(".activities.HomeScreen"))
}