package com.essentials.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.essentials.R;
import com.essentials.activities.Notifications;
import com.essentials.activities.Search;
import com.essentials.adapters.RecentSearchAdapter;
import com.essentials.adapters.SearchAdapter;
import com.essentials.data.models.search.RecentSearchResponse;
import com.essentials.data.models.search.SearchData;
import com.essentials.data.models.search.SearchResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.MySnapHelper;
import com.essentials.util.Prefs;
import com.essentials.util.RecyclerViewPositionHelper;
import com.google.android.material.button.MaterialButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {

    private static SearchFragment instance = null;
    @BindView(R.id.listing)
    RecyclerView listing;
    @BindView(R.id.main_container)
    LinearLayout mainContainer;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.recent_listing)
    RecyclerView recentListing;
    @BindView(R.id.suggestions_listing)
    RecyclerView suggestionsListing;
    @BindView(R.id.button_filter)
    MaterialButton buttonFilter;
    @BindView(R.id.search_input)
    EditText searchInput;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.error_search)
    TextView errorSearch;


    private AppManageInterface appManageInterface;
    private Handler handler;
    private Runnable runnable;
    private String path;
    private String type;
    private int LIMIT = 100, CURRENT_PAGE = 1, LAST_PAGE = 100;
    private String pageTitle, filter, image, keyword;
    private boolean isGenre;
    private Context context;
    private FragmentManager fragmentManager;
    private Activity activity;
    private boolean loadingInProgress, isCustomer;
    private RecyclerView.Adapter recentAdapter;
    private Call<SearchResponse> searchResultCall;
    private Call<RecentSearchResponse> recentSearchResultCall;
    private RecentSearchAdapter recentSearchAdapter;
    private ArrayList<SearchData> data = new ArrayList<>();
    private String type_result, type_suggestion, type_recent, type_save_recent, type_listing, filter_available, filter_unavailable;

    public static synchronized SearchFragment getInstance() {
        return instance;
    }

    public static synchronized SearchFragment newInstance() {
        return instance = new SearchFragment();
    }

//    @OnClick(R.id.search)
//    void setSearchClicked() {
//
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.search)
    void setSearch() {
        if (searchInput.getText() == null) {
//                if (recentListing.getVisibility() == View.GONE) {
            type = type_recent;
            getSuggestionSearchResult("");
            errorSearch.setError(getResources().getString(R.string.type_to_search));
            errorSearch.setVisibility(View.VISIBLE);
//                }
        } else if (searchInput.getText().toString().trim().length() < 1) {
//                if (recentListing.getVisibility() == View.GONE) {
            type = type_recent;
            getSuggestionSearchResult("");
            errorSearch.setError(getResources().getString(R.string.type_to_search));
            errorSearch.setVisibility(View.VISIBLE);
//                }
        } else {
            searchInput.clearFocus();
            filter = filter_unavailable;
            errorSearch.setVisibility(View.GONE);
            setSearchResult(searchInput.getText().toString());
        }
    }

    @OnClick(R.id.button_filter)
    void setButtonFilter() {
        type = type_listing;
        getSuggestionSearchResult("");
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        path = "";
        if (context != null) {
            type = context.getResources().getString(R.string.type_result);
            type_result = context.getResources().getString(R.string.type_result);
            type_suggestion = context.getResources().getString(R.string.type_suggestions);
            type_recent = context.getResources().getString(R.string.type_recent);
            type_save_recent = context.getResources().getString(R.string.type_save_recent);
            type_listing = context.getResources().getString(R.string.type_listing);
            filter_available = context.getResources().getString(R.string.filter_available);
            filter_unavailable = context.getResources().getString(R.string.filter_unavailable);

        }
        filter = "Unavailable";
        activity = getActivity();

        listing.setVisibility(View.GONE);
        suggestionsListing.setVisibility(View.GONE);
        recentListing.setVisibility(View.GONE);
        errorSearch.setVisibility(View.GONE);

        if (getArguments() != null) {
            keyword = getArguments().getString("keyword");
            isCustomer = getArguments().getBoolean("isCustomer");
        }
        if (keyword != null && !keyword.isEmpty()) {
            searchInput.setText(keyword);
            setSearchResult(keyword);
        } else {
            type = type_recent;
            getSuggestionSearchResult("");
        }

        init();
    }

    private void init() {
        searchInput.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(searchInput, InputMethodManager.SHOW_FORCED);

//        searchContainer.setEndIconOnClickListener(v -> {
//            if (searchInput.getText() == null) {
////                if (recentListing.getVisibility() == View.GONE) {
//                type = type_recent;
//                getSuggestionSearchResult("");
//                searchContainer.setError(getResources().getString(R.string.type_to_search));
////                }
//            } else if (searchInput.getText().toString().trim().length() < 1) {
////                if (recentListing.getVisibility() == View.GONE) {
//                type = type_recent;
//                getSuggestionSearchResult("");
//                searchContainer.setError(getResources().getString(R.string.type_to_search));
////                }
//            } else {
//                searchInput.clearFocus();
//                filter = filter_unavailable;
//                setSearchResult(searchInput.getText().toString());
//            }
//        });

//        searchInput.setOnFocusChangeListener((v, hasFocus) -> {
//            if (searchInput.getText() == null) {
////                if (recentListing.getVisibility() == View.GONE) {
//                    type = type_recent;
//                    getSuggestionSearchResult("");
////                }
//            } else if (searchInput.getText().toString().trim().length() < 1) {
////                if (recentListing.getVisibility() == View.GONE) {
//                    type = type_recent;
//                    getSuggestionSearchResult("");
////                }
//            } else {
//                searchContainer.setError("");
//                type = type_suggestion;
//                getSuggestionSearchResult(searchInput.getText().toString());
//            }
//        });

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if (searchInput.getText() == null) {
//                    if (recentListing.getVisibility() == View.GONE) {
//                        type = type_recent;
//                        getSuggestionSearchResult("");
//                    }
//                } else if (searchInput.getText().toString().trim().length() < 1) {
//                    if (recentListing.getVisibility() == View.GONE) {
//                        type = type_recent;
//                        getSuggestionSearchResult("");
//                    }
//                } else {
//                    searchContainer.setError("");
//                    type = type_suggestion;
//                    getSuggestionSearchResult(searchInput.getText().toString());
//                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (searchInput.getText() == null) {
                    if (recentListing.getVisibility() == View.GONE) {
                        type = type_recent;
                        getSuggestionSearchResult("");
                    }
                } else if (searchInput.getText().toString().trim().length() < 1) {
                    if (recentListing.getVisibility() == View.GONE) {
                        type = type_recent;
                        getSuggestionSearchResult("");
                    }
                } else {
                    errorSearch.setVisibility(View.GONE);
                    type = type_suggestion;
                    getSuggestionSearchResult(searchInput.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (searchInput.getText() == null) {
                    if (recentListing.getVisibility() == View.GONE) {
                        type = type_recent;
                        getSuggestionSearchResult("");
                    }
                }
            }
        });

        searchInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                ((InputMethodManager) Objects.requireNonNull(context.getSystemService(Activity.INPUT_METHOD_SERVICE))).hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (searchInput.getText() == null) {
//                    if (recentListing.getVisibility() == View.GONE) {
                    type = type_recent;
                    getSuggestionSearchResult("");
//                    }
                } else if (searchInput.getText().toString().trim().length() < 1) {
//                    if (recentListing.getVisibility() == View.GONE) {
                    type = type_recent;
                    getSuggestionSearchResult("");
//                    }
                } else {
                    searchInput.clearFocus();
                    filter = filter_unavailable;
                    setSearchResult(searchInput.getText().toString());

                }
                return true;
            }
            return false;
        });


        RecyclerView.ItemAnimator animator = listing.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        RecyclerViewPositionHelper positionHelper = RecyclerViewPositionHelper.createHelper(listing);

        listing.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) listing.getLayoutManager();
                    if (layoutManager != null) {
                        int total = layoutManager.getItemCount();
                        int currentLastItem = positionHelper.findLastCompletelyVisibleItemPosition() + 1;

                        if (currentLastItem == (total - 1)) {
                            if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                                loadingInProgress = true;
                                CURRENT_PAGE++;
                                getSearchResult(type, searchInput.getText().toString());
                            }
                        }
                    }
                }
            }
        });

        scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) && scrollY > oldScrollY) {
//                    if (scrollY > 0) {
//                        LinearLayoutManager layoutManager = (LinearLayoutManager) listing.getLayoutManager();
//                        if (layoutManager != null) {
//                            int total = layoutManager.getItemCount();
//                            int currentLastItem = positionHelper.findLastCompletelyVisibleItemPosition() + 1;
//
//                            if (currentLastItem == (total - 1)) {
                    if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                        loadingInProgress = true;
                        CURRENT_PAGE++;
                        getSearchResult(type, searchInput.getText().toString());
                    }
                }
            }
        });
    }

    private void setSuggestionsListing() {
        loader.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        listing.setVisibility(View.GONE);
        recentListing.setVisibility(View.GONE);
        suggestionsListing.setVisibility(View.VISIBLE);
    }

    private void setRecentListing() {
        loader.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        listing.setVisibility(View.GONE);
        suggestionsListing.setVisibility(View.GONE);
        recentListing.setVisibility(View.VISIBLE);
    }

    public void setSearchResult(String keyword) {
        loader.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        suggestionsListing.setVisibility(View.GONE);
        recentListing.setVisibility(View.GONE);
        listing.setVisibility(View.VISIBLE);
        type = type_result;
        filter = filter_unavailable;
        getSearchResult(type, keyword);
    }

    private void setSearchListing() {
        assert getArguments() != null;

        noDataFound.setVisibility(View.GONE);
        recentListing.setVisibility(View.GONE);
        suggestionsListing.setVisibility(View.GONE);
        loader.setVisibility(View.GONE);
        listing.setVisibility(View.VISIBLE);
    }

    private void setNoDataFound() {
        loader.setVisibility(View.GONE);
        listing.setVisibility(View.GONE);
        recentListing.setVisibility(View.GONE);
        suggestionsListing.setVisibility(View.GONE);
        noDataFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler.removeCallbacksAndMessages(null);
        }
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getSearchResult(String type_result, String search_keyword) {
        if (AppUtil.isInternetAvailable(context)) {
            if (CURRENT_PAGE == 1) {
                loader.setVisibility(View.VISIBLE);
            }
            if (searchResultCall != null && searchResultCall.isExecuted())
                searchResultCall.cancel();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("search", search_keyword);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_search = RequestBody.create(params, MediaType.parse("application/json"));
            searchResultCall = APIUtils.getAPIService().get_search_results(type_result, filter, Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""), LIMIT, 1, get_search);
            searchResultCall.enqueue(new Callback<SearchResponse>() {
                @Override
                public void onResponse(@NonNull Call<SearchResponse> call, @NonNull Response<SearchResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
//                                data.addAll(response.body().getData());
//                                listing.setHasFixedSize(true);
                                searchInput.setText(keyword);
                                searchInput.clearFocus();
                                listing.setHasFixedSize(true);
                                listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                listing.setOnFlingListener(null);
                                listing.setAdapter(new SearchAdapter(context, response.body().getData(), fragmentManager, isCustomer));
                                new MySnapHelper().attachToRecyclerView(listing);
                                noDataFound.setVisibility(View.GONE);
                                loader.setVisibility(View.GONE);

                                type = type_save_recent;
                                getSuggestionSearchResult(search_keyword);

                                CURRENT_PAGE = response.body().getCurrentPage();
                                LAST_PAGE = response.body().getLastPage();
                                if (CURRENT_PAGE == 1) {
                                    listing.setVisibility(View.VISIBLE);
                                }
                                loadingInProgress = false;
                                setSearchListing();
                                loader.setVisibility(View.GONE);
                            } else {
                                loadingInProgress = false;
                                if (CURRENT_PAGE == 1) {
                                    setNoDataFound();
                                    loader.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            if (CURRENT_PAGE == 1) {
                                setNoDataFound();
                                loader.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        if (CURRENT_PAGE == 1) {
                            loader.setVisibility(View.GONE);
                            setNoDataFound();
                        }

                    }
                }

                @Override
                public void onFailure(@NonNull Call<SearchResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (CURRENT_PAGE == 1) {
                        loader.setVisibility(View.GONE);
                    }

                    if (!call.isCanceled())
                        setNoDataFound();
                }
            });
        } else {
            if (CURRENT_PAGE == 1) {
                loader.setVisibility(View.GONE);
                setNoDataFound();
            }
        }
    }

    private void getSuggestionSearchResult(String keyword) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            if (recentSearchResultCall != null && recentSearchResultCall.isExecuted())
                recentSearchResultCall.cancel();
            if (recentSearchAdapter != null)
                recentSearchAdapter.clear();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("search", keyword);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody user_search = RequestBody.create(params, MediaType.parse("application/json"));
            recentSearchResultCall = APIUtils.getAPIService().get_recent_search_results(type, "7", user_search);
            recentSearchResultCall.enqueue(new Callback<RecentSearchResponse>() {
                @Override
                public void onResponse(@NonNull Call<RecentSearchResponse> call, @NonNull Response<RecentSearchResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (type.toLowerCase().equals(type_save_recent.toLowerCase())) {
                                loader.setVisibility(View.GONE);
                            } else {
                                if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                    if (type.toLowerCase().equals(type_suggestion.toLowerCase())) {
                                        suggestionsListing.setHasFixedSize(true);
                                        suggestionsListing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                        recentSearchAdapter = new RecentSearchAdapter(context, response.body().getData(), fragmentManager, isCustomer, type);
                                        suggestionsListing.setAdapter(recentSearchAdapter);
                                        setSuggestionsListing();
                                    } else if (type.toLowerCase().equals(type_listing.toLowerCase())) {
                                        recentListing.setHasFixedSize(true);
                                        recentListing.setLayoutManager(new LinearLayoutManager(context));
                                        recentSearchAdapter = new RecentSearchAdapter(context, response.body().getData(), fragmentManager, isCustomer, type);
                                        recentListing.setAdapter(recentSearchAdapter);
                                        setRecentListing();
                                    } else {
                                        recentListing.setHasFixedSize(true);
                                        recentListing.setLayoutManager(new LinearLayoutManager(context));
                                        recentSearchAdapter = new RecentSearchAdapter(context, response.body().getData(), fragmentManager, isCustomer, type);
                                        recentListing.setAdapter(recentSearchAdapter);
                                        setRecentListing();
                                    }
                                } else {
                                    if (type.toLowerCase().equals(type_save_recent.toLowerCase())) {
                                        loader.setVisibility(View.GONE);
                                    } else {
                                        loader.setVisibility(View.GONE);
                                        setRecentListing();
                                    }
                                }
                            }
                        } else {
                            if (type.toLowerCase().equals(type_save_recent.toLowerCase())) {
                                loader.setVisibility(View.GONE);
                            } else {
                                loader.setVisibility(View.GONE);
                                setRecentListing();
                            }
                        }
                    } else {
                        if (type.toLowerCase().equals(type_save_recent.toLowerCase())) {
                            loader.setVisibility(View.GONE);
                        } else {
                            loader.setVisibility(View.GONE);
                            setRecentListing();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RecentSearchResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (type.toLowerCase().equals(type_save_recent.toLowerCase())) {
                        loader.setVisibility(View.GONE);
                    } else {
                        loader.setVisibility(View.GONE);
                        if (!call.isCanceled())
                            setRecentListing();
                    }
                }
            });
        } else {
            if (type.toLowerCase().equals(type_save_recent.toLowerCase())) {
                loader.setVisibility(View.GONE);
            } else {
                loader.setVisibility(View.GONE);
                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                final AlertDialog dialog = new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setView(dialog_view)
                        .show();

                if (dialog.getWindow() != null)
                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                    dialog.dismiss();
                });
            }
        }
    }

}
