package com.essentials.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.activities.AppointmentHistory;
import com.essentials.activities.Chat;
import com.essentials.activities.HomeScreen;
import com.essentials.activities.Notifications;
import com.essentials.activities.Search;
import com.essentials.application.Essentials;
import com.essentials.data.models.common.CommonResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

public class SubmitAppointmentFragment extends Fragment {

    private static SubmitAppointmentFragment instance = null;

    @BindView(R.id.asset_image)
    CircularImageView assetImage;
    @BindView(R.id.asset_rating)
    RatingBar assetRating;
    @BindView(R.id.asset_rating_count)
    TextView assetRatingCount;
    @BindView(R.id.button_request_appointment)
    MaterialButton buttonRequestAppointment;
    @BindView(R.id.appointment_date_input)
    TextView appointmentDateInput;
    @BindView(R.id.appointment_date)
    LinearLayout appointmentDate;
    @BindView(R.id.appointment_time_input)
    TextView appointmentTimeInput;
    @BindView(R.id.appointment_time)
    LinearLayout appointmentTime;
    @BindView(R.id.appointment_address_input)
    TextView appointmentAddressInput;
    @BindView(R.id.appointment_address)
    LinearLayout appointmentAddress;
    @BindView(R.id.comments_input)
    TextInputEditText commentsInput;
    @BindView(R.id.comments_container)
    TextInputLayout commentsContainer;
    @BindView(R.id.button_submit)
    MaterialButton buttonSubmit;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.asset_name)
    TextView assetName;
    @BindView(R.id.asset_category)
    TextView assetCategory;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.main_container)
    CoordinatorLayout mainContainer;
    @BindView(R.id.button_call)
    ImageView buttonCall;
    @BindView(R.id.chat)
    ImageView chat;
    @BindView(R.id.chat_calling_container)
    LinearLayout chatCallingContainer;

    private AppManageInterface appManageInterface;
    private String path, type, pageTitle, date, showDate, time, day, healthCategoryId;
    private String name, image, categoryName, ratingCount, address;
    private float rating;
    private String contentType = "";
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private Activity activity;
    private Boolean isCustomer;

    public static synchronized SubmitAppointmentFragment getInstance() {
        return instance;
    }

    public static synchronized SubmitAppointmentFragment newInstance() {
        return instance = new SubmitAppointmentFragment();
    }

    public void setNoDataFound() {
        noDataFound.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.button_submit)
    void setButtonSubmit() {
        if (commentsInput.getText() == null) {
            commentsContainer.setError(getResources().getString(R.string.comments_empty));
        } else if (commentsInput.getText().toString().trim().length() == 0) {
            commentsContainer.setError(getResources().getString(R.string.comments_empty));
        } else {
            submitAppointment();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_submit_appointment, container, false);
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.search)
    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @OnClick(R.id.chat)
    void setChat() {
        startActivity(new Intent(context, Chat.class).putExtra("isCustomer", isCustomer).putExtra("path", path));
    }

    @OnClick(R.id.button_call)
    void setCall() {
        String[] PERMISSIONS = {Manifest.permission.CALL_PHONE};

        if (!AppUtil.hasPermissions(context, PERMISSIONS)) {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.call_permission_request));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog.dismiss());
            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(v -> {
                dialog.dismiss();
                askPermission(SubmitAppointmentFragment.this,
                        Manifest.permission.CALL_PHONE)
                        .onAccepted(result -> call_phone())
                        .onDenied(result -> {
                            View dialog_view1 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog1 = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view1)
                                    .show();

                            if (dialog1.getWindow() != null)
                                dialog1.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view1.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.call_permission_Deny));
                            ((Button) dialog_view1.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                            ((Button) dialog_view1.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

                            dialog_view1.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog1.dismiss());
                            dialog_view1.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog1.dismiss();
                                result.askAgain();
                            });
                        })
                        .onForeverDenied(result -> {
                            View dialog_view2 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog2 = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view2)
                                    .show();

                            if (dialog2.getWindow() != null)
                                dialog2.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view2.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.call_permission_Forever_Deny));
                            ((Button) dialog_view2.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                            ((Button) dialog_view2.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.go_to_settings));

                            dialog_view2.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog2.dismiss());
                            dialog_view2.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog2.dismiss();
                                result.goToSettings();
                            });
                        })
                        .ask();
            });
        } else{
            call_phone();
        }
    }

    private void call_phone(){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" +Prefs.getPrefInstance().getValue(context, Const.PHONE_NUMBER, "")));
        startActivity(callIntent);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        mainContainer.setVisibility(View.GONE);
        activity = getActivity();

        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            time = getArguments().getString("time");
            date = getArguments().getString("date");
            showDate = getArguments().getString("showDate");
            pageTitle = getArguments().getString("title");
            image = getArguments().getString("image");
            rating = getArguments().getFloat("rating");
            name = getArguments().getString("assetName");
            categoryName = getArguments().getString("categoryName");
            ratingCount = getArguments().getString("ratingCount");
            day = getArguments().getString("day");
            healthCategoryId = getArguments().getString("healthCategoryId");
            isCustomer = getArguments().getBoolean("isCustomer");
        }
        init();


    }

    private void init() {

        address = Prefs.getPrefInstance().getValue(context, Const.ADDRESS, "") + "," + " " +
                Prefs.getPrefInstance().getValue(context, Const.CITY, "") + "," + " " +
                Prefs.getPrefInstance().getValue(context, Const.STATE, "") + "," + " " +
                Prefs.getPrefInstance().getValue(context, Const.COUNTRY, "");
        Log.d("mytag","here is address is---------------------------------"+address);
        appointmentAddressInput.setText(address);
        appointmentTimeInput.setText(time);
        appointmentDateInput.setText(showDate);
        assetName.setText(name);
        assetCategory.setText(categoryName);
        assetRating.setRating(rating);
        assetRating.setRating(rating);
        if (ratingCount != null && !ratingCount.isEmpty() && !ratingCount.equals("(0)")) {
            assetRatingCount.setVisibility(View.VISIBLE);
            assetRating.setVisibility(View.VISIBLE);
        } else {
            assetRatingCount.setVisibility(View.GONE);
            assetRating.setVisibility(View.GONE);
        }
        assetRatingCount.setText(ratingCount);
        assetCategory.setText(categoryName);
        Glide
                .with(Essentials.applicationContext)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                .thumbnail(0.25f)
                .error(R.drawable.ic_profile_icon)
                .into(assetImage);
        mainContainer.setVisibility(View.VISIBLE);
        commentsInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                commentsContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void submitAppointment() {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("provider_id", path);
                jsonObject.put("customer_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                jsonObject.put("healthcarecategory_id", healthCategoryId);
                jsonObject.put("date", date);
                jsonObject.put("day", day);
                jsonObject.put("start_time", time);
                jsonObject.put("address", address);
                jsonObject.put("latitude", Prefs.getPrefInstance().getValue(context, Const.LATITUDE, ""));
                jsonObject.put("longitude", Prefs.getPrefInstance().getValue(context, Const.LONGITUDE, ""));
                jsonObject.put("comment", commentsInput.getText().toString());
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody submit_appointment = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().submit_appointment(submit_appointment).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {

                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);

                            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog.dismiss();
//                                startActivity(new Intent(context, HomeScreen.class).putExtra("isCustomer", isCustomer));
                                startActivity(new Intent(context, AppointmentHistory.class).putExtra("isCustomer", isCustomer)
                                        .putExtra("pending", "pending"));
//                                AppointmentHistoryFragment.getInstance().getAppointmentHistory();
                            });

                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        } else {
                            loader.setVisibility(View.GONE);
                            mainContainer.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                activity.onBackPressed();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        mainContainer.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                            activity.onBackPressed();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    mainContainer.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                        activity.onBackPressed();
                    });
                }
            });
        } else {
            mainContainer.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }
}


