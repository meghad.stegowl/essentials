package com.essentials.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.essentials.R;
import com.essentials.activities.Notifications;
import com.essentials.activities.Search;
import com.essentials.adapters.ListingAdapter;
import com.essentials.data.models.asset.AssetListingResponse;
import com.essentials.data.models.common.CommonResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.essentials.notifications.NotificationManager.didAppointmentAccepted;
import static com.essentials.notifications.NotificationManager.didAppointmentCancelled;
import static com.essentials.notifications.NotificationManager.didAppointmentCompleted;
import static com.essentials.notifications.NotificationManager.didAppointmentEnded;
import static com.essentials.notifications.NotificationManager.didAppointmentStarted;
import static com.essentials.notifications.NotificationManager.didAppointmentStatusChanged;
import static com.essentials.notifications.NotificationManager.didAppointmentSubmitted;
import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

public class AppointmentHistoryFragment extends Fragment {

    private static AppointmentHistoryFragment instance = null;
    @BindView(R.id.listing)
    RecyclerView listing;
    @BindView(R.id.main_container)
    LinearLayout mainContainer;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.tab_1_line)
    View tab1Line;
    @BindView(R.id.tab_1)
    TextView tab1;
    @BindView(R.id.tab_2_line)
    View tab2Line;
    @BindView(R.id.tab_2)
    TextView tab2;
    @BindView(R.id.tab_3_line)
    View tab3Line;
    @BindView(R.id.tab_3)
    TextView tab3;
    @BindView(R.id.tab_container)
    LinearLayout tabContainer;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;


    private AppManageInterface appManageInterface;
    private String path, pending;
    private String type, role;
    private String contentType = "", pageTitle;
    private int assetGroupClassPath;
    private boolean isCustomer;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private boolean loadingInProgress;
    private Activity activity;
    private String latitude, longitude;
    private int LIMIT = 100, CURRENT_PAGE = 1, LAST_PAGE = 100;

    public static synchronized AppointmentHistoryFragment getInstance() {
        return instance;
    }

    public static synchronized AppointmentHistoryFragment newInstance() {
        return instance = new AppointmentHistoryFragment();
    }

    public void setNoDataFound() {
        noDataFound.setVisibility(View.VISIBLE);
        listing.setVisibility(View.GONE);
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.search)
    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_appointment_history, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);
        listing.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);
        tabContainer.setVisibility(View.VISIBLE);
        activity = getActivity();
        if (getArguments() != null) {
            path = getArguments().getString("path");
            pending = getArguments().getString("pending");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
            isCustomer = getArguments().getBoolean("isCustomer");
        }
        if (isCustomer) role = getResources().getString(R.string.customers);
        else role = getResources().getString(R.string.providers);

        tab1.setText(context.getResources().getString(R.string.completed));
        tab2.setText(context.getResources().getString(R.string.pending));
        tab3.setText(context.getResources().getString(R.string.cancelled));

        if (pending != null && pending.equals("pending")) {
            if (pending.equals("pending")) {
                type = context.getResources().getString(R.string.pending);
                tab1.setSelected(false);
                tab2.setSelected(true);
                tab3.setSelected(false);
                tab1Line.setVisibility(View.INVISIBLE);
                tab3Line.setVisibility(View.INVISIBLE);
                tab2Line.setVisibility(View.VISIBLE);
            }
        } else {
            if (path.equals("pending")) {
                type = context.getResources().getString(R.string.pending);
                tab1.setSelected(false);
                tab2.setSelected(true);
                tab3.setSelected(false);
                tab1Line.setVisibility(View.INVISIBLE);
                tab3Line.setVisibility(View.INVISIBLE);
                tab2Line.setVisibility(View.VISIBLE);
            } else if (path.equals("canceled")) {
                type = context.getResources().getString(R.string.cancelled);
                tab1.setSelected(false);
                tab3.setSelected(true);
                tab2.setSelected(false);
                tab1Line.setVisibility(View.INVISIBLE);
                tab3Line.setVisibility(View.VISIBLE);
                tab2Line.setVisibility(View.INVISIBLE);
            } else if (path.equals("rateReview")) {
                type = context.getResources().getString(R.string.completed);
                tab1.setSelected(true);
                tab2.setSelected(false);
                tab3.setSelected(false);
                tab1Line.setVisibility(View.VISIBLE);
                tab2Line.setVisibility(View.INVISIBLE);
                tab3Line.setVisibility(View.INVISIBLE);
            } else if (path.equals("completed")) {
                Log.d("mytag", "here");
                if (isCustomer) {
                    Log.d("mytag", "is Customer history pending- completed:");
                    type = context.getResources().getString(R.string.pending);
                    tab1.setSelected(false);
                    tab2.setSelected(true);
                    tab3.setSelected(false);
                    tab1Line.setVisibility(View.INVISIBLE);
                    tab3Line.setVisibility(View.INVISIBLE);
                    tab2Line.setVisibility(View.VISIBLE);
                } else {
                    Log.d("mytag", "is not Customer  history completed- completed:");
                    type = context.getResources().getString(R.string.completed);
                    tab1.setSelected(true);
                    tab2.setSelected(false);
                    tab3.setSelected(false);
                    tab1Line.setVisibility(View.VISIBLE);
                    tab2Line.setVisibility(View.INVISIBLE);
                    tab3Line.setVisibility(View.INVISIBLE);
                }
            } else {
                type = context.getResources().getString(R.string.completed);
                tab1.setSelected(true);
                tab2.setSelected(false);
                tab3.setSelected(false);
                tab1Line.setVisibility(View.VISIBLE);
                tab2Line.setVisibility(View.INVISIBLE);
                tab3Line.setVisibility(View.INVISIBLE);
            }
        }
        noDataFound.setVisibility(View.GONE);
        getAppointmentHistory();
        CURRENT_PAGE = 1;
        LAST_PAGE = 1;
        LIMIT = 50;

        tab1.setOnClickListener(v -> {
            if (!tab1.isSelected()) {
                setTab1Selected();
            }
        });
        tab2.setOnClickListener(v -> {
            if (!tab2.isSelected()) {
                setTab2Selected();
            }
        });
        tab3.setOnClickListener(v -> {
            if (!tab3.isSelected()) {
                setTab3Selected();
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void setTab1Selected() {
        type = context.getResources().getString(R.string.completed);
        tab1.setSelected(true);
        tab2.setSelected(false);
        tab3.setSelected(false);
        tab1Line.setVisibility(View.VISIBLE);
        tab2Line.setVisibility(View.INVISIBLE);
        tab3Line.setVisibility(View.INVISIBLE);
        noDataFound.setVisibility(View.GONE);
        CURRENT_PAGE = 1;
        LAST_PAGE = 1;
        LIMIT = 50;
        getAppointmentHistory();
    }

    private void setTab2Selected() {
        type = context.getResources().getString(R.string.pending);
        tab1.setSelected(false);
        tab2.setSelected(true);
        tab3.setSelected(false);
        tab1Line.setVisibility(View.INVISIBLE);
        tab2Line.setVisibility(View.VISIBLE);
        tab3Line.setVisibility(View.INVISIBLE);
        noDataFound.setVisibility(View.GONE);
        CURRENT_PAGE = 1;
        LAST_PAGE = 1;
        LIMIT = 50;
        getAppointmentHistory();
    }

    private void setTab3Selected() {
        type = context.getResources().getString(R.string.cancelled);
        tab1.setSelected(false);
        tab2.setSelected(false);
        tab3.setSelected(true);
        tab1Line.setVisibility(View.INVISIBLE);
        tab2Line.setVisibility(View.INVISIBLE);
        tab3Line.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);
        CURRENT_PAGE = 1;
        LAST_PAGE = 1;
        LIMIT = 50;
        getAppointmentHistory();
    }

    public void getAppointmentHistory() {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().call_history(role, type, Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""), 1).enqueue(new Callback<AssetListingResponse>() {
                @Override
                public void onResponse(@NonNull Call<AssetListingResponse> call, @NonNull Response<AssetListingResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                listing.setHasFixedSize(true);
                                listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                listing.setAdapter(new ListingAdapter(response.body().getData(), context, fragmentManager, isCustomer, true));
                                listing.setVisibility(View.VISIBLE);
                                loader.setVisibility(View.GONE);
                                scrollView.setVisibility(View.VISIBLE);
                                noDataFound.setVisibility(View.GONE);
                            } else {
                                loader.setVisibility(View.GONE);
                                scrollView.setVisibility(View.GONE);
                                noDataFound.setVisibility(View.VISIBLE);
                            }
                        } else {
                            loader.setVisibility(View.GONE);
                            scrollView.setVisibility(View.GONE);
                            noDataFound.setVisibility(View.VISIBLE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                activity.onBackPressed();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        scrollView.setVisibility(View.GONE);
                        noDataFound.setVisibility(View.VISIBLE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AssetListingResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                    noDataFound.setVisibility(View.VISIBLE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            loader.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            noDataFound.setVisibility(View.VISIBLE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    public void setNotificationData(int id, String notiType, String path, boolean isCustomer) {
        if (id == didAppointmentStatusChanged) {
            if (tab2.isSelected()) {
                getActivity().runOnUiThread(this::setTab2Selected);
            }
        } else if (id == didAppointmentSubmitted) {

        } else if (id == didAppointmentStarted) {

        } else if (id == didAppointmentCancelled) {
            if (tab2.isSelected()) {
                getActivity().runOnUiThread(this::setTab2Selected);
            }
            if (tab3.isSelected()) {
                getActivity().runOnUiThread(this::setTab3Selected);
            }
        } else if (id == didAppointmentEnded) {

        } else if (id == didAppointmentAccepted) {

        } else if (id == didAppointmentCompleted) {
            if (!isCustomer) {
                if (tab1.isSelected()) {
                    getActivity().runOnUiThread(this::setTab1Selected);
                }
            }
            if (tab2.isSelected()) {
                getActivity().runOnUiThread(this::setTab2Selected);
            }
        } else {
            if (isCustomer) {
                if (tab2.isSelected()) {
                    getActivity().runOnUiThread(this::setTab2Selected);
                }
            }
            if (tab1.isSelected()) {
                getActivity().runOnUiThread(this::setTab1Selected);
            }
        }
    }

    public void setCall(String phone) {
        String[] PERMISSIONS = {Manifest.permission.CALL_PHONE};

        if (!AppUtil.hasPermissions(context, PERMISSIONS)) {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.call_permission_request));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.cancel));
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(context.getResources().getString(R.string.set_permission));

            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog.dismiss());
            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(v -> {
                dialog.dismiss();
                askPermission(AppointmentHistoryFragment.this,
                        Manifest.permission.CALL_PHONE)
                        .onAccepted(result -> call_phone())
                        .onDenied(result -> {
                            View dialog_view1 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog1 = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view1)
                                    .show();

                            if (dialog1.getWindow() != null)
                                dialog1.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view1.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.call_permission_Deny));
                            ((Button) dialog_view1.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.cancel));
                            ((Button) dialog_view1.findViewById(R.id.dialog_ok)).setText(context.getResources().getString(R.string.set_permission));

                            dialog_view1.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog1.dismiss());
                            dialog_view1.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog1.dismiss();
                                result.askAgain();
                            });
                        })
                        .onForeverDenied(result -> {
                            View dialog_view2 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog2 = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view2)
                                    .show();

                            if (dialog2.getWindow() != null)
                                dialog2.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view2.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.call_permission_Forever_Deny));
                            ((Button) dialog_view2.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.cancel));
                            ((Button) dialog_view2.findViewById(R.id.dialog_ok)).setText(context.getResources().getString(R.string.go_to_settings));

                            dialog_view2.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog2.dismiss());
                            dialog_view2.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog2.dismiss();
                                result.goToSettings();
                            });
                        })
                        .ask();
            });
        } else {
            call_phone();
        }
    }

    private void call_phone() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + Prefs.getPrefInstance().getValue(context, Const.PHONE_NUMBER, "")));
        context.startActivity(callIntent);
    }

    public void getAcceptAppointment(String history_id, String providers_id) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("history_id", history_id);
                jsonObject.put("provider_id", providers_id);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody start_appointment = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().accept_appointment("Accept", start_appointment).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.body() != null) {
                        loader.setVisibility(View.GONE);
                        noDataFound.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();
                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);
                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    } else {
                        noDataFound.setVisibility(View.GONE);
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                            activity.onBackPressed();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    noDataFound.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                        activity.onBackPressed();
                    });
                }
            });
        } else {
            noDataFound.setVisibility(View.GONE);
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }
}
