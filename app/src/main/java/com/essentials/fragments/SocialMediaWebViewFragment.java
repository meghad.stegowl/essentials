package com.essentials.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.essentials.R;
import com.essentials.activities.Search;
import com.essentials.util.AppManageInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SocialMediaWebViewFragment extends Fragment {

    private static SocialMediaWebViewFragment instance = null;
    @BindView(R.id.web_view)
    WebView webView;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.header)
    LinearLayout header;


    private AppManageInterface appManageInterface;
    private String path;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private Activity activity;

    public static synchronized SocialMediaWebViewFragment getInstance() {
        return instance;
    }

    public static synchronized SocialMediaWebViewFragment newInstance() {
        return instance = new SocialMediaWebViewFragment();
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_social_media_web_view, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);
        activity = getActivity();
        webView.setWebViewClient(new MyBrowser());

        if (getArguments() != null) {
            path = getArguments().getString("path");
        }

        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(path);

    }

//    @Override
//    public void onAttach(@NonNull Context context) {
//        super.onAttach(context);
//        Activity activity = (HomeScreen) context;
//        try {
//            appManageInterface = (AppManageInterface) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString() + " must implement Interface");
//        }
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private static class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
