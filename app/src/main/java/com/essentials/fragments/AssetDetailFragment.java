package com.essentials.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.activities.Chat;
import com.essentials.activities.Notifications;
import com.essentials.activities.RegisterOptions;
import com.essentials.activities.RequestAppointment;
import com.essentials.activities.Search;
import com.essentials.adapters.ReviewAdapter;
import com.essentials.application.Essentials;
import com.essentials.data.models.asset.AssetDetailsResponse;
import com.essentials.data.models.asset.AssetProviderData;
import com.essentials.data.models.asset.AssetRateAndReview;
import com.essentials.data.models.common.TimeSelectionResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

public class AssetDetailFragment extends Fragment implements OnMapReadyCallback {

    private static AssetDetailFragment instance = null;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.asset_image)
    CircularImageView assetImage;
    @BindView(R.id.asset_rating)
    RatingBar assetRating;
    @BindView(R.id.asset_rating_count)
    TextView assetRatingCount;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.about)
    TextView about;
    @BindView(R.id.ll_schedule)
    LinearLayout llSchedule;
    @BindView(R.id.rate_listing)
    RecyclerView rateListing;
    @BindView(R.id.reviews_listing)
    RecyclerView reviewsListing;
    @BindView(R.id.button_request_appointment)
    MaterialButton buttonRequestAppointment;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.rl_map)
    RelativeLayout rlMap;
    @BindView(R.id.map)
    FragmentContainerView map;
    @BindView(R.id.iv_map)
    ImageView ivMap;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    SupportMapFragment mapFragment;
    GoogleApiClient googleApiClient;
    LocationRequest locationRequest;
    GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    @BindView(R.id.tab_layout_container)
    LinearLayout tabLayoutContainer;
    @BindView(R.id.asset_name)
    TextView assetName;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.language)
    TextView language;
    @BindView(R.id.business_name)
    TextView business_name;
    @BindView(R.id.asset_category)
    TextView assetCategoryName;
    @BindView(R.id.listing_no_data_found)
    TextView listingNoDataFound;
    ArrayList<AssetProviderData> assetProvider;
    @BindView(R.id.main_container)
    CoordinatorLayout mainContainer;
    @BindView(R.id.chat)
    ImageView chat;
    @BindView(R.id.button_call)
    ImageView buttonCall;
    @BindView(R.id.chat_calling_container)
    LinearLayout chatCallingContainer;
    float rating;
    private AppManageInterface appManageInterface;
    private String path, type, name, businessname,addressName, image, categoryName, ratingCount, healthCategoryId;
    private double provider_lat, provider_long;
    private Boolean isCustomer;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private Activity activity;
    private FusedLocationProviderClient mFusedLocationClient;
    @BindView(R.id.tv_mon_start)
    TextView tv_mon_start;
    @BindView(R.id.tv_tue_start)
    TextView tv_tue_start;
    @BindView(R.id.tv_wed_start)
    TextView tv_wed_start;
    @BindView(R.id.tv_thurs_start)
    TextView tv_thurs_start;
    @BindView(R.id.tv_fri_start)
    TextView tv_fri_start;
    @BindView(R.id.tv_sat_start)
    TextView tv_sat_start;
    @BindView(R.id.tv_sun_start)
    TextView tv_sun_start;
    @BindView(R.id.tv_mon_end)
    TextView tv_mon_end;
    @BindView(R.id.tv_tue_end)
    TextView tv_tue_end;
    @BindView(R.id.tv_wed_end)
    TextView tv_wed_end;
    @BindView(R.id.tv_thurs_end)
    TextView tv_thurs_end;
    @BindView(R.id.tv_fri_end)
    TextView tv_fri_end;
    @BindView(R.id.tv_sat_end)
    TextView tv_sat_end;
    @BindView(R.id.tv_sun_end)
    TextView tv_sun_end;

    @BindView(R.id.tv_mon_dash)
    TextView tv_mon_dash;
    @BindView(R.id.tv_tue_dash)
    TextView tv_tue_dash;
    @BindView(R.id.tv_wed_dash)
    TextView tv_wed_dash;
    @BindView(R.id.tv_thurs_dash)
    TextView tv_thurs_dash;
    @BindView(R.id.tv_fri_dash)
    TextView tv_fri_dash;
    @BindView(R.id.tv_sat_dash)
    TextView tv_sat_dash;
    @BindView(R.id.tv_sun_dash)
    TextView tv_sun_dash;
    private TrackerSettings settings =
            new TrackerSettings()
                    .setUseGPS(true)
                    .setUseNetwork(true)
                    .setUsePassive(true)
                    .setTimeBetweenUpdates(1 * 60 * 1000)
                    .setMetersBetweenUpdates(100);
    double latitude = 0;
    double longitude = 0;

    public static synchronized AssetDetailFragment getInstance() {
        return instance;
    }

    public static synchronized AssetDetailFragment newInstance() {
        return instance = new AssetDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_asset_detail, container, false);
    }

    public void setNoDataFound() {
        noDataFound.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.chat)
    void setChat() {
        startActivity(new Intent(context, Chat.class).putExtra("isCustomer", isCustomer).putExtra("path", path));
    }

    @OnClick(R.id.button_call)
    void setCall() {
        String[] PERMISSIONS = {Manifest.permission.CALL_PHONE};

        if (!AppUtil.hasPermissions(context, PERMISSIONS)) {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.call_permission_request));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog.dismiss());
            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(v -> {
                dialog.dismiss();
                askPermission(AssetDetailFragment.this,
                        Manifest.permission.CALL_PHONE)
                        .onAccepted(result -> call_phone())
                        .onDenied(result -> {
                            View dialog_view1 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog1 = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view1)
                                    .show();

                            if (dialog1.getWindow() != null)
                                dialog1.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view1.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.call_permission_Deny));
                            ((Button) dialog_view1.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                            ((Button) dialog_view1.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

                            dialog_view1.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog1.dismiss());
                            dialog_view1.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog1.dismiss();
                                result.askAgain();
                            });
                        })
                        .onForeverDenied(result -> {
                            View dialog_view2 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog2 = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view2)
                                    .show();

                            if (dialog2.getWindow() != null)
                                dialog2.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view2.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.call_permission_Forever_Deny));
                            ((Button) dialog_view2.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                            ((Button) dialog_view2.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.go_to_settings));

                            dialog_view2.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog2.dismiss());
                            dialog_view2.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog2.dismiss();
                                result.goToSettings();
                            });
                        })
                        .ask();
            });
        } else {
            call_phone();
        }
    }

    private void call_phone() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + Prefs.getPrefInstance().getValue(context, Const.PHONE_NUMBER, "")));
        startActivity(callIntent);
    }

    @OnClick(R.id.button_request_appointment)
    void setButtonRequestAppointment() {
        if (Prefs.getPrefInstance().getValue(context, Const.LOGIN_ACCESS, "").equals(getString(R.string.logged_in))) {
            if (isCustomer)
                startActivity(new Intent(context, RequestAppointment.class)
                        .putExtra("path", path)
                        .putExtra("assetName", name)
                        .putExtra("businessname", businessname)
                        .putExtra("image", image)
                        .putExtra("rating", rating)
                        .putExtra("categoryName", categoryName)
                        .putExtra("ratingCount", ratingCount)
                        .putExtra("isCustomer", isCustomer)
                        .putExtra("healthCategoryId", healthCategoryId));
            else {
                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                final AlertDialog dialog = new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setView(dialog_view)
                        .show();

                if (dialog.getWindow() != null)
                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.please_customer_login_continue));
                ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(context.getResources().getString(R.string.sign_in));
                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.cancel));

                dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                    dialog.dismiss();
                    startActivity(new Intent(context, RegisterOptions.class).putExtra("isLogin", true));
                });

                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                    dialog.dismiss();
                });
            }
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.please_login_continue));
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(context.getResources().getString(R.string.sign_in));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.cancel));

            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                dialog.dismiss();
                startActivity(new Intent(context, RegisterOptions.class).putExtra("isLogin", true));
            });

            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.search)
    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        activity = getActivity();

        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            isCustomer = getArguments().getBoolean("isCustomer");
        }
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        init();
    }

    private void init() {
        if (Prefs.getPrefInstance().getValue(context, Const.LOGIN_ACCESS, "").equals(getString(R.string.logged_in)))
            chatCallingContainer.setVisibility(View.VISIBLE);
        else
            chatCallingContainer.setVisibility(View.GONE);


        loader.setVisibility(View.GONE);
        tabs.addTab(tabs.newTab().setText(getResources().getString(R.string.about)));
        tabs.addTab(tabs.newTab().setText(getResources().getString(R.string.schedule)));
        tabs.addTab(tabs.newTab().setText(getResources().getString(R.string.rate)));
        tabs.addTab(tabs.newTab().setText(getResources().getString(R.string.location)));
        tabs.addTab(tabs.newTab().setText(getResources().getString(R.string.reviews)));
        about.setVisibility(View.VISIBLE);
        rateListing.setVisibility(View.GONE);
        llSchedule.setVisibility(View.GONE);
        reviewsListing.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        mainContainer.setVisibility(View.GONE);
        rlMap.setVisibility(View.GONE);
        listingNoDataFound.setVisibility(View.GONE);


        tabs.setBackgroundColor(Color.TRANSPARENT);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tabs.getSelectedTabPosition() == 0) {
                    about.setVisibility(View.VISIBLE);
                    llSchedule.setVisibility(View.GONE);
                    rateListing.setVisibility(View.GONE);
                    reviewsListing.setVisibility(View.GONE);
                    rlMap.setVisibility(View.GONE);
                    listingNoDataFound.setVisibility(View.GONE);
                } else if (tabs.getSelectedTabPosition() == 1) {
                    about.setVisibility(View.GONE);
                    llSchedule.setVisibility(View.VISIBLE);
                    rateListing.setVisibility(View.GONE);
                    reviewsListing.setVisibility(View.GONE);
                    rlMap.setVisibility(View.GONE);
                    listingNoDataFound.setVisibility(View.GONE);
                } else if (tabs.getSelectedTabPosition() == 2) {
                    about.setVisibility(View.GONE);
                    llSchedule.setVisibility(View.GONE);
                    reviewsListing.setVisibility(View.GONE);
                    rlMap.setVisibility(View.GONE);
                    if (rateListing.getAdapter() != null) {
                        rateListing.setVisibility(View.VISIBLE);
                        listingNoDataFound.setVisibility(View.GONE);
                    } else {
                        rateListing.setVisibility(View.GONE);
                        listingNoDataFound.setText(R.string.no_rate_found);
                        listingNoDataFound.setVisibility(View.VISIBLE);
                    }
                } else if (tabs.getSelectedTabPosition() == 3) {
                        loadLocation();
                    about.setVisibility(View.GONE);
                    llSchedule.setVisibility(View.GONE);
                    rateListing.setVisibility(View.GONE);
                    reviewsListing.setVisibility(View.GONE);
                    rlMap.setVisibility(View.VISIBLE);
                    listingNoDataFound.setVisibility(View.GONE);
                } else {
                    about.setVisibility(View.GONE);
                    llSchedule.setVisibility(View.GONE);
                    rateListing.setVisibility(View.GONE);
                    rlMap.setVisibility(View.GONE);
                    if (reviewsListing.getAdapter() != null) {
                        reviewsListing.setVisibility(View.VISIBLE);
                        listingNoDataFound.setVisibility(View.GONE);
                    } else {
                        reviewsListing.setVisibility(View.GONE);
                        listingNoDataFound.setText(R.string.no_reviews_found);
                        listingNoDataFound.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        type = "others";
        getAssetDetails();



        ivMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Uri mapUri = Uri.parse("geo:0,0?q=" + Uri.encode(addressName));
//                Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
//                mapIntent.setPackage("com.google.android.apps.maps");
//                startActivity(mapIntent);
                String provider = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if ((latitude == 0 && longitude == 0) || !provider.contains("gps")) {
                    Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(gpsOptionsIntent);
                } else {
                    float destiLong = Float.parseFloat(String.valueOf(provider_long));
                    float destiLat = Float.parseFloat(String.valueOf(provider_lat));
                    String lat_pref = Prefs.getPrefInstance().getValue(context, Const.LATITUDE, "");
                    String long_pref = Prefs.getPrefInstance().getValue(context, Const.LONGITUDE, "");
//                    longitude = Float.parseFloat("-74.32555");
//                    latitude = Float.parseFloat("40.792736");
                    longitude = Float.parseFloat(long_pref);
                    latitude = Float.parseFloat(lat_pref);
                    Log.d("mytag","destiLat--------------:" + destiLat + " destiLong:-----------------++" + destiLong);
                    Log.d("mytag","latitude--------------:" + latitude + " longitude:-----------------" + longitude);
                    String uri = String.format("http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f", latitude, longitude, destiLat, destiLong);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                }

            }
        });
    }

    private void loadLocation() {
        LocationTracker tracker = null;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            tracker = new LocationTracker(context, settings) {
                @Override
                public void onLocationFound(@NonNull Location location) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d("mytag","lat" + location.getLatitude() + " long" + location.getLongitude());
                }

                @Override
                public void onTimeout() {
                }
            };
            tracker.startListening();
        }
    }

//    private void gotoLocationZoom(double v, double v1, int i) {
//        LatLng ll = new LatLng(v, v1);
//        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, i);
//        mGoogleMap.moveCamera(update);
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getAssetDetails() {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().get_asset_details(Prefs.getPrefInstance().getValue(context, Const.LATITUDE, ""), Prefs.getPrefInstance().getValue(context, Const.LONGITUDE, ""), path, 100, 1).enqueue(new Callback<AssetDetailsResponse>() {
                @Override
                public void onResponse(@NonNull Call<AssetDetailsResponse> call, @NonNull Response<AssetDetailsResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {

                            if (!isCustomer) {
                                if (Prefs.getPrefInstance().getValue(context, Const.USER_ID, "").equals(response.body().getProviderData().get(0).getUserId())) {
                                    buttonRequestAppointment.setVisibility(View.GONE);
                                    chat.setVisibility(View.GONE);
                                    buttonCall.setVisibility(View.GONE);
                                } else {
                                    buttonRequestAppointment.setVisibility(View.VISIBLE);
                                    chat.setVisibility(View.VISIBLE);
                                    buttonCall.setVisibility(View.VISIBLE);
                                }
                            } else {
                                buttonRequestAppointment.setVisibility(View.VISIBLE);
                                chat.setVisibility(View.VISIBLE);
                                buttonCall.setVisibility(View.VISIBLE);
                            }

                            ArrayList<TimeSelectionResponse> data = new ArrayList<>();
                            try {
                                provider_lat = Double.parseDouble(response.body().getProviderData().get(0).getLatitude());
                                provider_long = Double.parseDouble(response.body().getProviderData().get(0).getLongitude());
                            } catch (Exception e) {

                            }
                            mapFragment.getMapAsync(AssetDetailFragment.this);
                            about.setText(response.body().getProviderData().get(0).getServiceDescription());
                            Log.d("mytag", "about - " + response.body().getProviderData().get(0).getServiceDescription());
                            name = response.body().getProviderData().get(0).getFirstname() + " " + response.body().getProviderData().get(0).getLastname();
                            assetName.setText(name);
                            if(response.body().getProviderData().get(0).getTitle() != null && response.body().getProviderData().get(0).getTitle().isEmpty()){
                                title.setVisibility(View.GONE);
                            }else{
                                title.setText(response.body().getProviderData().get(0).getTitle());
                            }
                            if(response.body().getProviderData().get(0).getLang() != null && response.body().getProviderData().get(0).getLang().isEmpty()){
                                language.setVisibility(View.GONE);
                            }else{
                                language.setText(response.body().getProviderData().get(0).getLang());
                            }
                            businessname = response.body().getProviderData().get(0).getBusinessname();
                            business_name.setText(businessname);
                            Log.d("mytag","here is address is22222222222--------------------------"+addressName);
                            addressName = response.body().getProviderData().get(0).getAddress();
                            tvAddress.setText(addressName);
                            categoryName = response.body().getProviderData().get(0).getHealthcarecategory();
                            assetCategoryName.setText(categoryName);
                            rating = response.body().getProviderData().get(0).getRateAverage();
                            Log.d("mytag", "rating - " + rating);
                            assetRating.setRating(rating);

                            if (response.body().getProviderData().get(0).getMondayStartTime().equals("")) {
                                tv_mon_start.setText("Not Available");
                                tv_mon_end.setText("");
                                tv_mon_dash.setVisibility(View.GONE);
                            } else {
                                tv_mon_dash.setVisibility(View.VISIBLE);
                                tv_mon_start.setText(response.body().getProviderData().get(0).getMondayStartTime());
                                tv_mon_end.setText(response.body().getProviderData().get(0).getMondayEndTime());
                            }

                            if (response.body().getProviderData().get(0).getTuesdayStartTime().equals("")) {
                                tv_tue_start.setText("Not Available");
                                tv_tue_end.setText("");
                                tv_tue_dash.setVisibility(View.GONE);
                            } else {
                                tv_tue_dash.setVisibility(View.VISIBLE);
                                tv_tue_start.setText(response.body().getProviderData().get(0).getTuesdayStartTime());
                                tv_tue_end.setText(response.body().getProviderData().get(0).getTuesdayEndTime());
                            }

                            if (response.body().getProviderData().get(0).getWednesdayStartTime().equals("")) {
                                tv_wed_start.setText("Not Available");
                                tv_wed_end.setText("");
                                tv_wed_dash.setVisibility(View.GONE);
                            } else {
                                tv_wed_dash.setVisibility(View.VISIBLE);
                                tv_wed_start.setText(response.body().getProviderData().get(0).getWednesdayStartTime());
                                tv_wed_end.setText(response.body().getProviderData().get(0).getWednesdayEndTime());
                            }

                            if (response.body().getProviderData().get(0).getThursdayStartTime().equals("")) {
                                tv_thurs_start.setText("Not Available");
                                tv_thurs_end.setText("");
                                tv_thurs_dash.setVisibility(View.GONE);
                            } else {
                                tv_thurs_dash.setVisibility(View.VISIBLE);
                                tv_thurs_start.setText(response.body().getProviderData().get(0).getThursdayStartTime());
                                tv_thurs_end.setText(response.body().getProviderData().get(0).getThursdayEndTime());
                            }

                            if (response.body().getProviderData().get(0).getFridayStartTime().equals("")) {
                                tv_fri_start.setText("Not Available");
                                tv_fri_end.setText("");
                                tv_fri_dash.setVisibility(View.GONE);
                            } else {
                                tv_fri_dash.setVisibility(View.VISIBLE);
                                tv_fri_start.setText(response.body().getProviderData().get(0).getFridayStartTime());
                                tv_fri_end.setText(response.body().getProviderData().get(0).getFridayEndTime());
                            }

                            if (response.body().getProviderData().get(0).getSaturdayStartTime().equals("")) {
                                tv_sat_start.setText("Not Available");
                                tv_sat_end.setText("");
                                tv_sat_dash.setVisibility(View.GONE);
                            } else {
                                tv_sat_start.setText(response.body().getProviderData().get(0).getSaturdayStartTime());
                                tv_sat_end.setText(response.body().getProviderData().get(0).getSaturdayEndTime());
                                tv_sat_dash.setVisibility(View.VISIBLE);
                            }

                            if (response.body().getProviderData().get(0).getSundayStartTime().equals("")) {
                                tv_sun_start.setText("Not Available");
                                tv_sun_end.setText("");
                                tv_sun_dash.setVisibility(View.GONE);
                            } else {
                                tv_sun_start.setText(response.body().getProviderData().get(0).getSundayStartTime());
                                tv_sun_end.setText(response.body().getProviderData().get(0).getSundayEndTime());
                                tv_sun_dash.setVisibility(View.VISIBLE);
                            }

                            if (response.body().getProviderData().get(0).getTotalRateandreview() != null && !response.body().getProviderData().get(0).getTotalRateandreview().isEmpty() && !response.body().getProviderData().get(0).getTotalRateandreview().equals("0")) {
                                assetRatingCount.setVisibility(View.VISIBLE);
                                assetRating.setVisibility(View.VISIBLE);
                            } else {
                                assetRatingCount.setVisibility(View.GONE);
                                assetRating.setVisibility(View.GONE);
                            }
                            ratingCount = "(" + response.body().getProviderData().get(0).getTotalRateandreview() + ")";
                            assetRatingCount.setText(ratingCount);
                            path = response.body().getProviderData().get(0).getUserId();
                            image = response.body().getProviderData().get(0).getImage();
                            healthCategoryId = response.body().getProviderData().get(0).getHealthcarecategoryId();

                            Glide
                                    .with(Essentials.applicationContext)
                                    .load(response.body().getProviderData().get(0).getImage())
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                                    .thumbnail(0.25f)
                                    .error(R.drawable.ic_profile_icon)
                                    .into(assetImage);

                            if (response.body().getRateandreviewsData() != null && !response.body().getRateandreviewsData().isEmpty()) {
//                                if (type.toLowerCase().equals("rate".toLowerCase())) {
                                rateListing.setHasFixedSize(true);
                                rateListing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                rateListing.setAdapter(new ReviewAdapter(response.body().getRateandreviewsData(), context, fragmentManager));
//                                    rateListing.setVisibility(View.VISIBLE);
//                                } else if (type.toLowerCase().equals("review".toLowerCase())) {
                                reviewsListing.setHasFixedSize(true);
                                reviewsListing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                reviewsListing.setAdapter(new ReviewAdapter(response.body().getRateandreviewsData(), context, fragmentManager));
//                                    reviewsListing.setVisibility(View.VISIBLE);
//                                } else {
//                                    Log.d("mytag", "othe rthen rate review");
//                                    reviewsListing.setVisibility(View.GONE);
//                                    rateListing.setVisibility(View.GONE);
//                                }
                            }
                            loader.setVisibility(View.GONE);
                            listingNoDataFound.setVisibility(View.GONE);
                            mainContainer.setVisibility(View.VISIBLE);
                        } else {
                            loader.setVisibility(View.GONE);
                            mainContainer.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        mainContainer.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AssetDetailsResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    mainContainer.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            mainContainer.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void setReviewsListing(ArrayList<AssetRateAndReview> data) {

    }

    private void setRateListing(ArrayList<AssetRateAndReview> data) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latlng = new LatLng(provider_lat, provider_long);
        googleMap.addMarker(new MarkerOptions()
                .position(latlng)
                .title(name));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 10));
    }

}


