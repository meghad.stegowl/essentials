package com.essentials.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.activities.Notifications;
import com.essentials.activities.Search;
import com.essentials.adapters.ChatAdapter;
import com.essentials.application.Essentials;
import com.essentials.data.models.chat.MessageHistoryData;
import com.essentials.data.models.chat.MessageHistoryResponse;
import com.essentials.data.models.chat.SendMessageResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.essentials.util.RecyclerViewPositionHelper;
import com.google.android.material.textfield.TextInputEditText;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatFragment extends Fragment {

    private static ChatFragment instance = null;
    @BindView(R.id.message_listing)
    RecyclerView messageListing;
    @BindView(R.id.user_picture)
    CircularImageView userPicture;
    @BindView(R.id.messageInput)
    TextInputEditText messageInput;
    @BindView(R.id.send_message)
    ImageView sendMessage;
    @BindView(R.id.message_container)
    LinearLayout messageContainer;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    String path, title;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.user_profile)
    CircularImageView userProfile;
    private AppManageInterface appManageInterface;
    private Boolean isCustomer;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private Activity activity;
    private int LIMIT = 50, CURRENT_PAGE = 1, LAST_PAGE = 100;
    private boolean loadingInProgress;
    private ChatAdapter chatAdapter;

    public static synchronized ChatFragment getInstance() {
        return instance;
    }

    public static synchronized ChatFragment newInstance() {
        return instance = new ChatFragment();
    }

    public void setNoDataFound() {
        noDataFound.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.search)
    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @OnClick(R.id.send_message)
    void onSendMessageClicked() {
        if (messageInput.getText() == null) {
            messageInput.requestFocus();
            AppUtil.show_Snackbar(context, messageContainer, getResources().getString(R.string.message_empty), false);
        } else if (messageInput.getText().toString().trim().length() == 0) {
            messageInput.requestFocus();
            AppUtil.show_Snackbar(context, messageContainer, getResources().getString(R.string.message_empty), false);
        } else {
            sendMessage(messageInput.getText().toString());
            messageInput.getText().clear();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        activity = getActivity();
        init();
    }

    private void init() {
        if (getArguments() != null) {
            Log.d("OkHttpClient","isCustomerisCustomer is-------------------"+isCustomer);
            isCustomer = getArguments().getBoolean("isCustomer");
            path = getArguments().getString("path");
            title = getArguments().getString("title");
        }
        messageListing.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        loader.setVisibility(View.GONE);
        messageListing.setVisibility(View.GONE);
        progressbar.setVisibility(View.GONE);

        Glide
                .with(Essentials.applicationContext)
                .load(Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, ""))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                .thumbnail(0.25f)
                .error(R.drawable.ic_profile_icon)
                .into(userPicture);

        messageListing.setHasFixedSize(true);
        messageListing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        chatAdapter = new ChatAdapter(new ArrayList<>(), context, fragmentManager);
        messageListing.setAdapter(chatAdapter);
        chatAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                super.onItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                messageListing.smoothScrollToPosition(chatAdapter.getItemCount());
            }
        });

        messageListing.setVisibility(View.VISIBLE);

        RecyclerViewPositionHelper positionHelper = RecyclerViewPositionHelper.createHelper(messageListing);

        messageListing.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy < 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) messageListing.getLayoutManager();
                    if (layoutManager != null) {
                        int currentFirstItem = positionHelper.findFirstCompletelyVisibleItemPosition() + 1;

                        if (currentFirstItem <= 1) {
                            if (!loadingInProgress && CURRENT_PAGE < LAST_PAGE) {
                                progressbar.setVisibility(View.VISIBLE);
                                loadingInProgress = true;
                                CURRENT_PAGE++;
                                getMessageHistory();
                            }
                        }
                    }
                }
            }
        });

        messageInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                ((InputMethodManager) Objects.requireNonNull(context.getSystemService(Activity.INPUT_METHOD_SERVICE))).hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (messageInput.getText() == null) {
                    messageInput.requestFocus();
                    AppUtil.show_Snackbar(context, messageContainer, getResources().getString(R.string.message_empty), false);
                } else if (messageInput.getText().toString().trim().length() == 0) {
                    messageInput.requestFocus();
                    AppUtil.show_Snackbar(context, messageContainer, getResources().getString(R.string.message_empty), false);
                } else {
                    sendMessage(messageInput.getText().toString());
                    messageInput.getText().clear();
                }
            }
            return false;
        });

        getMessageHistory();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void sendMessage(String message) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.GONE);
            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("from_user", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
//                jsonObject.put("to_user", path);
//                jsonObject.put("msg", message);
//            } catch (JSONException e) {
//                loader.setVisibility(View.GONE);
//                e.printStackTrace();
//            }

            if (path != Prefs.getPrefInstance().getValue(context, Const.USER_ID, "")) {
                Log.d("mytag", "from listed true");
                jsonObject = new JSONObject();
                try {
                    jsonObject.put("from_user", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    jsonObject.put("to_user", path);
                    jsonObject.put("msg", message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                jsonObject = new JSONObject();
                try {
                    jsonObject.put("from_user", path);
                    jsonObject.put("to_user", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                    jsonObject.put("msg", message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            String params = jsonObject.toString();
            final RequestBody send_message = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().send_message(send_message).enqueue(new Callback<SendMessageResponse>() {
                @Override
                public void onResponse(@NonNull Call<SendMessageResponse> call, @NonNull Response<SendMessageResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            MessageHistoryData data = new MessageHistoryData();
                            data.setDate(response.body().getDate());
                            data.setDisplayDate(response.body().getDisplayDate());
                            data.setDisplayTime(response.body().getDisplayTime());
                            data.setMessage(response.body().getMessage());
                            data.setMId(response.body().getMId());
                            data.setSendUserId(response.body().getSendUserId());
                            chatAdapter.add(data);
                            messageListing.setVisibility(View.VISIBLE);
                            noDataFound.setVisibility(View.GONE);
                            loader.setVisibility(View.GONE);
                        }
                        else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                    else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SendMessageResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    private void getMessageHistory() {
        if (AppUtil.isInternetAvailable(context)) {
            if (CURRENT_PAGE == 1) {
                if (chatAdapter != null) chatAdapter.clear();
                loader.setVisibility(View.VISIBLE);
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("from_user", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                jsonObject.put("to_user", path);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_message_history = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_message_history(LIMIT, CURRENT_PAGE, get_message_history).enqueue(new Callback<MessageHistoryResponse>() {
                @Override
                public void onResponse(@NonNull Call<MessageHistoryResponse> call, @NonNull Response<MessageHistoryResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                progressbar.setVisibility(View.GONE);
                                loadingInProgress = false;
                                LAST_PAGE = response.body().getLastPage();
                                messageListing.setVisibility(View.VISIBLE);
                                Glide
                                        .with(Essentials.applicationContext)
                                        .load(response.body().getToUserImage())
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                                        .thumbnail(0.25f)
                                        .error(R.drawable.ic_profile_icon)
                                        .into(userProfile);
                                userName.setText(title);

                                if (CURRENT_PAGE == 1) {
                                    chatAdapter.add(response.body().getData(), true);
                                    loader.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.GONE);
                                } else {
                                    chatAdapter.add(response.body().getData(), false);
                                }
                            } else {
                                progressbar.setVisibility(View.GONE);
                                loadingInProgress = false;
                                if (CURRENT_PAGE == 1) {
                                    loader.setVisibility(View.GONE);
                                    messageListing.setVisibility(View.INVISIBLE);
                                    noDataFound.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            progressbar.setVisibility(View.GONE);
                            loadingInProgress = false;
                            if (CURRENT_PAGE == 1) {
                                loader.setVisibility(View.GONE);
                                messageListing.setVisibility(View.INVISIBLE);
                                noDataFound.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        progressbar.setVisibility(View.GONE);
                        loadingInProgress = false;
                        if (CURRENT_PAGE == 1) {
                            loader.setVisibility(View.GONE);
                            messageListing.setVisibility(View.INVISIBLE);
                            noDataFound.setVisibility(View.VISIBLE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MessageHistoryResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    progressbar.setVisibility(View.GONE);
                    loadingInProgress = false;
                    if (CURRENT_PAGE == 1) {
                        loader.setVisibility(View.GONE);
                        messageListing.setVisibility(View.INVISIBLE);
                        noDataFound.setVisibility(View.VISIBLE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }
            });
        } else {
            progressbar.setVisibility(View.GONE);
            loadingInProgress = false;
            if (CURRENT_PAGE == 1) {
                loader.setVisibility(View.GONE);
                messageListing.setVisibility(View.INVISIBLE);
                noDataFound.setVisibility(View.VISIBLE);
                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                final AlertDialog dialog = new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setView(dialog_view)
                        .show();

                if (dialog.getWindow() != null)
                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                    dialog.dismiss();
                });
            }
        }

    }

    public void gotNewMessage(MessageHistoryData data) {
        chatAdapter.add(data);
        messageListing.setVisibility(View.VISIBLE);
        noDataFound.setVisibility(View.GONE);
        loader.setVisibility(View.GONE);
    }
}


