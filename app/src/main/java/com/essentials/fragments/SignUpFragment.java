package com.essentials.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.essentials.R;
import com.essentials.activities.Login;
import com.essentials.adapters.PlaceAutoSuggestAdapter;
import com.essentials.data.models.category.CategoryResponse;
import com.essentials.data.models.category.CategoryResponseData;
import com.essentials.data.models.common.CommonResponse;
import com.essentials.data.models.location.LocationResponse;
import com.essentials.data.models.location.LocationResponseData;
import com.essentials.data.models.payment.OrderResponse;
import com.essentials.data.models.payment.PaymentResponse;
import com.essentials.data.models.user.UploadImageResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.iceteck.silicompressorr.SiliCompressor;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.stripe.android.Stripe;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputListener;
import com.stripe.android.view.CardMultilineWidget;
import com.yalantis.ucrop.UCrop;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

public class SignUpFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private static SignUpFragment instance = null;
    private static String uploaded_image = "";
    private static Stripe stripe;
    private static String amount;
    @BindView(R.id.back_page_patient)
    LinearLayout backPagePatient;
    @BindView(R.id.header_patient)
    LinearLayout headerPatient;
    @BindView(R.id.image_loader_patient)
    ProgressBar imageLoaderPatient;
    @BindView(R.id.user_picture_patient)
    CircularImageView userPicturePatient;
    @BindView(R.id.frame_patient)
    FrameLayout framePatient;

    @BindView(R.id.first_name_patient_input)
    TextInputEditText firstNamePatientInput;
    @BindView(R.id.first_name_patient_container)
    TextInputLayout firstNamePatientContainer;
    @BindView(R.id.last_name_patient_input)
    TextInputEditText lastNamePatientInput;
    @BindView(R.id.last_name_patient_container)
    TextInputLayout lastNamePatientContainer;
    @BindView(R.id.username_patient_input)
    TextInputEditText usernamePatientInput;
    @BindView(R.id.username_patient_container)
    TextInputLayout usernamePatientContainer;
    @BindView(R.id.email_patient_input)
    TextInputEditText emailPatientInput;
    @BindView(R.id.email_patient_container)
    TextInputLayout emailPatientContainer;
    @BindView(R.id.password_patient_input)
    TextInputEditText passwordPatientInput;
    @BindView(R.id.password_patient_container)
    TextInputLayout passwordPatientContainer;
    @BindView(R.id.confirm_password_patient_input)
    TextInputEditText confirmPasswordPatientInput;
    @BindView(R.id.confirm_password_patient_container)
    TextInputLayout confirmPasswordPatientContainer;
    @BindView(R.id.country_patient)
    Spinner countryPatient;
    @BindView(R.id.country_patient_error)
    TextView countryPatientError;
    @BindView(R.id.state_patient)
    Spinner statePatient;
    @BindView(R.id.state_patient_error)
    TextView statePatientError;
    @BindView(R.id.city_patient)
    Spinner cityPatient;
    @BindView(R.id.city_patient_error)
    TextView cityPatientError;
    @BindView(R.id.zip_code_patient_input)
    TextInputEditText zipCodePatientInput;
    @BindView(R.id.zip_code_patient_container)
    TextInputLayout zipCodePatientContainer;
    @BindView(R.id.phone_patient_input)
    TextInputEditText phonePatientInput;
    @BindView(R.id.phone_patient_container)
    TextInputLayout phonePatientContainer;
    @BindView(R.id.create_profile_patient)
    MaterialCheckBox createProfilePatient;
    @BindView(R.id.page_patient)
    LinearLayout pagePatient;
    @BindView(R.id.back_page_one)
    LinearLayout backPageOne;
    @BindView(R.id.header_page_one)
    LinearLayout headerPageOne;
    @BindView(R.id.image_loader)
    ProgressBar imageLoader;
    @BindView(R.id.user_picture)
    CircularImageView userPicture;
    @BindView(R.id.frame)
    FrameLayout frame;
    @BindView(R.id.select_picture)
    ImageView selectPicture;
    @BindView(R.id.select_picture_patient)
    ImageView selectPicturePatient;
    @BindView(R.id.first_name_input)
    TextInputEditText firstNameInput;
    @BindView(R.id.first_name_container)
    TextInputLayout firstNameContainer;
    @BindView(R.id.last_name_input)
    TextInputEditText lastNameInput;
    @BindView(R.id.last_name_container)
    TextInputLayout lastNameContainer;
    @BindView(R.id.title_input)
    TextInputEditText titleInput;
    @BindView(R.id.username_input)
    TextInputEditText usernameInput;
    @BindView(R.id.username_container)
    TextInputLayout usernameContainer;
    @BindView(R.id.email_input)
    TextInputEditText emailInput;
    @BindView(R.id.email_container)
    TextInputLayout emailContainer;
    @BindView(R.id.language_input)
    TextInputEditText languageInput;
    @BindView(R.id.password_input)
    TextInputEditText passwordInput;
    @BindView(R.id.password_container)
    TextInputLayout passwordContainer;
    @BindView(R.id.confirm_password_input)
    TextInputEditText confirmPasswordInput;
    @BindView(R.id.confirm_password_container)
    TextInputLayout confirmPasswordContainer;
    @BindView(R.id.categories_business)
    Spinner categoriesBusiness;
    @BindView(R.id.categories_business_error)
    TextView categoriesBusinessError;
    @BindView(R.id.service_input)
    TextInputEditText serviceInput;
    @BindView(R.id.service_container)
    TextInputLayout serviceContainer;
    @BindView(R.id.continue_text)
    MaterialCheckBox continueText;
    @BindView(R.id.layout_container)
    LinearLayout layoutContainer;
    @BindView(R.id.page_one)
    LinearLayout pageOne;
    @BindView(R.id.back_page_two)
    LinearLayout backPageTwo;
    @BindView(R.id.header_business)
    LinearLayout headerBusiness;
    @BindView(R.id.business_name_input)
    TextInputEditText businessNameInput;
    @BindView(R.id.business_name_container)
    TextInputLayout businessNameContainer;
    @BindView(R.id.country_business)
    Spinner countryBusiness;
    @BindView(R.id.country_business_error)
    TextView countryBusinessError;
    @BindView(R.id.state_business)
    Spinner stateBusiness;
    @BindView(R.id.state_business_error)
    TextView stateBusinessError;
    @BindView(R.id.city_business)
    Spinner cityBusiness;
    @BindView(R.id.city_business_error)
    TextView cityBusinessError;
    @BindView(R.id.zip_code_business_input)
    TextInputEditText zipCodeBusinessInput;
    @BindView(R.id.zip_code_business_container)
    TextInputLayout zipCodeBusinessContainer;
    @BindView(R.id.phone_input)
    TextInputEditText phoneInput;
    @BindView(R.id.phone_container)
    TextInputLayout phoneContainer;
    @BindView(R.id.monday)
    MaterialCheckBox monday;
    @BindView(R.id.tuesday)
    MaterialCheckBox tuesday;
    @BindView(R.id.wednesday)
    MaterialCheckBox wednesday;
    @BindView(R.id.thursday)
    MaterialCheckBox thursday;
    @BindView(R.id.friday)
    MaterialCheckBox friday;
    @BindView(R.id.saturday)
    MaterialCheckBox saturday;
    @BindView(R.id.sunday)
    MaterialCheckBox sunday;
    @BindView(R.id.mon_start_time)
    TextView monStartTime;
    @BindView(R.id.mon_end_time)
    TextView monEndTime;
    @BindView(R.id.mon_time_container)
    LinearLayout monTimeContainer;
    @BindView(R.id.mon_closed)
    TextView monClosed;
    @BindView(R.id.tue_start_time)
    TextView tueStartTime;
    @BindView(R.id.tue_end_time)
    TextView tueEndTime;
    @BindView(R.id.tue_time_container)
    LinearLayout tueTimeContainer;
    @BindView(R.id.tue_closed)
    TextView tueClosed;
    @BindView(R.id.wed_start_time)
    TextView wedStartTime;
    @BindView(R.id.wed_end_time)
    TextView wedEndTime;
    @BindView(R.id.wed_time_container)
    LinearLayout wedTimeContainer;
    @BindView(R.id.wed_closed)
    TextView wedClosed;
    @BindView(R.id.thu_start_time)
    TextView thuStartTime;
    @BindView(R.id.thu_end_time)
    TextView thuEndTime;
    @BindView(R.id.thu_time_container)
    LinearLayout thuTimeContainer;
    @BindView(R.id.thu_closed)
    TextView thuClosed;
    @BindView(R.id.fri_start_time)
    TextView friStartTime;
    @BindView(R.id.fri_end_time)
    TextView friEndTime;
    @BindView(R.id.fri_time_container)
    LinearLayout friTimeContainer;
    @BindView(R.id.fri_closed)
    TextView friClosed;
    @BindView(R.id.sat_start_time)
    TextView satStartTime;
    @BindView(R.id.sat_end_time)
    TextView satEndTime;
    @BindView(R.id.sat_time_container)
    LinearLayout satTimeContainer;
    @BindView(R.id.sat_closed)
    TextView satClosed;
    @BindView(R.id.sun_start_time)
    TextView sunStartTime;
    @BindView(R.id.sun_end_time)
    TextView sunEndTime;
    @BindView(R.id.sun_time_container)
    LinearLayout sunTimeContainer;
    @BindView(R.id.sun_closed)
    TextView sunClosed;
    @BindView(R.id.create_profile_business)
    MaterialCheckBox createProfileBusiness;
    @BindView(R.id.page_two)
    LinearLayout pageTwo;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.main_container)
    CoordinatorLayout mainContainer;
    ArrayAdapter<String> countryAdapter;
    ArrayAdapter<String> cityAdapter;
    ArrayAdapter<String> stateAdapter;
    ArrayAdapter<String> categoryAdapter;
    ArrayList<String> countryList = new ArrayList<>();
    ArrayList<String> cityList = new ArrayList<>();
    ArrayList<String> statesList = new ArrayList<>();
    ArrayList<String> categoryList = new ArrayList<>();
    List<LocationResponseData> countryResponse = new ArrayList<>();
    List<LocationResponseData> stateResponse = new ArrayList<>();
    List<LocationResponseData> cityResponse = new ArrayList<>();
    List<CategoryResponseData> categoryResponse = new ArrayList<>();
    int patient_city_id, patient_state_id, business_city_id, business_state_id, business_categories_id, patient_country_id, business_country_id;
    int monday_selection, tuesday_selection, wednesday_selection, thursday_selection, friday_selection, saturday_selection, sunday_selection;
    Boolean isCustomer;
    String hourOfTheDay;
    int mHour, mMinute, hours;
    String startTime;
    Handler handler;
    int rollId = 1;
    @BindView(R.id.auto_complete_address_patient)
    AutoCompleteTextView autoCompleteAddressPatient;
    @BindView(R.id.patient_address_error)
    TextView patientAddressError;
    @BindView(R.id.auto_complete_address_business)
    AutoCompleteTextView autoCompleteAddressBusiness;
    @BindView(R.id.business_address_error)
    TextView businessAddressError;
    private String path;
    private String type;
    private String  pageTitle;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private Activity activity;
    private String image = "", latitude, longitude, address;
    private String showpicturedialog_title, selectfromgallerymsg, selectfromcameramsg, canceldialog;
    private final int GALLERY = 2;
    private final int CAMERA = 1;
    private Uri resultUri;
    private Bitmap bitmap;

    public static synchronized SignUpFragment getInstance() {
        return instance;
    }

    public static synchronized SignUpFragment newInstance() {
        return instance = new SignUpFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @OnClick(R.id.back_page_patient)
    public void setBackPagePatient() {
        activity.onBackPressed();
    }

    @OnClick(R.id.back_page_one)
    public void setBack() {
        activity.onBackPressed();
    }

    @OnCheckedChanged({R.id.monday, R.id.tuesday, R.id.wednesday, R.id.thursday, R.id.friday, R.id.saturday, R.id.sunday})
    public void setCheckbox(CheckBox checkbox, boolean isChecked) {
        // Check which checkbox was clicked
        if (checkbox == monday) {
            monday_selection = isChecked ? 1 : 0;
            monTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            monClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                monStartTime.setText(getResources().getString(R.string.time_ten));
                monEndTime.setText(getResources().getString(R.string.time_five));
            } else {
                monStartTime.setText("");
                monEndTime.setText("");
            }
        } else if (checkbox == tuesday) {
            tuesday_selection = isChecked ? 1 : 0;
            tueTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            tueClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                tueStartTime.setText(getResources().getString(R.string.time_ten));
                tueEndTime.setText(getResources().getString(R.string.time_five));
            } else {
                tueStartTime.setText("");
                tueEndTime.setText("");
            }
        } else if (checkbox == wednesday) {
            wednesday_selection = isChecked ? 1 : 0;
            wedTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            wedClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                wedStartTime.setText(getResources().getString(R.string.time_ten));
                wedEndTime.setText(getResources().getString(R.string.time_five));
            } else {
                wedStartTime.setText("");
                wedEndTime.setText("");
            }
        } else if (checkbox == thursday) {
            thursday_selection = isChecked ? 1 : 0;
            thuTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            thuClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                thuStartTime.setText(getResources().getString(R.string.time_ten));
                thuEndTime.setText(getResources().getString(R.string.time_five));
            } else {
                thuStartTime.setText("");
                thuEndTime.setText("");
            }
        } else if (checkbox == friday) {
            friday_selection = isChecked ? 1 : 0;
            friTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            friClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                friStartTime.setText(getResources().getString(R.string.time_ten));
                friEndTime.setText(getResources().getString(R.string.time_five));
            } else {
                friStartTime.setText("");
                friEndTime.setText("");
            }
        } else if (checkbox == saturday) {
            saturday_selection = isChecked ? 1 : 0;
            satTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            satClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                satStartTime.setText(getResources().getString(R.string.time_ten));
                satEndTime.setText(getResources().getString(R.string.time_five));
            } else {
                satStartTime.setText("");
                satEndTime.setText("");
            }
        } else {
            sunday_selection = isChecked ? 1 : 0;
            sunTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            sunClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                sunStartTime.setText(getResources().getString(R.string.time_ten));
                sunEndTime.setText(getResources().getString(R.string.time_five));
            } else {
                sunStartTime.setText("");
                sunEndTime.setText("");
            }
        }
    }

    @OnClick({R.id.mon_start_time, R.id.mon_end_time, R.id.tue_start_time, R.id.tue_end_time, R.id.wed_start_time, R.id.wed_end_time,
            R.id.thu_start_time, R.id.thu_end_time, R.id.fri_start_time, R.id.fri_end_time, R.id.sat_start_time, R.id.sat_end_time,
            R.id.sun_start_time, R.id.sun_end_time})
    public void pickDoor(TextView textView) {

        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> {
            String formatTime = hourOfDay + ":" + minute;
            SimpleDateFormat fmt = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            Date date = null;
            try {
                date = fmt.parse(formatTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
            String formattedTime = fmtOut.format(date);
            startTime = formattedTime;
            textView.setText(formattedTime);
        }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @OnClick(R.id.back_page_two)
    public void setBackBusiness() {
        pagePatient.setVisibility(View.GONE);
        pageOne.setVisibility(View.VISIBLE);
        pageTwo.setVisibility(View.GONE);
    }

    @OnClick(R.id.create_profile_patient)
    public void onCreateProfile() {
        if (firstNamePatientInput.getText() == null) {
            firstNamePatientContainer.setError(getResources().getString(R.string.first_name_empty));
        } else if (firstNamePatientInput.getText().toString().trim().length() == 0) {
            firstNamePatientContainer.setError(getResources().getString(R.string.first_name_empty));
        } else if (lastNamePatientInput.getText() == null) {
            lastNamePatientContainer.setError(getResources().getString(R.string.last_name_empty));
        } else if (lastNamePatientInput.getText().toString().trim().length() == 0) {
            lastNamePatientContainer.setError(getResources().getString(R.string.last_name_empty));
        } else if (usernamePatientInput.getText() == null) {
            usernamePatientContainer.setError(getResources().getString(R.string.username_empty));
        } else if (usernamePatientInput.getText().toString().trim().length() == 0) {
            usernamePatientContainer.setError(getResources().getString(R.string.username_empty));
        } else if (emailPatientInput.getText() == null) {
            emailPatientContainer.setError(getResources().getString(R.string.email_empty));
        } else if (emailPatientInput.getText().toString().trim().length() == 0) {
            emailPatientContainer.setError(getResources().getString(R.string.email_empty));
        } else if (!TextUtils.isDigitsOnly(emailPatientInput.getText()) && !Patterns.EMAIL_ADDRESS.matcher(emailPatientInput.getText()).matches()) {
            emailPatientContainer.setError(getResources().getString(R.string.email_not_valid));
        } else if (passwordPatientInput.getText() == null) {
            passwordPatientContainer.setError(getResources().getString(R.string.password_empty));
        } else if (passwordPatientInput.getText().toString().trim().length() == 0) {
            passwordPatientContainer.setError(getResources().getString(R.string.password_empty));
        } else if (confirmPasswordPatientInput.getText() == null) {
            confirmPasswordPatientContainer.setError(getResources().getString(R.string.confirm_password_empty));
        } else if (confirmPasswordPatientInput.getText().toString().trim().length() == 0) {
            confirmPasswordPatientContainer.setError(getResources().getString(R.string.confirm_password_empty));
        } else if (!confirmPasswordPatientInput.getText().toString().equals(passwordPatientInput.getText().toString())) {
            confirmPasswordPatientContainer.setError(getResources().getString(R.string.confirm_password_not_valid));
        } else if (autoCompleteAddressPatient.getText() == null) {
            patientAddressError.setVisibility(View.VISIBLE);
            patientAddressError.setText(getResources().getString(R.string.address_empty));
        } else if (autoCompleteAddressPatient.getText().toString().trim().length() == 0) {
            patientAddressError.setVisibility(View.VISIBLE);
            patientAddressError.setText(getResources().getString(R.string.address_empty));
        } else if (zipCodePatientInput.getText() == null) {
            zipCodePatientContainer.setError(getResources().getString(R.string.zip_code_empty));
        } else if (zipCodePatientInput.getText().toString().trim().length() == 0) {
            zipCodePatientContainer.setError(getResources().getString(R.string.zip_code_empty));
        } else if (zipCodePatientInput.getText().toString().trim().length() < 5) {
            zipCodePatientContainer.setError(getResources().getString(R.string.zip_code_incorrect));
        } else if (phonePatientInput.getText() == null) {
            phonePatientContainer.setError(getResources().getString(R.string.phone_number_empty));
        } else if (phonePatientInput.getText().toString().trim().length() == 0) {
            phonePatientContainer.setError(getResources().getString(R.string.phone_number_empty));
        } else if (phonePatientInput.getText().toString().trim().length() < 12) {
            phonePatientContainer.setError(getResources().getString(R.string.phone_number_incorrect));
        } else if (countryPatient.getSelectedItemPosition() == 0) {
            countryPatientError.setVisibility(View.VISIBLE);
            countryPatientError.setText(getResources().getString(R.string.please_select_country));
        } else if (statePatient.getSelectedItemPosition() == 0) {
            statePatientError.setVisibility(View.VISIBLE);
            statePatientError.setText(getResources().getString(R.string.please_select_state));
        } else if (cityPatient.getSelectedItemPosition() == 0) {
            cityPatientError.setVisibility(View.VISIBLE);
            cityPatientError.setText(getResources().getString(R.string.please_select_city));
        } else {
            usernameInput.clearFocus();
            firstNameInput.clearFocus();
            lastNameInput.clearFocus();
            titleInput.clearFocus();
            emailPatientInput.clearFocus();
            passwordInput.clearFocus();
            zipCodePatientInput.clearFocus();
            confirmPasswordInput.clearFocus();
            String image = Prefs.getPrefInstance().getValue(context, Const.SET_PROFILE_IMAGE, "");
//            if (image.equals("")) {
            if (resultUri == null){
                Log.d("mytag","here is address is--------------------------"+autoCompleteAddressPatient.getText().toString());
                callRegisterPatient(firstNamePatientInput.getText().toString(), lastNamePatientInput.getText().toString(),
                        autoCompleteAddressPatient.getText().toString(), usernamePatientInput.getText().toString(),
                        emailPatientInput.getText().toString(), passwordPatientInput.getText().toString(), confirmPasswordPatientInput.getText().toString(),
                        phonePatientInput.getText().toString(), zipCodePatientInput.getText().toString(), countryPatient.getSelectedItem().toString(),
                        statePatient.getSelectedItem().toString(), cityPatient.getSelectedItem().toString(),image);

            } else {
                uploadProfilePicture(/*image,*/ firstNamePatientInput.getText().toString(), lastNamePatientInput.getText().toString()
                        ,titleInput.getText().toString(),businessNameInput.getText().toString(),
                        serviceInput.getText().toString(), autoCompleteAddressPatient.getText().toString(), usernamePatientInput.getText().toString(),
                        emailPatientInput.getText().toString(),languageInput.getText().toString(), passwordPatientInput.getText().toString(), confirmPasswordPatientInput.getText().toString(), phonePatientInput.getText().toString(),
                        zipCodePatientInput.getText().toString(), "", "", "",
                        "", "", "", "", "", "",
                        "", "", "", "", "", countryPatient.getSelectedItem().toString(),
                        statePatient.getSelectedItem().toString(), cityPatient.getSelectedItem().toString(),
                        new File(getRealPathFromURIPath(resultUri, getActivity())), resultUri);
            }

        }
    }

    @OnClick(R.id.create_profile_business)
    public void onCreateProfileBusiness() {
        if (businessNameInput.getText() == null) {
            businessNameContainer.setError(getResources().getString(R.string.business_name_empty));
        } else if (businessNameInput.getText().toString().trim().length() == 0) {
            businessNameContainer.setError(getResources().getString(R.string.business_name_empty));
        } else if (autoCompleteAddressBusiness.getText() == null) {
            businessAddressError.setVisibility(View.VISIBLE);
            businessAddressError.setText(getResources().getString(R.string.address_empty));
        } else if (autoCompleteAddressBusiness.getText().toString().trim().length() == 0) {
            businessAddressError.setVisibility(View.VISIBLE);
            businessAddressError.setText(getResources().getString(R.string.address_empty));
        } else if (countryBusiness.getSelectedItemPosition() == 0) {
            countryBusinessError.setVisibility(View.VISIBLE);
            countryBusinessError.setText(getResources().getString(R.string.please_select_country));
        } else if (stateBusiness.getSelectedItemPosition() == 0) {
            stateBusinessError.setVisibility(View.VISIBLE);
            stateBusinessError.setText(getResources().getString(R.string.please_select_state));
        } else if (cityBusiness.getSelectedItemPosition() == 0) {
            cityBusinessError.setVisibility(View.VISIBLE);
            cityBusinessError.setText(getResources().getString(R.string.please_select_city));
        } else if (zipCodeBusinessInput.getText() == null) {
            zipCodeBusinessContainer.setError(getResources().getString(R.string.zip_code_empty));
        } else if (zipCodeBusinessInput.getText().toString().trim().length() == 0) {
            zipCodeBusinessContainer.setError(getResources().getString(R.string.zip_code_empty));
        } else if (zipCodeBusinessInput.getText().toString().trim().length() < 5) {
            zipCodeBusinessContainer.setError(getResources().getString(R.string.zip_code_incorrect));
        } else if (phoneInput.getText() == null) {
            phoneContainer.setError(getResources().getString(R.string.phone_number_empty));
        } else if (phoneInput.getText().toString().trim().length() == 0) {
            phoneContainer.setError(getResources().getString(R.string.phone_number_empty));
        } else {
            if (!monday.isChecked()) {
                monStartTime.setText("");
                monEndTime.setText("");
            }
            if (!tuesday.isChecked()) {
                tueStartTime.setText("");
                tueEndTime.setText("");
            }
            if (!wednesday.isChecked()) {
                wedStartTime.setText("");
                wedEndTime.setText("");
            }
            if (!thursday.isChecked()) {
                thuStartTime.setText("");
                thuEndTime.setText("");
            }
            if (!friday.isChecked()) {
                friStartTime.setText("");
                friEndTime.setText("");
            }
            if (!saturday.isChecked()) {
                satStartTime.setText("");
                satEndTime.setText("");
            }
            if (!sunday.isChecked()) {
                sunStartTime.setText("");
                sunEndTime.setText("");
            }
            emailInput.clearFocus();
            firstNameInput.clearFocus();
            lastNameInput.clearFocus();
            titleInput.clearFocus();
            passwordInput.clearFocus();
            languageInput.clearFocus();
            confirmPasswordInput.clearFocus();
            businessNameInput.clearFocus();
            autoCompleteAddressBusiness.clearFocus();
            zipCodeBusinessInput.clearFocus();
            String image = Prefs.getPrefInstance().getValue(context, Const.SET_PROFILE_IMAGE, "");
//            if (image.equals("")) {
            if (resultUri == null){
                callRegisterBusiness(firstNameInput.getText().toString(), lastNameInput.getText().toString(),titleInput.getText().toString(),
                        businessNameInput.getText().toString(),serviceInput.getText().toString(),autoCompleteAddressBusiness.getText().toString(),
                        usernameInput.getText().toString(),emailInput.getText().toString(),languageInput.getText().toString(),
                        passwordInput.getText().toString(), confirmPasswordInput.getText().toString(), phoneInput.getText().toString(),
                        zipCodeBusinessInput.getText().toString(), monStartTime.getText().toString(), monEndTime.getText().toString(), tueStartTime.getText().toString(),
                        tueEndTime.getText().toString(), wedStartTime.getText().toString(), wedEndTime.getText().toString(), thuStartTime.getText().toString(),
                        thuEndTime.getText().toString(), friStartTime.getText().toString(), friEndTime.getText().toString(), satStartTime.getText().toString(),
                        satEndTime.getText().toString(), sunStartTime.getText().toString(), sunEndTime.getText().toString(), countryBusiness.getSelectedItem().toString(),
                        stateBusiness.getSelectedItem().toString(), cityBusiness.getSelectedItem().toString(),image);
            } else {
                uploadProfilePicture(/*image,*/ firstNameInput.getText().toString(), lastNameInput.getText().toString(),titleInput.getText().toString(), businessNameInput.getText().toString(),
                        serviceInput.getText().toString(), autoCompleteAddressBusiness.getText().toString(), usernameInput.getText().toString(),
                        emailInput.getText().toString(),languageInput.getText().toString(), passwordInput.getText().toString(), confirmPasswordInput.getText().toString(), phoneInput.getText().toString(),
                        zipCodeBusinessInput.getText().toString(), monStartTime.getText().toString(), monEndTime.getText().toString(), tueStartTime.getText().toString(),
                        tueEndTime.getText().toString(), wedStartTime.getText().toString(), wedEndTime.getText().toString(), thuStartTime.getText().toString(),
                        thuEndTime.getText().toString(), friStartTime.getText().toString(), friEndTime.getText().toString(), satStartTime.getText().toString(),
                        satEndTime.getText().toString(), sunStartTime.getText().toString(), sunEndTime.getText().toString(), countryBusiness.getSelectedItem().toString(),
                        stateBusiness.getSelectedItem().toString(), cityBusiness.getSelectedItem().toString(),
                        new File(getRealPathFromURIPath(resultUri, getActivity())), resultUri);
            }
        }
    }

    @OnClick(R.id.continue_text)
    public void onContinueProfile() {
        if (firstNameInput.getText() == null) {
            firstNameContainer.setError(getResources().getString(R.string.first_name_empty));
        } else if (firstNameInput.getText().toString().trim().length() == 0) {
            firstNameContainer.setError(getResources().getString(R.string.first_name_empty));
        } else if (lastNameInput.getText() == null) {
            lastNameContainer.setError(getResources().getString(R.string.last_name_empty));
        } else if (lastNameInput.getText().toString().trim().length() == 0) {
            lastNameContainer.setError(getResources().getString(R.string.last_name_empty));
        } else if (usernameInput.getText() == null) {
            usernameContainer.setError(getResources().getString(R.string.username_empty));
        } else if (usernameInput.getText().toString().trim().length() == 0) {
            usernameContainer.setError(getResources().getString(R.string.username_empty));
        } else if (emailInput.getText() == null) {
            emailContainer.setError(getResources().getString(R.string.email_empty));
        } else if (emailInput.getText().toString().trim().length() == 0) {
            emailContainer.setError(getResources().getString(R.string.email_empty));
        } else if (!TextUtils.isDigitsOnly(emailInput.getText()) && !Patterns.EMAIL_ADDRESS.matcher(emailInput.getText()).matches()) {
            emailContainer.setError(getResources().getString(R.string.email_not_valid));
        } else if (passwordInput.getText() == null) {
            passwordContainer.setError(getResources().getString(R.string.password_empty));
        } else if (passwordInput.getText().toString().trim().length() == 0) {
            passwordContainer.setError(getResources().getString(R.string.password_empty));
        } else if (confirmPasswordInput.getText() == null) {
            confirmPasswordContainer.setError(getResources().getString(R.string.confirm_password_empty));
        } else if (confirmPasswordInput.getText().toString().trim().length() == 0) {
            confirmPasswordContainer.setError(getResources().getString(R.string.confirm_password_empty));
        } else if (!confirmPasswordInput.getText().toString().equals(passwordInput.getText().toString())) {
            confirmPasswordContainer.setError(getResources().getString(R.string.confirm_password_not_valid));
        } else if (categoriesBusiness.getSelectedItemPosition() == 0) {
            categoriesBusinessError.setVisibility(View.VISIBLE);
            categoriesBusinessError.setText(getResources().getString(R.string.please_select_category));
        } else if (serviceInput.getText() == null) {
            serviceContainer.setError(getResources().getString(R.string.service_empty));
        } else if (serviceInput.getText().toString().trim().length() == 0) {
            serviceContainer.setError(getResources().getString(R.string.service_empty));
        } else {
            usernameInput.clearFocus();
            firstNameInput.clearFocus();
            lastNameInput.clearFocus();
            titleInput.clearFocus();
            passwordInput.clearFocus();
            serviceInput.clearFocus();
            confirmPasswordInput.clearFocus();
            pagePatient.setVisibility(View.GONE);
            pageOne.setVisibility(View.GONE);
            pageTwo.setVisibility(View.VISIBLE);

        }
    }

    @OnClick({R.id.select_picture, R.id.select_picture_patient})
    public void pickImage(ImageView imageView) {
        firstNameInput.clearFocus();
        lastNameInput.clearFocus();
        titleInput.clearFocus();
        usernameInput.clearFocus();
        passwordInput.clearFocus();
        confirmPasswordInput.clearFocus();

        requestMultiplePermissions();
        showpicturedialog_title = "Select the Action";
        selectfromgallerymsg = "Select photo from Gallery";
        selectfromcameramsg = "Capture photo from Camera";
        canceldialog = "Cancel";
        showPictureDialog_chooser();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);
        activity = getActivity();

        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
            isCustomer = getArguments().getBoolean("isCustomer");
        }
        init();
    }

    private void init() {
        loader.setVisibility(View.GONE);
        statePatient.setOnItemSelectedListener(this);
        cityPatient.setOnItemSelectedListener(this);
        stateBusiness.setOnItemSelectedListener(this);
        cityBusiness.setOnItemSelectedListener(this);
        categoriesBusiness.setOnItemSelectedListener(this);
        countryPatient.setOnItemSelectedListener(this);
        countryBusiness.setOnItemSelectedListener(this);
        handler = new Handler();
        rollId = 1;
        scrollView.scrollTo(0, 0);
        autoCompleteAddressPatient.setAdapter(new PlaceAutoSuggestAdapter(context, android.R.layout.simple_list_item_1));
        autoCompleteAddressBusiness.setAdapter(new PlaceAutoSuggestAdapter(context, android.R.layout.simple_list_item_1));

        if (isCustomer) {
            pagePatient.setVisibility(View.VISIBLE);
            pageOne.setVisibility(View.GONE);
        } else {
            pagePatient.setVisibility(View.GONE);
            pageOne.setVisibility(View.VISIBLE);
        }
        pageTwo.setVisibility(View.GONE);
        if (isCustomer) {
            latitude = "-34";
            longitude = "151";
        } else {
            latitude = "33.990873";
            longitude = "150.991734";
        }

        getCountry();
        getHealthCareCategory();

        usernameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                usernameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        usernamePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                usernamePatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        autoCompleteAddressPatient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                patientAddressError.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        autoCompleteAddressBusiness.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                businessAddressError.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        firstNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                firstNameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        firstNamePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                firstNamePatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lastNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                lastNameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lastNamePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                lastNamePatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordPatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordPatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        confirmPasswordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                confirmPasswordContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        confirmPasswordPatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                confirmPasswordPatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        zipCodePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                zipCodePatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        zipCodeBusinessInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                zipCodeBusinessContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        phoneInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                phoneContainer.setError("");
                if (s.length() > 1 && !s.toString().substring(s.length() - 1).equals("-") && (s.length() == 3 || s.length() == 7) && phoneInput.getText() != null)
                    phoneInput.getText().append("-");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        phonePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                phonePatientContainer.setError("");
                if (s.length() > 1 && !s.toString().substring(s.length() - 1).equals("-") && (s.length() == 3 || s.length() == 7) && phonePatientInput.getText() != null)
                    phonePatientInput.getText().append("-");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        emailPatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailPatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        businessNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                businessNameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        serviceInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                serviceContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // handle result of pick image chooser
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(context, data);
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(context, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//                mCropImageUri = imageUri;
//                //requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
//            } else {
//                // no permissions required or already granted, can start crop image activity
//                startCropImageActivity(imageUri);
//            }
//        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                Uri resultUri = result.getUri();
//                image = SiliCompressor.with(context).compress(resultUri.getPath(), Environment.getExternalStoragePublicDirectory("/" + getResources().getString(R.string.app_name) + "/Images/"));
//                File imageFile = new File(image);
//                userPicture.setImageURI(Uri.fromFile(imageFile));
//                userPicturePatient.setImageURI(Uri.fromFile(imageFile));
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Exception error = result.getError();
//                error.printStackTrace();
//            }
//        }
//    }
//
//    private void startCropImageActivity(Uri imageUri) {
//        CropImage.activity(imageUri)
//                .setCropShape(CropImageView.CropShape.RECTANGLE)
//                .start(context, SignUpFragment.this);
//    }
//
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                CropImage.startPickImageActivity(context, SignUpFragment.this);
//            } else {
//                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
//                final AlertDialog dialog = new AlertDialog.Builder(context)
//                        .setCancelable(false)
//                        .setView(dialog_view)
//                        .show();
//
//                if (dialog.getWindow() != null)
//                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);
//
//                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.cancel_image_pick));
//                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
//                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
//
//                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
//            }
//        }
//        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
//            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // required permissions granted, start crop image activity
//                startCropImageActivity(mCropImageUri);
//            } else {
//                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
//                final AlertDialog dialog = new AlertDialog.Builder(context)
//                        .setCancelable(false)
//                        .setView(dialog_view)
//                        .show();
//
//                if (dialog.getWindow() != null)
//                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);
//
//                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.cancel_image_pick));
//                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
//                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
//
//                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
//            }
//        }
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (isCustomer) {
            if (view == countryPatient.getSelectedView()) {
                if (countryPatient.getSelectedItemPosition() != 0) {
                    patient_country_id = countryResponse.get(position - 1).getId();
                    getState(patient_country_id);
                    countryPatientError.setVisibility(View.GONE);
                }
            } else if (view == statePatient.getSelectedView()) {
                //get city
                if (statePatient.getSelectedItemPosition() != 0) {
                    patient_state_id = stateResponse.get(position - 1).getId();
                    getCity(patient_state_id);
                    statePatientError.setVisibility(View.GONE);
                }
            } else {
                if (cityPatient.getSelectedItemPosition() != 0) {
                    patient_city_id = cityResponse.get(position - 1).getId();
                    cityPatientError.setVisibility(View.GONE);
                }
            }
        } else {
            if (view == countryBusiness.getSelectedView()) {
                if (countryBusiness.getSelectedItemPosition() != 0) {
                    business_country_id = countryResponse.get(position - 1).getId();
                    getState(countryResponse.get(position - 1).getId());
                    countryBusinessError.setVisibility(View.GONE);
                }
            } else if (view == stateBusiness.getSelectedView()) {
                //get city
                if (stateBusiness.getSelectedItemPosition() != 0) {
                    business_state_id = stateResponse.get(position - 1).getId();
                    getCity(stateResponse.get(position - 1).getId());
                    stateBusinessError.setVisibility(View.GONE);
                }
            } else if (view == cityBusiness.getSelectedView()) {
                if (cityBusiness.getSelectedItemPosition() != 0) {
                    business_city_id = cityResponse.get(position - 1).getId();
                    cityBusinessError.setVisibility(View.GONE);
                }
            } else {
                if (categoriesBusiness.getSelectedItemPosition() != 0) {
                    try {
                        business_categories_id = categoryResponse.get(position - 1).getHealthcarecategoryId();
                        categoriesBusinessError.setVisibility(View.GONE);
                    }catch (Exception e){

                    }
                }
            }
        }
        ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void setCountryList(List<String> countryList) {
        countryAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, countryList);
        countryAdapter.setDropDownViewResource(R.layout.row_spinner);
        countryPatient.setAdapter(countryAdapter);
        countryBusiness.setAdapter(countryAdapter);
        int position = countryList.indexOf("United States");
        countryPatient.setSelection(position, true);
        countryBusiness.setSelection(position, true);
    }

    private void setCityList(List<String> cityList) {
        cityAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, cityList);
        cityAdapter.setDropDownViewResource(R.layout.row_spinner);
        cityPatient.setAdapter(cityAdapter);
        cityBusiness.setAdapter(cityAdapter);
    }

    private void setCategoryList(List<String> categoryList) {
        categoryAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, categoryList);
        categoryAdapter.setDropDownViewResource(R.layout.row_spinner);
        categoriesBusiness.setAdapter(categoryAdapter);
    }

    private void setStateList(List<String> stateList) {
        stateAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, stateList);
        stateAdapter.setDropDownViewResource(R.layout.row_spinner);
        statePatient.setAdapter(stateAdapter);
        stateBusiness.setAdapter(stateAdapter);
    }

    private void callRegisterPatient(String firstName, String lastName, String address, String username, String email, String password,
                                     String confirm_password, String phone, String zip_code, String country_name, String state_name,
                                     String city_name, String url) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("username", username);
                jsonObject.put("firstname", firstName);
                jsonObject.put("lastname", lastName);
                jsonObject.put("email", email);
                jsonObject.put("password", password);
                jsonObject.put("confirm_password", confirm_password);
                jsonObject.put("address", address);
                jsonObject.put("country", country_name);
                jsonObject.put("state", state_name);
                jsonObject.put("city", city_name);
                jsonObject.put("phone", phone);
                jsonObject.put("zipcode", zip_code);
//                jsonObject.put("image", uploaded_image);
                jsonObject.put("image", url);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody user_registering = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().user_registering(getResources().getString(R.string.customers), user_registering).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
//                            Prefs.getPrefInstance().setValue(context, Const.FCM_ID, response.body().getFcmId());
//                            Prefs.getPrefInstance().setValue(context, Const.USER_FULL_NAME, fullName);
//                            Prefs.getPrefInstance().setValue(context, Const.PHONE_NUMBER, phone);
//                            Prefs.getPrefInstance().setValue(context, Const.USER_NAME, username);
//                            Prefs.getPrefInstance().setValue(context, Const.PASSWORD, password);
//                            Prefs.getPrefInstance().setValue(context, Const.USER_DOB, birthDate);
//                            Prefs.getPrefInstance().setValue(context, Const.EMAIL, email);

                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));

                            (dialog_view.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);
                            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog.dismiss();
                                startActivity(new Intent(context, Login.class).putExtra("isCustomer", true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                getActivity().finish();
                            });
                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);


                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));

                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void callRegisterBusiness(String firstName, String lastName,String title, String businessName, String serviceDescription,
                                      String address, String username, String email, String language, String password, String confirm_password, String phone,
                                      String zip_code, String monday_start_time, String monday_end_time, String tuesday_start_time,
                                      String tuesday_end_time, String wednesday_start_time, String wednesday_end_time, String thursday_start_time, String thursday_end_time,
                                      String friday_start_time, String friday_end_time, String saturday_start_time, String saturday_end_time, String sunday_start_time,
                                      String sunday_end_time, String country_name, String state_name, String city_name, String url) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("username", username);
                jsonObject.put("firstname", firstName);
                jsonObject.put("lastname", lastName);
                jsonObject.put("title", title);
                jsonObject.put("businessname", businessName);
                jsonObject.put("email", email);
                jsonObject.put("lang", language);
                jsonObject.put("password", password);
                jsonObject.put("confirm_password", confirm_password);
                jsonObject.put("address", address);
                jsonObject.put("country", country_name);
                jsonObject.put("state", state_name);
                jsonObject.put("city", city_name);
                jsonObject.put("phone", phone);
                jsonObject.put("zipcode", zip_code);
                jsonObject.put("service_description", serviceDescription);
                jsonObject.put("healthcarecategory_id", business_categories_id);
                jsonObject.put("monday", monday_selection);
                jsonObject.put("monday_start_time", monday_start_time);
                jsonObject.put("monday_end_time", monday_end_time);
                jsonObject.put("tuesday", tuesday_selection);
                jsonObject.put("tuesday_start_time", tuesday_start_time);
                jsonObject.put("tuesday_end_time", tuesday_end_time);
                jsonObject.put("wednesday", wednesday_selection);
                jsonObject.put("wednesday_start_time", wednesday_start_time);
                jsonObject.put("wednesday_end_time", wednesday_end_time);
                jsonObject.put("thursday", thursday_selection);
                jsonObject.put("thursday_start_time", thursday_start_time);
                jsonObject.put("thursday_end_time", thursday_end_time);
                jsonObject.put("friday", friday_selection);
                jsonObject.put("friday_start_time", friday_start_time);
                jsonObject.put("friday_end_time", friday_end_time);
                jsonObject.put("saturday", saturday_selection);
                jsonObject.put("saturday_start_time", saturday_start_time);
                jsonObject.put("saturday_end_time", saturday_end_time);
                jsonObject.put("sunday", sunday_selection);
                jsonObject.put("sunday_start_time", sunday_start_time);
                jsonObject.put("sunday_end_time", sunday_end_time);
//                jsonObject.put("image", uploaded_image);
                jsonObject.put("image", url);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody user_registering = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().user_registering(getResources().getString(R.string.providers), user_registering).enqueue(new Callback<CommonResponse>() {
                @SuppressLint("StringFormatMatches")
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {

                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(String.format(getResources().getString(R.string.payment_text), Prefs.getPrefInstance().getValue(context, Const.PAYMENT_AMOUNT, "")));

                            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.pay));
                            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog.dismiss();
                                getPaymentAmount(response.body().getUser_id());
                            });

                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                startActivity(new Intent(context, Login.class).putExtra("isCustomer", false).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                getActivity().finish();
                            });
                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);


                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));

                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }


                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void getCountry() {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_country(getResources().getString(R.string.type_countries)).enqueue(new Callback<LocationResponse>() {
                @Override
                public void onResponse(@NonNull Call<LocationResponse> call, @NonNull Response<LocationResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                countryResponse = response.body().getData();
                                countryList = new ArrayList<>();
                                countryList.add("Select Country");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    countryList.add(response.body().getData().get(i).getName());
                                }

                            } else {
                                countryList = new ArrayList<>();
                                countryList.add("Select Country");

                            }
                        } else {
                            countryList = new ArrayList<>();
                            countryList.add("Select Country");

                        }
                    } else {
                        countryList = new ArrayList<>();
                        countryList.add("Select Country");

                    }
                    setCountryList(countryList);
                    statesList = new ArrayList<>();
                    statesList.add("Select State");
                    setStateList(statesList);
                    cityList = new ArrayList<>();
                    cityList.add("Select City");
                    setCityList(cityList);
                }

                @Override
                public void onFailure(@NonNull Call<LocationResponse> call, @NonNull Throwable t) {
                    countryList = new ArrayList<>();
                    countryList.add("Select Country");
                    setCountryList(countryList);

                    statesList = new ArrayList<>();
                    statesList.add("Select State");
                    setStateList(statesList);

                    cityList = new ArrayList<>();
                    cityList.add("Select City");
                    setCityList(cityList);
                }
            });
        } else {
            countryList = new ArrayList<>();
            countryList.add("Select Country");
            setCountryList(countryList);

            statesList = new ArrayList<>();
            statesList.add("Select State");
            setStateList(statesList);

            cityList = new ArrayList<>();
            cityList.add("Select City");
            setCityList(cityList);
        }
    }

    private void getState(int country_id) {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_state(getResources().getString(R.string.type_states), country_id).enqueue(new Callback<LocationResponse>() {
                @Override
                public void onResponse(@NonNull Call<LocationResponse> call, @NonNull Response<LocationResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                stateResponse = new ArrayList<>();
                                stateResponse = response.body().getData();
                                statesList = new ArrayList<>();
                                statesList.add("Select State");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    statesList.add(response.body().getData().get(i).getName());
                                }
                            } else {
                                statesList = new ArrayList<>();
                                statesList.add("Select State");
                            }
                        } else {
                            statesList = new ArrayList<>();
                            statesList.add("Select State");
                        }
                    } else {
                        statesList = new ArrayList<>();
                        statesList.add("Select State");
                    }
                    setStateList(statesList);
                }

                @Override
                public void onFailure(@NonNull Call<LocationResponse> call, @NonNull Throwable t) {
                    statesList = new ArrayList<>();
                    statesList.add("Select State");
                    setStateList(statesList);
                }
            });
        } else {
            statesList = new ArrayList<>();
            statesList.add("Select State");
            setStateList(statesList);
        }
    }

    private void getCity(int state_id) {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_city(getResources().getString(R.string.type_cities), state_id).enqueue(new Callback<LocationResponse>() {
                @Override
                public void onResponse(@NonNull Call<LocationResponse> call, @NonNull Response<LocationResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                cityResponse = new ArrayList<>();
                                cityResponse = response.body().getData();
                                cityList = new ArrayList<>();
                                cityList.add("Select City");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    cityList.add(response.body().getData().get(i).getName());
                                }
                            } else {
                                cityList = new ArrayList<>();
                                cityList.add("Select City");
                            }
                        } else {
                            cityList = new ArrayList<>();
                            cityList.add("Select City");
                        }
                    } else {
                        cityList = new ArrayList<>();
                        cityList.add("Select City");
                    }
                    setCityList(cityList);
                }

                @Override
                public void onFailure(@NonNull Call<LocationResponse> call, @NonNull Throwable t) {
                    cityList = new ArrayList<>();
                    cityList.add("Select City");
                    setCityList(cityList);
                }
            });
        } else {
            cityList = new ArrayList<>();
            cityList.add("Select City");
            setCityList(cityList);
        }
    }

    private void getHealthCareCategory() {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_category().enqueue(new Callback<CategoryResponse>() {
                @Override
                public void onResponse(@NonNull Call<CategoryResponse> call, @NonNull Response<CategoryResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                categoryResponse = new ArrayList<>();
                                categoryResponse = response.body().getData();
                                categoryList = new ArrayList<>();
                                categoryList.add("Select Essentials Category");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    categoryList.add(response.body().getData().get(i).getType());
                                }

                            } else {
                                categoryList = new ArrayList<>();
                                categoryList.add("Select Essentials Category");
                            }
                        } else {
                            categoryList = new ArrayList<>();
                            categoryList.add("Select Essentials Category");
                        }
                    } else {
                        categoryList = new ArrayList<>();
                        categoryList.add("Select Essentials Category");
                    }
                    setCategoryList(categoryList);
                }

                @Override
                public void onFailure(@NonNull Call<CategoryResponse> call, @NonNull Throwable t) {
                    categoryList = new ArrayList<>();
                    categoryList.add("Select Essentials Category");
                    setCityList(cityList);
                }
            });
        } else {
            categoryList = new ArrayList<>();
            categoryList.add("Select Essentials Category");
            setCategoryList(categoryList);

        }
    }

    private void uploadProfilePicture(/*String filePath,*/ String firstName, String lastName,String title, String businessName,
                                                           String serviceDescription,
                                      String address, String username, String email,String language, String password, String confirm_password, String phone,
                                      String zip_code, String monday_start_time, String monday_end_time, String tuesday_start_time,
                                      String tuesday_end_time, String wednesday_start_time, String wednesday_end_time, String thursday_start_time, String thursday_end_time,
                                      String friday_start_time, String friday_end_time, String saturday_start_time, String saturday_end_time, String sunday_start_time,
                                      String sunday_end_time, String country_name, String state_name, String city_name,File file,
                                                           final Uri resultUri) {
//        File imageFile = new File(filePath);
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
//            final RequestBody profile_image = RequestBody.create(imageFile, MediaType.parse("image/*"));
//            MultipartBody.Part profile_image_body = MultipartBody.Part.createFormData("image", imageFile.getName(), profile_image);
            final RequestBody profile_image = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), profile_image);
            Log.d("mytag", "uploadProfilePictureuploadProfilePicture is called");
            APIUtils.getAPIService().upload_image(fileToUpload).enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(@NonNull Call<UploadImageResponse> call, @NonNull Response<UploadImageResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
//                            uploaded_image = response.body().getImage().trim();
                            if (isCustomer) {
                                try {
                                userPicturePatient.setImageBitmap(MediaStore.Images.Media.getBitmap(context.getContentResolver(),
                                        resultUri));
                                Prefs.getPrefInstance().setValue(context, Const.SET_PROFILE_IMAGE, response.body().getImage());
                                callRegisterPatient(firstName, lastName, address, username, email, password, confirm_password, phone,
                                        zip_code, country_name, state_name, city_name, response.body().getImage());
                                  } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    userPicture.setImageBitmap(MediaStore.Images.Media.getBitmap(context.getContentResolver(),
                                            resultUri));
                                    Prefs.getPrefInstance().setValue(context, Const.SET_PROFILE_IMAGE, response.body().getImage());
                                callRegisterBusiness(firstName, lastName,title, businessName, serviceDescription,
                                        address, username, email,language, password, confirm_password, phone,
                                        zip_code, monday_start_time, monday_end_time, tuesday_start_time,
                                        tuesday_end_time, wednesday_start_time, wednesday_end_time, thursday_start_time, thursday_end_time,
                                        friday_start_time, friday_end_time, saturday_start_time, saturday_end_time, sunday_start_time,
                                        sunday_end_time, country_name, state_name, city_name, response.body().getImage());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            loader.setVisibility(View.GONE);
                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UploadImageResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                }
            });
        } else {
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    private void getPaymentAmount(String user_id) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().get_payment_amount().enqueue(new Callback<PaymentResponse>() {
                @Override
                public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            amount = response.body().getAmount();
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.payment_dialog), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            CardMultilineWidget cardInput = dialog_view.findViewById(R.id.cardInput);
                            cardInput.setCardInputListener(new CardInputListener() {
                                @Override
                                public void onFocusChange(@NotNull FocusField focusField) {

                                }

                                @Override
                                public void onCardComplete() {
                                }

                                @Override
                                public void onExpirationComplete() {

                                }

                                @Override
                                public void onCvcComplete() {

                                }

                            });

                            Button cancel = dialog_view.findViewById(R.id.cancel);
                            cancel.setOnClickListener(v12 -> dialog.dismiss());
                            loader.setVisibility(View.GONE);
                            Button pay = dialog_view.findViewById(R.id.payButton);
                            pay.setOnClickListener(v1 -> {
                                loader.setVisibility(View.VISIBLE);
                                Card card = cardInput.getCard();
                                if (card != null && card.validateCard()) {
//                                    stripe = new Stripe(context, "pk_test_zYnCgv6nLRc5ZCZvdnqR4fRS");
                                    stripe = new Stripe(context, context.getResources().getString(R.string.stripe_key));
                                    try {
                                        String token = new getToken(card).execute().get();
                                        if (token != null) {
                                            dialog.dismiss();
                                            createOrder(token, user_id);
                                        } else {
                                            loader.setVisibility(View.GONE);
                                            View dialog_view1 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                            final AlertDialog dialog1 = new AlertDialog.Builder(context)
                                                    .setCancelable(false)
                                                    .setView(dialog_view1)
                                                    .show();

                                            if (dialog1.getWindow() != null)
                                                dialog1.getWindow().getDecorView().getBackground().setAlpha(0);

                                            ((TextView) dialog_view1.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.something_went_wrong));
                                            (dialog_view1.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                                            ((Button) dialog_view1.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.ok));
                                            dialog_view1.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                                dialog1.dismiss();
                                                dialog.dismiss();
                                            });
                                        }
                                    } catch (ExecutionException | InterruptedException e) {
                                        e.printStackTrace();
                                        loader.setVisibility(View.GONE);
                                    }
                                } else {
                                    loader.setVisibility(View.GONE);
                                }
                            });


                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void createOrder(String token, String user_id) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.GONE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", user_id);
                jsonObject.put("amount", amount);
                jsonObject.put("token", token);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody create_order = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().create_order(create_order).enqueue(new Callback<OrderResponse>() {
                @Override
                public void onResponse(@NonNull Call<OrderResponse> call, @NonNull Response<OrderResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                startActivity(new Intent(context, Login.class).putExtra("isCustomer", false).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                getActivity().finish();
                            });


                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<OrderResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    static class getToken extends AsyncTask<Void, Void, String> {

        Card card;

        getToken(Card card) {
            this.card = card;
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                Token token = stripe.createCardTokenSynchronous(card);
                return token != null ? token.getId() : null;
            } catch (AuthenticationException | CardException | APIException | APIConnectionException | InvalidRequestException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private void requestMultiplePermissions() {
        Dexter.withActivity((Activity) context)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                            Toast.makeText(context, "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(context, "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showPictureDialog_chooser() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(context);
        pictureDialog.setTitle(showpicturedialog_title);
        String[] pictureDialogItems = {selectfromgallerymsg, selectfromcameramsg, canceldialog};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                            case 3:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        startActivityForResult(intent, CAMERA);
    }

    private void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    CropImage(contentURI);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAMERA) {
            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    Log.d("mytag", "f=temp");
                    break;
                }
            }

            try {
                CropImage(Uri.fromFile(f));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == UCrop.REQUEST_CROP) {
            handleUCropResult(data);
        }
    }

    private void CropImage(Uri contentURI) throws IOException {
        Uri destinationUri = Uri.fromFile(new File(context.getCacheDir(), "temp.jpg"));
        UCrop.Options options = new UCrop.Options();
//      options.setCompressionQuality(IMAGE_COMPRESSION);
        options.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(context, R.color.colorPrimary));

        UCrop.of(contentURI, destinationUri)
                .withOptions(options)
                .start(context, SignUpFragment.this);
    }

    private void handleUCropResult(Intent data) {
        if (data == null) {
            setResultCancelled();
            return;
        }
        try {
            resultUri = UCrop.getOutput(data);
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), resultUri);
            Log.d("mytag", "my bitmap iss-" + bitmap);
//             Uploadimagetoserver(new File(getRealPathFromURIPath(resultUri, getActivity())), resultUri);
            userPicture.setImageBitmap(bitmap);
            userPicturePatient.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setResultCancelled() {
        Intent intent = new Intent();
        setResult(getActivity().RESULT_CANCELED, intent);
        getActivity().finish();
    }

    private void setResult(int resultCanceled, Intent intent) {

    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

}
