package com.essentials.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.activities.HomeScreen;
import com.essentials.application.Essentials;
import com.essentials.data.models.category.CategoryResponse;
import com.essentials.data.models.category.CategoryResponseData;
import com.essentials.data.models.location.LocationResponse;
import com.essentials.data.models.location.LocationResponseData;
import com.essentials.data.models.user.ProfileResponse;
import com.essentials.data.models.user.UploadImageResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.iceteck.silicompressorr.SiliCompressor;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

public class EditProfileFragment11 extends Fragment {

    private static EditProfileFragment11 instance = null;
    @BindView(R.id.back_page_patient)
    LinearLayout backPagePatient;
    @BindView(R.id.header_patient)
    LinearLayout headerPatient;
    @BindView(R.id.image_loader_patient)
    ProgressBar imageLoaderPatient;

    @BindView(R.id.frame_patient)
    FrameLayout framePatient;
    @BindView(R.id.select_picture_patient)
    ImageView selectPicturePatient;
    @BindView(R.id.first_name_patient_input)
    TextInputEditText firstNamePatientInput;
    @BindView(R.id.first_name_patient_container)
    TextInputLayout firstNamePatientContainer;
    @BindView(R.id.last_name_patient_input)
    TextInputEditText lastNamePatientInput;
    @BindView(R.id.last_name_patient_container)
    TextInputLayout lastNamePatientContainer;
    @BindView(R.id.username_patient_input)
    TextInputEditText usernamePatientInput;
    @BindView(R.id.username_patient_container)
    TextInputLayout usernamePatientContainer;
    @BindView(R.id.email_patient_input)
    TextInputEditText emailPatientInput;
    @BindView(R.id.email_patient_container)
    TextInputLayout emailPatientContainer;
    @BindView(R.id.address_input_patient)
    TextInputEditText addressInputPatient;
    @BindView(R.id.address_container_patient)
    TextInputLayout addressContainerPatient;
    @BindView(R.id.country_patient)
    Spinner countryPatient;
    @BindView(R.id.country_patient_error)
    TextView countryPatientError;
    @BindView(R.id.state_patient)
    Spinner statePatient;
    @BindView(R.id.state_patient_error)
    TextView statePatientError;
    @BindView(R.id.city_patient)
    Spinner cityPatient;
    @BindView(R.id.city_patient_error)
    TextView cityPatientError;
    @BindView(R.id.zip_code_patient_input)
    TextInputEditText zipCodePatientInput;
    @BindView(R.id.zip_code_patient_container)
    TextInputLayout zipCodePatientContainer;
    @BindView(R.id.phone_patient_input)
    TextInputEditText phonePatientInput;
    @BindView(R.id.phone_patient_container)
    TextInputLayout phonePatientContainer;
    @BindView(R.id.submit_patient)
    MaterialCheckBox submitPatient;
    @BindView(R.id.page_patient)
    LinearLayout pagePatient;
    @BindView(R.id.back_page_one)
    LinearLayout backPageOne;
    @BindView(R.id.header_page_one)
    LinearLayout headerPageOne;
    @BindView(R.id.image_loader)
    ProgressBar imageLoader;
    @BindView(R.id.frame)
    FrameLayout frame;
    @BindView(R.id.select_picture)
    ImageView selectPicture;
    @BindView(R.id.first_name_input)
    TextInputEditText firstNameInput;
    @BindView(R.id.first_name_container)
    TextInputLayout firstNameContainer;
    @BindView(R.id.last_name_input)
    TextInputEditText lastNameInput;
    @BindView(R.id.last_name_container)
    TextInputLayout lastNameContainer;
    @BindView(R.id.username_input)
    TextInputEditText usernameInput;
    @BindView(R.id.username_container)
    TextInputLayout usernameContainer;
    @BindView(R.id.email_input)
    TextInputEditText emailInput;
    @BindView(R.id.email_container)
    TextInputLayout emailContainer;
    @BindView(R.id.categories_business)
    Spinner categoriesBusiness;
    @BindView(R.id.category_business_error)
    TextView categoryBusinessError;
    @BindView(R.id.service_input)
    TextInputEditText serviceInput;
    @BindView(R.id.service_container)
    TextInputLayout serviceContainer;
    @BindView(R.id.continue_text)
    MaterialCheckBox continueText;
    @BindView(R.id.layout_container)
    LinearLayout layoutContainer;
    @BindView(R.id.page_one)
    LinearLayout pageOne;
    @BindView(R.id.back_page_two)
    LinearLayout backPageTwo;
    @BindView(R.id.header_business)
    LinearLayout headerBusiness;
    @BindView(R.id.business_name_input)
    TextInputEditText businessNameInput;
    @BindView(R.id.business_name_container)
    TextInputLayout businessNameContainer;
    @BindView(R.id.address_input)
    TextInputEditText addressInput;
    @BindView(R.id.address_container)
    TextInputLayout addressContainer;
    @BindView(R.id.country_business)
    Spinner countryBusiness;
    @BindView(R.id.country_business_error)
    TextView countryBusinessError;
    @BindView(R.id.state_business)
    Spinner stateBusiness;
    @BindView(R.id.state_business_error)
    TextView stateBusinessError;
    @BindView(R.id.city_business)
    Spinner cityBusiness;
    @BindView(R.id.city_business_error)
    TextView cityBusinessError;
    @BindView(R.id.zip_code_business_input)
    TextInputEditText zipCodeBusinessInput;
    @BindView(R.id.zip_code_business_container)
    TextInputLayout zipCodeBusinessContainer;
    @BindView(R.id.phone_input)
    TextInputEditText phoneInput;
    @BindView(R.id.phone_container)
    TextInputLayout phoneContainer;
    @BindView(R.id.monday)
    MaterialCheckBox monday;
    @BindView(R.id.tuesday)
    MaterialCheckBox tuesday;
    @BindView(R.id.wednesday)
    MaterialCheckBox wednesday;
    @BindView(R.id.thursday)
    MaterialCheckBox thursday;
    @BindView(R.id.friday)
    MaterialCheckBox friday;
    @BindView(R.id.saturday)
    MaterialCheckBox saturday;
    @BindView(R.id.sunday)
    MaterialCheckBox sunday;
    @BindView(R.id.mon_start_time)
    TextView monStartTime;
    @BindView(R.id.mon_end_time)
    TextView monEndTime;
    @BindView(R.id.mon_time_container)
    LinearLayout monTimeContainer;
    @BindView(R.id.mon_closed)
    TextView monClosed;
    @BindView(R.id.tue_start_time)
    TextView tueStartTime;
    @BindView(R.id.tue_end_time)
    TextView tueEndTime;
    @BindView(R.id.tue_time_container)
    LinearLayout tueTimeContainer;
    @BindView(R.id.tue_closed)
    TextView tueClosed;
    @BindView(R.id.wed_start_time)
    TextView wedStartTime;
    @BindView(R.id.wed_end_time)
    TextView wedEndTime;
    @BindView(R.id.wed_time_container)
    LinearLayout wedTimeContainer;
    @BindView(R.id.wed_closed)
    TextView wedClosed;
    @BindView(R.id.thu_start_time)
    TextView thuStartTime;
    @BindView(R.id.thu_end_time)
    TextView thuEndTime;
    @BindView(R.id.thu_time_container)
    LinearLayout thuTimeContainer;
    @BindView(R.id.thu_closed)
    TextView thuClosed;
    @BindView(R.id.fri_start_time)
    TextView friStartTime;
    @BindView(R.id.fri_end_time)
    TextView friEndTime;
    @BindView(R.id.fri_time_container)
    LinearLayout friTimeContainer;
    @BindView(R.id.fri_closed)
    TextView friClosed;
    @BindView(R.id.sat_start_time)
    TextView satStartTime;
    @BindView(R.id.sat_end_time)
    TextView satEndTime;
    @BindView(R.id.sat_time_container)
    LinearLayout satTimeContainer;
    @BindView(R.id.sat_closed)
    TextView satClosed;
    @BindView(R.id.sun_start_time)
    TextView sunStartTime;
    @BindView(R.id.sun_end_time)
    TextView sunEndTime;
    @BindView(R.id.sun_time_container)
    LinearLayout sunTimeContainer;
    @BindView(R.id.sun_closed)
    TextView sunClosed;
    @BindView(R.id.submit_business)
    MaterialCheckBox submitBusiness;
    @BindView(R.id.page_two)
    LinearLayout pageTwo;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.main_container)
    CoordinatorLayout mainContainer;
    @BindView(R.id.user_picture_patient)
    CircularImageView userPicturePatient;
    @BindView(R.id.user_picture)
    CircularImageView userPicture;
    private AppManageInterface appManageInterface;
    private String path;
    private String type;
    private String contentType = "", pageTitle;
    private int assetGroupClassPath;
    private boolean isGenre, isGrid, isLanguage;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private boolean loadingInProgress;
    private Activity activity;
    private static String uploaded_image = "";
    Handler handler;
    int rollId = 1;
    String hourOfTheDay;
    int mHour, mMinute, hours;
    String startTime;
    Boolean isCustomer;
    ArrayAdapter<String> countryAdapter;
    ArrayAdapter<String> cityAdapter;
    ArrayAdapter<String> stateAdapter;
    ArrayAdapter<String> categoryAdapter;
    ArrayList<String> countryList = new ArrayList<>();
    ArrayList<String> cityList = new ArrayList<>();
    ArrayList<String> statesList = new ArrayList<>();
    ArrayList<String> categoryList = new ArrayList<>();
    List<LocationResponseData> countryResponse = new ArrayList<>();
    List<LocationResponseData> stateResponse = new ArrayList<>();
    List<LocationResponseData> cityResponse = new ArrayList<>();
    List<CategoryResponseData> categoryResponse = new ArrayList<>();
    int patient_city_id, patient_state_id, business_city_id, business_state_id, business_categories_id, patient_country_id, business_country_id;
    int monday_selection, tuesday_selection, wednesday_selection, thursday_selection, friday_selection, saturday_selection, sunday_selection;
    private Uri mCropImageUri;
    private String image = "", selectedCountry = "", selectedState = "", selectedCity = "", selectedCategory = "";

    public static synchronized EditProfileFragment11 getInstance() {
        return instance;
    }

    public static synchronized EditProfileFragment11 newInstance() {
        return instance = new EditProfileFragment11();
    }

    @OnClick(R.id.back_page_patient)
    public void setBackPagePatient() {
        activity.onBackPressed();
    }

    @OnClick(R.id.back_page_one)
    public void setBack() {
        activity.onBackPressed();
    }

    @OnCheckedChanged({R.id.monday, R.id.tuesday, R.id.wednesday, R.id.thursday, R.id.friday, R.id.saturday, R.id.sunday})
    public void setCheckbox(CheckBox checkbox, boolean isChecked) {
        // Check which checkbox was clicked
        if (checkbox == monday) {
            monday_selection = isChecked ? 1 : 0;
            monTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            monClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                monStartTime.setText(getResources().getString(R.string.time_ten));
                monEndTime.setText(getResources().getString(R.string.time_ten));
            } else {
                monStartTime.setText("");
                monEndTime.setText("");
            }
        } else if (checkbox == tuesday) {
            tuesday_selection = isChecked ? 1 : 0;
            tueTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            tueClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                tueStartTime.setText(getResources().getString(R.string.time_ten));
                tueEndTime.setText(getResources().getString(R.string.time_ten));
            } else {
                tueStartTime.setText("");
                tueEndTime.setText("");
            }
        } else if (checkbox == wednesday) {
            wednesday_selection = isChecked ? 1 : 0;
            wedTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            wedClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                wedStartTime.setText(getResources().getString(R.string.time_ten));
                wedEndTime.setText(getResources().getString(R.string.time_ten));
            } else {
                wedStartTime.setText("");
                wedEndTime.setText("");
            }
        } else if (checkbox == thursday) {
            thursday_selection = isChecked ? 1 : 0;
            thuTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            thuClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                thuStartTime.setText(getResources().getString(R.string.time_ten));
                thuEndTime.setText(getResources().getString(R.string.time_ten));
            } else {
                thuStartTime.setText("");
                thuEndTime.setText("");
            }
        } else if (checkbox == friday) {
            friday_selection = isChecked ? 1 : 0;
            friTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            friClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                friStartTime.setText(getResources().getString(R.string.time_ten));
                friEndTime.setText(getResources().getString(R.string.time_ten));
            } else {
                friStartTime.setText("");
                friEndTime.setText("");
            }
        } else if (checkbox == saturday) {
            saturday_selection = isChecked ? 1 : 0;
            satTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            satClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                satStartTime.setText(getResources().getString(R.string.time_ten));
                satEndTime.setText(getResources().getString(R.string.time_ten));
            } else {
                satStartTime.setText("");
                satEndTime.setText("");
            }
        } else {
            sunday_selection = isChecked ? 1 : 0;
            sunTimeContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            sunClosed.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            if (isChecked) {
                sunStartTime.setText(getResources().getString(R.string.time_ten));
                sunEndTime.setText(getResources().getString(R.string.time_ten));
            } else {
                sunStartTime.setText("");
                sunEndTime.setText("");
            }
        }

    }

    @OnClick({R.id.mon_start_time, R.id.mon_end_time, R.id.tue_start_time, R.id.tue_end_time, R.id.wed_start_time, R.id.wed_end_time,
            R.id.thu_start_time, R.id.thu_end_time, R.id.fri_start_time, R.id.fri_end_time, R.id.sat_start_time, R.id.sat_end_time,
            R.id.sun_start_time, R.id.sun_end_time})
    public void pickDoor(TextView textView) {

        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> {
            String formatTime = hourOfDay + ":" + minute;
            SimpleDateFormat fmt = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            Date date = null;
            try {
                date = fmt.parse(formatTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
            String formattedTime = fmtOut.format(date);
            startTime = formattedTime;
            textView.setText(formattedTime);
        }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @OnClick(R.id.back_page_two)
    public void setBackBusiness() {
        pagePatient.setVisibility(View.GONE);
        pageOne.setVisibility(View.VISIBLE);
        pageTwo.setVisibility(View.GONE);
    }

    @OnClick(R.id.submit_patient)
    public void setSubmitPatient() {
        if (firstNamePatientInput.getText() == null) {
            firstNamePatientContainer.setError(getResources().getString(R.string.first_name_empty));
        } else if (firstNamePatientInput.getText().toString().trim().length() == 0) {
            firstNamePatientContainer.setError(getResources().getString(R.string.first_name_empty));
        } else if (lastNamePatientInput.getText() == null) {
            lastNamePatientContainer.setError(getResources().getString(R.string.last_name_empty));
        } else if (lastNamePatientInput.getText().toString().trim().length() == 0) {
            lastNamePatientContainer.setError(getResources().getString(R.string.last_name_empty));
        } else if (addressInputPatient.getText() == null) {
            addressContainerPatient.setError(getResources().getString(R.string.address_empty));
        } else if (addressInputPatient.getText().toString().trim().length() == 0) {
            addressContainerPatient.setError(getResources().getString(R.string.address_empty));
        } else if (zipCodePatientInput.getText() == null) {
            zipCodePatientContainer.setError(getResources().getString(R.string.zip_code_empty));
        } else if (zipCodePatientInput.getText().toString().trim().length() == 0) {
            zipCodePatientContainer.setError(getResources().getString(R.string.zip_code_empty));
        } else if (zipCodePatientInput.getText().toString().trim().length() < 5) {
            zipCodePatientContainer.setError(getResources().getString(R.string.zip_code_incorrect));
        }else if (phonePatientInput.getText() == null) {
            phonePatientContainer.setError(getResources().getString(R.string.phone_number_empty));
        } else if (phonePatientInput.getText().toString().trim().length() == 0) {
            phonePatientContainer.setError(getResources().getString(R.string.phone_number_empty));
        } else if (phonePatientInput.getText().toString().trim().length() < 5) {
            phonePatientContainer.setError(getResources().getString(R.string.phone_number_incorrect));
        }else if (countryPatient.getSelectedItemPosition() == 0) {
            countryPatientError.setVisibility(View.VISIBLE);
            countryPatientError.setText(getResources().getString(R.string.please_select_country));
        } else if (statePatient.getSelectedItemPosition() == 0) {
            statePatientError.setVisibility(View.VISIBLE);
            statePatientError.setText(getResources().getString(R.string.please_select_state));
        } else if (cityPatient.getSelectedItemPosition() == 0) {
            cityPatientError.setVisibility(View.VISIBLE);
            cityPatientError.setText(getResources().getString(R.string.please_select_city));
        }else {
            usernameInput.clearFocus();
            firstNameInput.clearFocus();
            lastNameInput.clearFocus();
            emailPatientInput.clearFocus();
            zipCodePatientInput.clearFocus();

            if (image.equals("")) {
                updateProfilePatient(firstNamePatientInput.getText().toString(), lastNamePatientInput.getText().toString(), addressInputPatient.getText().toString(),
                        phonePatientInput.getText().toString(), zipCodePatientInput.getText().toString(),countryPatient.getSelectedItem().toString(),
                        statePatient.getSelectedItem().toString(), cityPatient.getSelectedItem().toString());
            } else {
                uploadProfilePicture(image, firstNamePatientInput.getText().toString(), lastNamePatientInput.getText().toString(), businessNameInput.getText().toString(),
                        serviceInput.getText().toString(), addressInputPatient.getText().toString(), phonePatientInput.getText().toString(),
                        zipCodePatientInput.getText().toString(), "", "", "",
                        "", "", "", "",
                        "", "", "", "",
                        "", "", "", countryPatient.getSelectedItem().toString(),
                        statePatient.getSelectedItem().toString(), cityPatient.getSelectedItem().toString());
            }


        }
    }

    @OnClick(R.id.submit_business)
    public void setSubmitBusiness() {
        if (businessNameInput.getText() == null) {
            businessNameContainer.setError(getResources().getString(R.string.business_name_empty));
        } else if (businessNameInput.getText().toString().trim().length() == 0) {
            businessNameContainer.setError(getResources().getString(R.string.business_name_empty));
        } else if (addressInput.getText() == null) {
            addressContainer.setError(getResources().getString(R.string.address_empty));
        } else if (addressInput.getText().toString().trim().length() == 0) {
            addressContainer.setError(getResources().getString(R.string.address_empty));
        } else if (countryBusiness.getSelectedItemPosition() == 0) {
            countryBusinessError.setVisibility(View.VISIBLE);
            countryBusinessError.setText(getResources().getString(R.string.please_select_country));
        } else if (stateBusiness.getSelectedItemPosition() == 0) {
            stateBusinessError.setVisibility(View.VISIBLE);
            stateBusinessError.setText(getResources().getString(R.string.please_select_state));
        } else if (cityBusiness.getSelectedItemPosition() == 0) {
            cityBusinessError.setVisibility(View.VISIBLE);
            cityBusinessError.setText(getResources().getString(R.string.please_select_city));
        }else if (zipCodeBusinessInput.getText() == null) {
            zipCodeBusinessContainer.setError(getResources().getString(R.string.zip_code_empty));
        } else if (zipCodeBusinessInput.getText().toString().trim().length() == 0) {
            zipCodeBusinessContainer.setError(getResources().getString(R.string.zip_code_empty));
        } else if (zipCodeBusinessInput.getText().toString().trim().length() < 5) {
            zipCodeBusinessContainer.setError(getResources().getString(R.string.zip_code_incorrect));
        }else if (phoneInput.getText() == null) {
            phoneContainer.setError(getResources().getString(R.string.phone_number_empty));
        } else if (phoneInput.getText().toString().trim().length() == 0) {
            phoneContainer.setError(getResources().getString(R.string.phone_number_empty));
        } else if (phoneInput.getText().toString().trim().length() < 12) {
            phoneContainer.setError(getResources().getString(R.string.phone_number_incorrect));
        }else {
            if (!monday.isChecked()) {
                monStartTime.setText("");
                monEndTime.setText("");
            }
            if (!tuesday.isChecked()) {
                tueStartTime.setText("");
                tueEndTime.setText("");
            }
            if (!wednesday.isChecked()) {
                wedStartTime.setText("");
                wedEndTime.setText("");
            }
            if (!thursday.isChecked()) {
                thuStartTime.setText("");
                thuEndTime.setText("");
            }
            if (!friday.isChecked()) {
                friStartTime.setText("");
                friEndTime.setText("");
            }
            if (!saturday.isChecked()) {
                satStartTime.setText("");
                satEndTime.setText("");
            }
            if (!sunday.isChecked()) {
                sunStartTime.setText("");
                sunEndTime.setText("");
            }
            emailInput.clearFocus();
            firstNameInput.clearFocus();
            lastNameInput.clearFocus();
            categoriesBusiness.clearFocus();
            if (image.equals("")) {
                updateProfileBusiness(firstNameInput.getText().toString(), lastNameInput.getText().toString(), businessNameInput.getText().toString(),
                        serviceInput.getText().toString(), addressInput.getText().toString(), phoneInput.getText().toString(),
                        zipCodeBusinessInput.getText().toString(), monStartTime.getText().toString(), monEndTime.getText().toString(), tueStartTime.getText().toString(),
                        tueEndTime.getText().toString(), wedStartTime.getText().toString(), wedEndTime.getText().toString(), thuStartTime.getText().toString(),
                        thuEndTime.getText().toString(), friStartTime.getText().toString(), friEndTime.getText().toString(), satStartTime.getText().toString(),
                        satEndTime.getText().toString(), sunStartTime.getText().toString(), sunEndTime.getText().toString(),countryBusiness.getSelectedItem().toString(),
                        stateBusiness.getSelectedItem().toString(), cityBusiness.getSelectedItem().toString());
            } else {
                uploadProfilePicture(image, firstNameInput.getText().toString(), lastNameInput.getText().toString(), businessNameInput.getText().toString(),
                        serviceInput.getText().toString(), addressInput.getText().toString(), phoneInput.getText().toString(),
                        zipCodeBusinessInput.getText().toString(), monStartTime.getText().toString(), monEndTime.getText().toString(), tueStartTime.getText().toString(),
                        tueEndTime.getText().toString(), wedStartTime.getText().toString(), wedEndTime.getText().toString(), thuStartTime.getText().toString(),
                        thuEndTime.getText().toString(), friStartTime.getText().toString(), friEndTime.getText().toString(), satStartTime.getText().toString(),
                        satEndTime.getText().toString(), sunStartTime.getText().toString(), sunEndTime.getText().toString(),countryBusiness.getSelectedItem().toString(),
                        stateBusiness.getSelectedItem().toString(), cityBusiness.getSelectedItem().toString());
            }


        }
    }

    @OnClick(R.id.continue_text)
    public void onContinueProfile() {
        if (firstNameInput.getText() == null) {
            firstNameContainer.setError(getResources().getString(R.string.first_name_empty));
        } else if (firstNameInput.getText().toString().trim().length() == 0) {
            firstNameContainer.setError(getResources().getString(R.string.first_name_empty));
        } else if (lastNameInput.getText() == null) {
            lastNameContainer.setError(getResources().getString(R.string.last_name_empty));
        } else if (lastNameInput.getText().toString().trim().length() == 0) {
            lastNameContainer.setError(getResources().getString(R.string.last_name_empty));
        } else if (categoriesBusiness.getSelectedItemPosition() == 0) {
            categoryBusinessError.setVisibility(View.VISIBLE);
            categoryBusinessError.setText(getResources().getString(R.string.please_select_category));
        }else if (serviceInput.getText() == null) {
            serviceContainer.setError(getResources().getString(R.string.service_empty));
        } else if (serviceInput.getText().toString().trim().length() == 0) {
            serviceContainer.setError(getResources().getString(R.string.service_empty));
        } else {
            usernameInput.clearFocus();
            firstNameInput.clearFocus();
            lastNameInput.clearFocus();
            serviceInput.clearFocus();
            pagePatient.setVisibility(View.GONE);
            pageOne.setVisibility(View.GONE);
            pageTwo.setVisibility(View.VISIBLE);

        }
    }

    @OnClick({R.id.select_picture, R.id.select_picture_patient})
    public void pickImage(ImageView imageView) {
        firstNameInput.clearFocus();
        lastNameInput.clearFocus();
        usernameInput.clearFocus();
        View dialog_root_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button_2), null);
        AlertDialog root_dialog = new AlertDialog.Builder(context)
                .setCancelable(false)
                .setView(dialog_root_view)
                .show();

        if (root_dialog.getWindow() != null)
            root_dialog.getWindow().getDecorView().getBackground().setAlpha(0);

        ((Button) dialog_root_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
        if (Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, "").equals("")) {
            ((TextView) dialog_root_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.upload_picture_text));
            ((Button) dialog_root_view.findViewById(R.id.remove_picture)).setVisibility(View.GONE);
        } else {
            ((TextView) dialog_root_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.upload_picture_text));
            ((Button) dialog_root_view.findViewById(R.id.remove_picture)).setVisibility(View.GONE);
            ((Button) dialog_root_view.findViewById(R.id.remove_picture)).setText(getResources().getString(R.string.remove_picture));
        }
        ((Button) dialog_root_view.findViewById(R.id.upload_picture)).setText(getResources().getString(R.string.upload_picture));

        dialog_root_view.findViewById(R.id.dialog_cancel).setOnClickListener(view1 -> root_dialog.dismiss());
        dialog_root_view.findViewById(R.id.remove_picture).setOnClickListener(view -> {
            root_dialog.dismiss();
//            removeProfilePicture();
        });
        dialog_root_view.findViewById(R.id.upload_picture).setOnClickListener(view1 -> {
            root_dialog.dismiss();
            String[] PERMISSIONS = {Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};

            if (!AppUtil.hasPermissions(context, PERMISSIONS)) {
                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                AlertDialog dialog = new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setView(dialog_view)
                        .show();

                if (dialog.getWindow() != null)
                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.permission_request));
                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog.dismiss());
                dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(v -> {
                    dialog.dismiss();
                    askPermission(EditProfileFragment11.this,
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .onAccepted(result -> CropImage.startPickImageActivity(context, EditProfileFragment11.this))
                            .onDenied(result -> {
                                View dialog_view1 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                AlertDialog dialog1 = new AlertDialog.Builder(context)
                                        .setCancelable(false)
                                        .setView(dialog_view1)
                                        .show();

                                if (dialog1.getWindow() != null)
                                    dialog1.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view1.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.permission_Deny));
                                ((Button) dialog_view1.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                                ((Button) dialog_view1.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

                                dialog_view1.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog1.dismiss());
                                dialog_view1.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                    dialog1.dismiss();
                                    result.askAgain();
                                });
                            })
                            .onForeverDenied(result -> {
                                View dialog_view2 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                                AlertDialog dialog2 = new AlertDialog.Builder(context)
                                        .setCancelable(false)
                                        .setView(dialog_view2)
                                        .show();

                                if (dialog2.getWindow() != null)
                                    dialog2.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view2.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.permission_Forever_Deny));
                                ((Button) dialog_view2.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                                ((Button) dialog_view2.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.go_to_settings));

                                dialog_view2.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog2.dismiss());
                                dialog_view2.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                    dialog2.dismiss();
                                    result.goToSettings();
                                });
                            })
                            .ask();
                });
            } else CropImage.startPickImageActivity(context, EditProfileFragment11.this);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(context, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(context, imageUri)) {
                mCropImageUri = imageUri;
            } else {
                startCropImageActivity(imageUri);
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                image = SiliCompressor.with(context).compress(resultUri.getPath(), Environment.getExternalStoragePublicDirectory("/" + getResources().getString(R.string.app_name) + "/Images/"));
                File imageFile = new File(image);
                userPicture.setImageURI(Uri.fromFile(imageFile));
                userPicturePatient.setImageURI(Uri.fromFile(imageFile));
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(context, EditProfileFragment11.this);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(context, EditProfileFragment11.this);
            } else {
                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                final AlertDialog dialog = new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setView(dialog_view)
                        .show();

                if (dialog.getWindow() != null)
                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.cancel_image_pick));
                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));

                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                final AlertDialog dialog = new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setView(dialog_view)
                        .show();

                if (dialog.getWindow() != null)
                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.cancel_image_pick));
                (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));

                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_edit_profile, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        activity = getActivity();

        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
            isCustomer = getArguments().getBoolean("isCustomer");
        }
        init();
    }

    private void init() {
        loader.setVisibility(View.GONE);
        handler = new Handler();
        rollId = 1;
        scrollView.scrollTo(0, 0);

        statePatient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                if (statePatient.getSelectedItemPosition() != 0) {
                    patient_state_id = stateResponse.get(position - 1).getId();
                    selectedState = stateResponse.get(position - 1).getName();
                    getCity(stateResponse.get(position - 1).getId());
                    statePatientError.setVisibility(View.GONE);
                } else {
                    getCity(0);
                    statePatientError.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cityPatient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                if (cityPatient.getSelectedItemPosition() != 0) {
                    patient_city_id = cityResponse.get(position - 1).getId();
                    selectedCity = cityResponse.get(position - 1).getName();
                    cityPatientError.setVisibility(View.GONE);
                } else {
                    cityPatientError.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        stateBusiness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                if (stateBusiness.getSelectedItemPosition() != 0) {
                    business_state_id = stateResponse.get(position - 1).getId();
                    selectedState = stateResponse.get(position - 1).getName();
                    getCity(stateResponse.get(position - 1).getId());
                    stateBusinessError.setVisibility(View.GONE);
                } else {
                    getCity(0);
                    stateBusinessError.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cityBusiness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                if (cityBusiness.getSelectedItemPosition() != 0) {
                    business_city_id = cityResponse.get(position - 1).getId();
                    selectedCity = cityResponse.get(position - 1).getName();
                    cityBusinessError.setVisibility(View.GONE);
                } else {
                    cityBusinessError.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        categoriesBusiness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                if (categoriesBusiness.getSelectedItemPosition() != 0) {
                    business_categories_id = categoryResponse.get(position - 1).getHealthcarecategoryId();
                    selectedCategory = categoryResponse.get(position - 1).getType();
                    categoryBusinessError.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        countryPatient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                if (countryPatient.getSelectedItemPosition() != 0) {
                    patient_country_id = countryResponse.get(position - 1).getId();
                    selectedCountry = countryResponse.get(position - 1).getName();
                    Log.d("mytag", "selected country  - "+ selectedCountry);
                    getState(countryResponse.get(position - 1).getId());
                    countryPatientError.setVisibility(View.GONE);
                } else {
                    getState(0);
                    countryPatientError.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        countryBusiness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                if (countryBusiness.getSelectedItemPosition() != 0) {
                    business_country_id = countryResponse.get(position - 1).getId();
                    selectedCountry = countryResponse.get(position - 1).getName();
                    getState(countryResponse.get(position - 1).getId());
                    countryBusinessError.setVisibility(View.GONE);
                } else {
                    getState(0);
                    countryBusinessError.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (isCustomer) getProfileCustomer();
        else getProfileBusiness();

        if (isCustomer) {
            pagePatient.setVisibility(View.VISIBLE);
            pageOne.setVisibility(View.GONE);
        } else {
            pagePatient.setVisibility(View.GONE);
            pageOne.setVisibility(View.VISIBLE);
        }
        pageTwo.setVisibility(View.GONE);
        usernameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                usernameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        usernamePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                usernamePatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addressInputPatient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addressContainerPatient.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addressInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addressContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        firstNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                firstNameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        firstNamePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                firstNamePatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lastNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                lastNameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lastNamePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                lastNamePatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        zipCodePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                zipCodePatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        zipCodeBusinessInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                zipCodeBusinessContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        phoneInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                phoneContainer.setError("");
                if (s.length() > 1 && !s.toString().substring(s.length() - 1).equals("-") && (s.length() == 3 || s.length() == 7) && phoneInput.getText() != null)
                    phoneInput.getText().append("-");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        phonePatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                phonePatientContainer.setError("");
                if (s.length() > 1 && !s.toString().substring(s.length() - 1).equals("-") && (s.length() == 3 || s.length() == 7) && phonePatientInput.getText() != null)
                    phonePatientInput.getText().append("-");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        emailPatientInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailPatientContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        businessNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                businessNameContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        serviceInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                serviceContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void setCountryList(List<String> countryList) {
        countryAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, countryList);
        countryAdapter.setDropDownViewResource(R.layout.row_spinner);
        countryPatient.setAdapter(countryAdapter);
        countryBusiness.setAdapter(countryAdapter);
        int position = countryList.indexOf(selectedCountry);
        countryPatient.setSelection(position, true);
        countryBusiness.setSelection(position, true);
    }

    private void setCityList(List<String> cityList) {
        cityAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, cityList);
        cityAdapter.setDropDownViewResource(R.layout.row_spinner);
        cityPatient.setAdapter(cityAdapter);
        cityBusiness.setAdapter(cityAdapter);
        int position = cityList.indexOf(selectedCity);
        cityPatient.setSelection(position, true);
        cityBusiness.setSelection(position, true);
    }

    private void setCategoryList(List<String> categoryList) {
        categoryAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, categoryList);
        categoryAdapter.setDropDownViewResource(R.layout.row_spinner);
        categoriesBusiness.setAdapter(categoryAdapter);
        int position = categoryList.indexOf(selectedCategory);
        categoriesBusiness.setSelection(position, true);
    }

    private void setStateList(List<String> stateList) {
        stateAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, stateList);
        stateAdapter.setDropDownViewResource(R.layout.row_spinner);
        statePatient.setAdapter(stateAdapter);
        stateBusiness.setAdapter(stateAdapter);
        int position = stateList.indexOf(selectedState);
        statePatient.setSelection(position, true);
        stateBusiness.setSelection(position, true);
    }

    private void updateProfilePatient(String firstName, String lastName, String address, String phone, String zip_code, String country_name, String state_name, String city_name) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("firstname", firstName);
                jsonObject.put("lastname", lastName);
                jsonObject.put("address", address);
                jsonObject.put("country", country_name);
                jsonObject.put("state", state_name);
                jsonObject.put("city", city_name);
                jsonObject.put("phone", phone);
                jsonObject.put("zipcode", zip_code);
                jsonObject.put("image", uploaded_image);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody update_profile = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().update_profile(getResources().getString(R.string.customers), getResources().getString(R.string.update), Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""), update_profile).enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<ProfileResponse> call, @NonNull Response<ProfileResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (uploaded_image != null)
                            Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, response.body().getData().get(0).getImage());
                            Prefs.getPrefInstance().setValue(context, Const.PHONE_NUMBER, response.body().getData().get(0).getPhone());
                            Prefs.getPrefInstance().setValue(context, Const.USER_FIRST_NAME, response.body().getData().get(0).getFirstname());
                            Prefs.getPrefInstance().setValue(context, Const.USER_LAST_NAME, response.body().getData().get(0).getLastname());
                            Prefs.getPrefInstance().setValue(context, Const.COUNTRY, response.body().getData().get(0).getCountry());
                            Prefs.getPrefInstance().setValue(context, Const.STATE, response.body().getData().get(0).getState());
                            Prefs.getPrefInstance().setValue(context, Const.CITY, response.body().getData().get(0).getCity());
                            Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, response.body().getData().get(0).getImage());

                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));

                            (dialog_view.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);
                            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog.dismiss();
                                if (isCustomer)
                                    startActivity(new Intent(context, HomeScreen.class).putExtra("isCustomer", true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                else
                                    startActivity(new Intent(context, HomeScreen.class).putExtra("isCustomer", false).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                getActivity().finish();
                            });
                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);


                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));

                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ProfileResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        }
        else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void updateProfileBusiness(String firstName, String lastName, String businessName, String serviceDescription,
                                       String address, String phone,
                                       String zip_code, String monday_start_time, String monday_end_time, String tuesday_start_time,
                                       String tuesday_end_time, String wednesday_start_time, String wednesday_end_time, String thursday_start_time, String thursday_end_time,
                                       String friday_start_time, String friday_end_time, String saturday_start_time, String saturday_end_time, String sunday_start_time,
                                       String sunday_end_time, String country_name, String state_name, String city_name) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("firstname", firstName);
                jsonObject.put("lastname", lastName);
                jsonObject.put("businessname", businessName);
                jsonObject.put("address", address);
                jsonObject.put("country", country_name);
                jsonObject.put("state", state_name);
                jsonObject.put("city", city_name);
                jsonObject.put("phone", phone);
                jsonObject.put("zipcode", zip_code);
                jsonObject.put("service_description", serviceDescription);
                jsonObject.put("healthcarecategory_id", business_categories_id);
                jsonObject.put("monday", monday_selection);
                jsonObject.put("monday_start_time", monday_start_time);
                jsonObject.put("monday_end_time", monday_end_time);
                jsonObject.put("tuesday", tuesday_selection);
                jsonObject.put("tuesday_start_time", tuesday_start_time);
                jsonObject.put("tuesday_end_time", tuesday_end_time);
                jsonObject.put("wednesday", wednesday_selection);
                jsonObject.put("wednesday_start_time", wednesday_start_time);
                jsonObject.put("wednesday_end_time", wednesday_end_time);
                jsonObject.put("thursday", thursday_selection);
                jsonObject.put("thursday_start_time", thursday_start_time);
                jsonObject.put("thursday_end_time", thursday_end_time);
                jsonObject.put("friday", friday_selection);
                jsonObject.put("friday_start_time", friday_start_time);
                jsonObject.put("friday_end_time", friday_end_time);
                jsonObject.put("saturday", saturday_selection);
                jsonObject.put("saturday_start_time", saturday_start_time);
                jsonObject.put("saturday_end_time", saturday_end_time);
                jsonObject.put("sunday", sunday_selection);
                jsonObject.put("sunday_start_time", sunday_start_time);
                jsonObject.put("sunday_end_time", sunday_end_time);
                jsonObject.put("image", uploaded_image);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody update_profile = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().update_profile(getResources().getString(R.string.providers), getResources().getString(R.string.update), Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""), update_profile).enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<ProfileResponse> call, @NonNull Response<ProfileResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (uploaded_image != null)
                            Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, response.body().getData().get(0).getImage());
                            Prefs.getPrefInstance().setValue(context, Const.PHONE_NUMBER, response.body().getData().get(0).getPhone());
                            Prefs.getPrefInstance().setValue(context, Const.USER_FIRST_NAME, response.body().getData().get(0).getFirstname());
                            Prefs.getPrefInstance().setValue(context, Const.USER_LAST_NAME, response.body().getData().get(0).getLastname());
                            Prefs.getPrefInstance().setValue(context, Const.COUNTRY, response.body().getData().get(0).getCountry());
                            Prefs.getPrefInstance().setValue(context, Const.STATE, response.body().getData().get(0).getState());
                            Prefs.getPrefInstance().setValue(context, Const.CITY, response.body().getData().get(0).getCity());
                            Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, response.body().getData().get(0).getImage());
                            Prefs.getPrefInstance().setValue(context, Const.HEALTH_CARE_CATEGORY_ID, response.body().getData().get(0).getHealthcarecategoryId());

                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));

                            (dialog_view.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);
                            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog.dismiss();
                                if (isCustomer)
                                    startActivity(new Intent(context, HomeScreen.class).putExtra("isCustomer", true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                else
                                    startActivity(new Intent(context, HomeScreen.class).putExtra("isCustomer", false).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                getActivity().finish();
                            });
                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));

                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }


                @Override
                public void onFailure(@NonNull Call<ProfileResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void getCountry() {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_country(getResources().getString(R.string.type_countries)).enqueue(new Callback<LocationResponse>() {
                @Override
                public void onResponse(@NonNull Call<LocationResponse> call, @NonNull Response<LocationResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                countryResponse = response.body().getData();
                                countryList = new ArrayList<>();
                                countryList.add("Select Country");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    countryList.add(response.body().getData().get(i).getName());
                                }
                            } else {
                                countryList = new ArrayList<>();
                                countryList.add("Select Country");

                            }
                        } else {
                            countryList = new ArrayList<>();
                            countryList.add("Select Country");

                        }
                    } else {
                        countryList = new ArrayList<>();
                        countryList.add("Select Country");

                    }
                    statesList = new ArrayList<>();
                    statesList.add("Select State");
                    setStateList(statesList);
                    cityList = new ArrayList<>();
                    cityList.add("Select City");
                    setCityList(cityList);
                    setCountryList(countryList);
                }

                @Override
                public void onFailure(@NonNull Call<LocationResponse> call, @NonNull Throwable t) {
                    countryList = new ArrayList<>();
                    countryList.add("Select Country");
                    setCountryList(countryList);

                    statesList = new ArrayList<>();
                    statesList.add("Select State");
                    setStateList(statesList);

                    cityList = new ArrayList<>();
                    cityList.add("Select City");
                    setCityList(cityList);
                }
            });
        } else {
            countryList = new ArrayList<>();
            countryList.add("Select Country");
            setCountryList(countryList);

            statesList = new ArrayList<>();
            statesList.add("Select State");
            setStateList(statesList);

            cityList = new ArrayList<>();
            cityList.add("Select City");
            setCityList(cityList);
        }
    }

    private void getState(int country_id) {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_state(getResources().getString(R.string.type_states), country_id).enqueue(new Callback<LocationResponse>() {
                @Override
                public void onResponse(@NonNull Call<LocationResponse> call, @NonNull Response<LocationResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                stateResponse = new ArrayList<>();
                                stateResponse = response.body().getData();
                                statesList = new ArrayList<>();
                                statesList.add("Select State");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    statesList.add(response.body().getData().get(i).getName());
                                }
                            } else {
                                statesList = new ArrayList<>();
                                statesList.add("Select State");
                            }
                        } else {
                            statesList = new ArrayList<>();
                            statesList.add("Select State");
                        }
                    } else {
                        statesList = new ArrayList<>();
                        statesList.add("Select State");
                    }
                    setStateList(statesList);
                }

                @Override
                public void onFailure(@NonNull Call<LocationResponse> call, @NonNull Throwable t) {
                    statesList = new ArrayList<>();
                    statesList.add("Select State");
                    setStateList(statesList);
                }
            });
        } else {
            statesList = new ArrayList<>();
            statesList.add("Select State");
            setStateList(statesList);
        }
    }

    private void getCity(int state_id) {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_city(getResources().getString(R.string.type_cities), state_id).enqueue(new Callback<LocationResponse>() {
                @Override
                public void onResponse(@NonNull Call<LocationResponse> call, @NonNull Response<LocationResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                cityResponse = new ArrayList<>();
                                cityResponse = response.body().getData();
                                cityList = new ArrayList<>();
                                cityList.add("Select City");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    cityList.add(response.body().getData().get(i).getName());
                                }
                            } else {
                                cityList = new ArrayList<>();
                                cityList.add("Select City");
                            }
                        } else {
                            cityList = new ArrayList<>();
                            cityList.add("Select City");
                        }
                    } else {
                        cityList = new ArrayList<>();
                        cityList.add("Select City");
                    }
                    setCityList(cityList);
                }

                @Override
                public void onFailure(@NonNull Call<LocationResponse> call, @NonNull Throwable t) {
                    cityList = new ArrayList<>();
                    cityList.add("Select City");
                    setCityList(cityList);
                }
            });
        } else {
            cityList = new ArrayList<>();
            cityList.add("Select City");
            setCityList(cityList);
        }
    }

    private void getHealthCareCategory() {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_category().enqueue(new Callback<CategoryResponse>() {
                @Override
                public void onResponse(@NonNull Call<CategoryResponse> call, @NonNull Response<CategoryResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                categoryResponse = new ArrayList<>();
                                categoryResponse = response.body().getData();
                                categoryList = new ArrayList<>();
                                categoryList.add("Select Essentials Category");
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    categoryList.add(response.body().getData().get(i).getType());
                                }

                            } else {
                                categoryList = new ArrayList<>();
                                categoryList.add("Select Essentials Category");
                            }
                        } else {
                            categoryList = new ArrayList<>();
                            categoryList.add("Select Essentials Category");
                        }
                    } else {
                        categoryList = new ArrayList<>();
                        categoryList.add("Select Essentials Category");
                    }
                    setCategoryList(categoryList);
                }

                @Override
                public void onFailure(@NonNull Call<CategoryResponse> call, @NonNull Throwable t) {
                    categoryList = new ArrayList<>();
                    categoryList.add("Select Essentials Category");
                    setCityList(cityList);
                }
            });
        } else {
            categoryList = new ArrayList<>();
            categoryList.add("Select Essentials Category");
            setCategoryList(categoryList);

        }
    }

    private void uploadProfilePicture(String filePath, String firstName, String lastName, String businessName, String serviceDescription,
                                      String address, String phone,
                                      String zip_code, String monday_start_time, String monday_end_time, String tuesday_start_time,
                                      String tuesday_end_time, String wednesday_start_time, String wednesday_end_time, String thursday_start_time, String thursday_end_time,
                                      String friday_start_time, String friday_end_time, String saturday_start_time, String saturday_end_time, String sunday_start_time,
                                      String sunday_end_time, String country_name, String state_name, String city_name) {
        File imageFile = new File(filePath);
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            final RequestBody profile_image = RequestBody.create(imageFile, MediaType.parse("image/*"));
            MultipartBody.Part profile_image_body = MultipartBody.Part.createFormData("image", imageFile.getName(), profile_image);
            APIUtils.getAPIService().upload_image(profile_image_body).enqueue(new Callback<UploadImageResponse>() {
                @Override
                public void onResponse(@NonNull Call<UploadImageResponse> call, @NonNull Response<UploadImageResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            uploaded_image = response.body().getImage().trim();
                            if (isCustomer) {
                                updateProfilePatient(firstName, lastName, address, phone, zip_code, country_name, state_name, city_name);
                            } else {
                                updateProfileBusiness(firstName, lastName, businessName, serviceDescription,
                                        address, phone, zip_code, monday_start_time, monday_end_time, tuesday_start_time,
                                        tuesday_end_time, wednesday_start_time, wednesday_end_time, thursday_start_time, thursday_end_time,
                                        friday_start_time, friday_end_time, saturday_start_time, saturday_end_time, sunday_start_time,
                                        sunday_end_time, country_name, state_name, city_name);
                            }
                            loader.setVisibility(View.GONE);
                        } else {
                            loader.setVisibility(View.GONE);
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UploadImageResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                }
            });
        } else {
            loader.setVisibility(View.GONE);
        }
    }

    private void getProfileCustomer() {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_profile(getResources().getString(R.string.customers), getResources().getString(R.string.type_details), Prefs.getPrefInstance().getValue(context, Const.USER_ID, "")).enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<ProfileResponse> call, @NonNull Response<ProfileResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                Prefs.getPrefInstance().setValue(context, Const.USER_NAME, response.body().getData().get(0).getUsername());
                                Prefs.getPrefInstance().setValue(context, Const.EMAIL, response.body().getData().get(0).getEmail());
                                Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, response.body().getData().get(0).getImage());
                                Prefs.getPrefInstance().setValue(context, Const.USER_ID, response.body().getData().get(0).getUserId());
                                Prefs.getPrefInstance().setValue(context, Const.PHONE_NUMBER, response.body().getData().get(0).getPhone());
                                Prefs.getPrefInstance().setValue(context, Const.USER_FIRST_NAME, response.body().getData().get(0).getFirstname());
                                Prefs.getPrefInstance().setValue(context, Const.USER_LAST_NAME, response.body().getData().get(0).getLastname());
                                Prefs.getPrefInstance().setValue(context, Const.COUNTRY, response.body().getData().get(0).getCountry());
                                Prefs.getPrefInstance().setValue(context, Const.STATE, response.body().getData().get(0).getState());
                                Prefs.getPrefInstance().setValue(context, Const.CITY, response.body().getData().get(0).getCity());

                                Glide
                                        .with(Essentials.applicationContext)
                                        .load(Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, ""))
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                                        .thumbnail(0.25f)
                                        .error(R.drawable.ic_profile_icon)
                                        .into(userPicturePatient);
                                firstNamePatientInput.setText(response.body().getData().get(0).getFirstname());
                                lastNamePatientInput.setText(response.body().getData().get(0).getLastname());
                                emailPatientInput.setText(response.body().getData().get(0).getEmail());
                                usernamePatientInput.setText(response.body().getData().get(0).getUsername());
                                addressInputPatient.setText(response.body().getData().get(0).getAddress());
                                selectedCountry = response.body().getData().get(0).getCountry();
                                selectedState = response.body().getData().get(0).getState();
                                selectedCity = response.body().getData().get(0).getCity();
                                zipCodePatientInput.setText(response.body().getData().get(0).getZipcode());
                                phonePatientInput.setText(response.body().getData().get(0).getPhone());
                                getCountry();
                            } else {

                            }
                        } else {

                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(@NonNull Call<ProfileResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();

                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    private void getProfileBusiness() {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_profile(getResources().getString(R.string.providers), getResources().getString(R.string.type_details), Prefs.getPrefInstance().getValue(context, Const.USER_ID, "")).enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<ProfileResponse> call, @NonNull Response<ProfileResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                Prefs.getPrefInstance().setValue(context, Const.PROFILE_IMAGE, response.body().getData().get(0).getImage());
                                Prefs.getPrefInstance().setValue(context, Const.USER_ID, response.body().getData().get(0).getUserId());
                                Prefs.getPrefInstance().setValue(context, Const.PHONE_NUMBER, response.body().getData().get(0).getPhone());
                                Prefs.getPrefInstance().setValue(context, Const.USER_FIRST_NAME, response.body().getData().get(0).getFirstname());
                                Prefs.getPrefInstance().setValue(context, Const.USER_LAST_NAME, response.body().getData().get(0).getLastname());
                                Prefs.getPrefInstance().setValue(context, Const.COUNTRY, response.body().getData().get(0).getCountry());
                                Prefs.getPrefInstance().setValue(context, Const.ADDRESS, response.body().getData().get(0).getAddress());
                                Prefs.getPrefInstance().setValue(context, Const.STATE, response.body().getData().get(0).getState());
                                Prefs.getPrefInstance().setValue(context, Const.CITY, response.body().getData().get(0).getCity());
                                Glide
                                        .with(Essentials.applicationContext)
                                        .load(Prefs.getPrefInstance().getValue(context, Const.PROFILE_IMAGE, ""))
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                                        .thumbnail(0.25f)
                                        .error(R.drawable.ic_profile_icon)
                                        .into(userPicture);

                                firstNameInput.setText(response.body().getData().get(0).getFirstname());
                                lastNameInput.setText(response.body().getData().get(0).getLastname());
                                addressInput.setText(response.body().getData().get(0).getAddress());
                                usernameInput.setText(response.body().getData().get(0).getUsername());
                                emailInput.setText(response.body().getData().get(0).getEmail());
                                zipCodeBusinessInput.setText(response.body().getData().get(0).getZipcode());
                                phoneInput.setText(response.body().getData().get(0).getPhone());

                                selectedCountry = response.body().getData().get(0).getCountry();
                                selectedState = response.body().getData().get(0).getState();
                                selectedCity = response.body().getData().get(0).getCity();
                                selectedCategory = response.body().getData().get(0).getHealthcarecategory();
                                getCountry();
                                getHealthCareCategory();
                                if (response.body().getData().get(0).getMonday() != null)
                                    monday.setChecked(response.body().getData().get(0).getMonday().equals(1));
                                if (response.body().getData().get(0).getTuesday() != null)
                                    tuesday.setChecked(response.body().getData().get(0).getTuesday().equals(1));
                                if (response.body().getData().get(0).getWednesday() != null)
                                    wednesday.setChecked(response.body().getData().get(0).getWednesday().equals(1));
                                if (response.body().getData().get(0).getThursday() != null)
                                    thursday.setChecked(response.body().getData().get(0).getThursday().equals(1));
                                if (response.body().getData().get(0).getFriday() != null)
                                    friday.setChecked(response.body().getData().get(0).getFriday().equals(1));
                                if (response.body().getData().get(0).getSaturday() != null)
                                    saturday.setChecked(response.body().getData().get(0).getSaturday().equals(1));
                                if (response.body().getData().get(0).getSunday() != null)
                                    sunday.setChecked(response.body().getData().get(0).getSunday().equals(1));
                                monStartTime.setText(response.body().getData().get(0).getMondayStartTime());
                                monEndTime.setText(response.body().getData().get(0).getMondayEndTime());
                                tueStartTime.setText(response.body().getData().get(0).getTuesdayStartTime());
                                tueEndTime.setText(response.body().getData().get(0).getTuesdayEndTime());
                                wedStartTime.setText(response.body().getData().get(0).getWednesdayStartTime());
                                wedEndTime.setText(response.body().getData().get(0).getWednesdayEndTime());
                                thuStartTime.setText(response.body().getData().get(0).getThursdayStartTime());
                                thuEndTime.setText(response.body().getData().get(0).getThursdayEndTime());
                                friStartTime.setText(response.body().getData().get(0).getFridayStartTime());
                                friEndTime.setText(response.body().getData().get(0).getFridayEndTime());
                                satStartTime.setText(response.body().getData().get(0).getSaturdayStartTime());
                                satEndTime.setText(response.body().getData().get(0).getSaturdayEndTime());
                                sunStartTime.setText(response.body().getData().get(0).getSundayStartTime());
                                sunEndTime.setText(response.body().getData().get(0).getSundayEndTime());
                                businessNameInput.setText(response.body().getData().get(0).getBusinessName());
                                serviceInput.setText(response.body().getData().get(0).getServiceDescription());

                            } else {

                            }
                        } else {

                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(@NonNull Call<ProfileResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();

                }
            });
        } else {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(context.getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }

    }
}
