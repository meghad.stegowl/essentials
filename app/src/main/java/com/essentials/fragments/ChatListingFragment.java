package com.essentials.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.essentials.R;
import com.essentials.activities.Notifications;
import com.essentials.activities.Search;
import com.essentials.adapters.ChatListingAdapter;
import com.essentials.data.models.chat.ChatListingResponse;
import com.essentials.data.models.chat.MessageHistoryData;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.MySnapHelper;
import com.essentials.util.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatListingFragment extends Fragment {

    private static ChatListingFragment instance = null;
    @BindView(R.id.listing)
    RecyclerView listing;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;

    private AppManageInterface appManageInterface;
    private Boolean isCustomer;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private Activity activity;

    public static synchronized ChatListingFragment getInstance() {
        return instance;
    }

    public static synchronized ChatListingFragment newInstance() {
        return instance = new ChatListingFragment();
    }

    public void setNoDataFound() {
        noDataFound.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.search)
    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat_listing, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getChatListing();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        activity = getActivity();

        if (getArguments() != null) {
            isCustomer = getArguments().getBoolean("isCustomer");
        }

        init();
    }

    private void init() {

        listing.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
        getChatListing();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getChatListing() {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_chat_listing(Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""), 100).enqueue(new Callback<ChatListingResponse>() {
                @Override
                public void onResponse(@NonNull Call<ChatListingResponse> call, @NonNull Response<ChatListingResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                listing.setHasFixedSize(true);
                                listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                listing.setAdapter(new ChatListingAdapter(response.body().getData(), context, fragmentManager, isCustomer));
                                listing.setVisibility(View.VISIBLE);
                                noDataFound.setVisibility(View.GONE);
                                loader.setVisibility(View.GONE);
                            } else {
                                loader.setVisibility(View.GONE);
                                noDataFound.setVisibility(View.VISIBLE);
                                listing.setVisibility(View.GONE);
                            }
                        } else {
                            loader.setVisibility(View.GONE);
                            noDataFound.setVisibility(View.VISIBLE);
                            listing.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    }
                }

                @Override
                public void onFailure
                        (@NonNull Call<ChatListingResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    noDataFound.setVisibility(View.VISIBLE);
                    listing.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            loader.setVisibility(View.GONE);
            noDataFound.setVisibility(View.GONE);
            listing.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    public void gotNewMessage(MessageHistoryData data) {
        getChatListing();
    }
}


