package com.essentials.fragments;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.cardview.widget.CardView;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.essentials.R;
import com.essentials.activities.Notifications;
import com.essentials.activities.Search;
import com.essentials.adapters.ListingAdapter;
import com.essentials.data.models.asset.AssetListingResponse;
import com.essentials.data.models.common.CommonResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.MySnapHelper;
import com.essentials.util.Prefs;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssetListingFragment extends Fragment {

    private static AssetListingFragment instance = null;
    @BindView(R.id.listing)
    RecyclerView listing;
    @BindView(R.id.asset_rating)
    AppCompatRatingBar assetRating;
    @BindView(R.id.review_input)
    TextInputEditText reviewInput;
    @BindView(R.id.review_container)
    TextInputLayout reviewContainer;
    @BindView(R.id.button_submit)
    MaterialButton buttonSubmit;
    @BindView(R.id.rate_review_container)
    CardView rateReviewContainer;
    @BindView(R.id.cancel_reason_input)
    TextInputEditText cancelReasonInput;
    @BindView(R.id.cancel_reason_container)
    TextInputLayout cancelReasonContainer;
    @BindView(R.id.button_cancel)
    MaterialButton buttonCancel;
    @BindView(R.id.cancel_appointment_container)
    LinearLayout cancelAppointmentContainer;
    @BindView(R.id.main_container)
    RelativeLayout mainContainer;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    String hourOfTheDay, time, appointmentDate, day, endTime, startAppointmentDate;
    int mHour, mMinute, hours;
    private AppManageInterface appManageInterface;
    private String path, history_id, provider_id,healthcarecategory_id;
    private String type;
    private String contentType = "", pageTitle;
    private int assetGroupClassPath;
    private boolean isCustomer, isRateReview, isCancelAppointment, isStartAppointment, isHistory, isAssetListing;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private boolean loadingInProgress;
    private Activity activity;
    private String latitude, longitude;

    public static synchronized AssetListingFragment getInstance() {
        return instance;
    }

    public static synchronized AssetListingFragment newInstance() {
        return instance = new AssetListingFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_asset_listing, container, false);
    }


    public void setNoDataFound() {
        noDataFound.setVisibility(View.VISIBLE);
        listing.setVisibility(View.GONE);
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.button_submit)
    void setButtonSubmit() {
        if (reviewInput.getText() == null) {
            reviewContainer.setError(getResources().getString(R.string.write_review_error));
        } else if (reviewInput.getText().toString().trim().length() == 0) {
            reviewContainer.setError(getResources().getString(R.string.write_review_error));
        } else if (assetRating.getRating() == 0) {
            AppUtil.show_Snackbar(context, mainContainer, getResources().getString(R.string.rating_error), false);
        } else getRateReview(reviewInput.getText().toString());
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.search)
    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @OnClick(R.id.button_cancel)
    void setButtonCancel() {
        if (startAppointmentDate != null && !startAppointmentDate.isEmpty()) {
            if (time != null && !time.isEmpty()) {
                if (cancelReasonInput.getText() != null && cancelReasonInput.getText().toString().trim().length() > 0) {

//                    String cancelCurrentTime = ;
                    getCancelledAppointment(cancelReasonInput.getText().toString());
                } else
                    cancelReasonContainer.setError(context.getResources().getString(R.string.reason_empty));
            } else {
                AppUtil.show_Snackbar(context, mainContainer, getResources().getString(R.string.please_select_time), false);
            }
        } else {
            AppUtil.show_Snackbar(context, mainContainer, getResources().getString(R.string.please_select_date), false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);
        listing.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);
        rateReviewContainer.setVisibility(View.GONE);
        cancelAppointmentContainer.setVisibility(View.GONE);
        activity = getActivity();
        if (getArguments() != null) {
            path = getArguments().getString("path");
            healthcarecategory_id = getArguments().getString("healthcarecategory_id");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
            history_id = getArguments().getString("history_id");
            provider_id = getArguments().getString("provider_id");
            isCustomer = getArguments().getBoolean("isCustomer");
            isRateReview = getArguments().getBoolean("isRateReview");
            isCancelAppointment = getArguments().getBoolean("isCancelAppointment");
            isStartAppointment = getArguments().getBoolean("isStartAppointment");
            isHistory = getArguments().getBoolean("isHistory");
            isAssetListing = getArguments().getBoolean("isAssetListing");
        }
        init(view);
    }

    private void init(View view) {

//        Calendar startDate = Calendar.getInstance();
//        startDate.add(Calendar.MONTH, 0);
//
//        /* ends after 1 month from now */
//        Calendar endDate = Calendar.getInstance();
//        endDate.add(Calendar.MONTH, 11);
//
//        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
//                .range(startDate, endDate)
//                .datesNumberOnScreen(5)
//                .defaultSelectedDate(Calendar.getInstance())
//                .build();
//
//        appointmentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Calendar.getInstance().getTime());
//        day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(Calendar.getInstance().getTime());
//        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
//            @Override
//            public void onCalendarScroll(HorizontalCalendarView calendarView, int dx, int dy) {
//                super.onCalendarScroll(calendarView, dx, dy);
//
//            }
//
//            @Override
//            public boolean onDateLongClicked(Calendar date, int position) {
//                return super.onDateLongClicked(date, position);
//            }
//
//            @Override
//            public void onDateSelected(Calendar date, int position) {
//                appointmentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date.getTime());
//                day = (String) DateFormat.format("EEEE", date);
//            }
//        });

        if (isRateReview) {
            scrollView.setVisibility(View.VISIBLE);
            rateReviewContainer.setVisibility(View.VISIBLE);
            listing.setVisibility(View.GONE);
            cancelAppointmentContainer.setVisibility(View.GONE);
        } else if (isCancelAppointment) {
            Date currentTime = Calendar.getInstance().getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa",Locale.ENGLISH);
            time = fmtOut.format(currentTime);
            startAppointmentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Calendar.getInstance().getTime());
            scrollView.setVisibility(View.VISIBLE);
            rateReviewContainer.setVisibility(View.GONE);
            listing.setVisibility(View.GONE);
            cancelAppointmentContainer.setVisibility(View.VISIBLE);
        } else if (isStartAppointment) {
            Date currentTime = Calendar.getInstance().getTime();
            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa",Locale.ENGLISH);
            time = fmtOut.format(currentTime);
            startAppointmentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Calendar.getInstance().getTime());
            if (type.toLowerCase().equals("start".toLowerCase())) {
                type = getResources().getString(R.string.start);
            } else {
                type = getResources().getString(R.string.end);
            }
            getCompleteAppointment();
        } else if (isAssetListing) {
            rateReviewContainer.setVisibility(View.GONE);
            cancelAppointmentContainer.setVisibility(View.GONE);
            getAssetListing();
        } else {
            rateReviewContainer.setVisibility(View.GONE);
            cancelAppointmentContainer.setVisibility(View.GONE);
            listing.setVisibility(View.GONE);
            noDataFound.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getAssetListing() {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().get_listing(healthcarecategory_id,Prefs.getPrefInstance().getValue(context, Const.LATITUDE, ""),
                    Prefs.getPrefInstance().getValue(context, Const.LONGITUDE, ""),
                    100, 1).enqueue(new Callback<AssetListingResponse>() {
                @Override
                public void onResponse(@NonNull Call<AssetListingResponse> call, @NonNull Response<AssetListingResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if(response.body().getData().size()== 0){
                                loader.setVisibility(View.GONE);
                                setNoDataFound();
                            }else{
                                listing.setHasFixedSize(true);
                                listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                                listing.setOnFlingListener(null);
                                listing.setAdapter(new ListingAdapter(response.body().getData(), context, fragmentManager, isCustomer, false));
                                new MySnapHelper().attachToRecyclerView(listing);
                                listing.setVisibility(View.VISIBLE);
                                loader.setVisibility(View.GONE);
                                scrollView.setVisibility(View.VISIBLE);
                            }

                        } else {
                            loader.setVisibility(View.GONE);
                            scrollView.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                activity.onBackPressed();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        scrollView.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AssetListingResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            loader.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    private void getRateReview(String reviewText) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.GONE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("history_id", history_id);
                jsonObject.put("customer_id", Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));
                jsonObject.put("provider_id", provider_id);
                jsonObject.put("rate", assetRating.getRating());
                jsonObject.put("review", reviewText);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody get_rate_review = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().get_rate_review(get_rate_review).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            noDataFound.setVisibility(View.GONE);
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                activity.onBackPressed();
                                AppointmentHistoryFragment.getInstance().getAppointmentHistory();
                            });
                        } else {
                            loader.setVisibility(View.GONE);
                            noDataFound.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        noDataFound.setVisibility(View.GONE);
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    noDataFound.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            noDataFound.setVisibility(View.GONE);
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    private void getCompleteAppointment() {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                if (type.equals(getResources().getString(R.string.start))){
                    jsonObject.put("history_id", history_id);
                    jsonObject.put("provider_id", provider_id);
                    jsonObject.put("date", startAppointmentDate);
                    jsonObject.put("start_time", time);
                }else {
                    jsonObject.put("history_id", history_id);
                    jsonObject.put("provider_id", provider_id);
                    jsonObject.put("end_time", time);
                }
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody start_appointment = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().start_appointment(type, start_appointment).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            noDataFound.setVisibility(View.GONE);
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                activity.onBackPressed();
                                AppointmentHistoryFragment.getInstance().getAppointmentHistory();
                            });
                        } else {
                            loader.setVisibility(View.GONE);
                            noDataFound.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                activity.onBackPressed();
                            });
                        }
                    } else {
                        noDataFound.setVisibility(View.GONE);
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                            activity.onBackPressed();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    noDataFound.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                        activity.onBackPressed();
                    });
                }
            });
        } else {
            noDataFound.setVisibility(View.GONE);
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    private void getCancelledAppointment(String reason) {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("history_id", history_id);
                jsonObject.put("provider_id", provider_id);
                jsonObject.put("cancel_time", time);
                jsonObject.put("cancel_date", startAppointmentDate);
                jsonObject.put("cancel_reason", reason);

            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody start_appointment = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().cancel_appointment(start_appointment).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            noDataFound.setVisibility(View.GONE);
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                activity.onBackPressed();
                                AppointmentHistoryFragment.getInstance().getAppointmentHistory();
                            });
                        } else {
                            loader.setVisibility(View.GONE);
                            noDataFound.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        noDataFound.setVisibility(View.GONE);
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    noDataFound.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            noDataFound.setVisibility(View.GONE);
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

//    private void startAppointment() {
//        if (AppUtil.isInternetAvailable(context)) {
//            loader.setVisibility(View.GONE);
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("history_id", history_id);
//                jsonObject.put("provider_id", provider_id);
//                jsonObject.put("end_time", end_time);
//            } catch (JSONException e) {
//                loader.setVisibility(View.GONE);
//                e.printStackTrace();
//            }
//            String params = jsonObject.toString();
//            final RequestBody start_appointment = RequestBody.create(params, MediaType.parse("application/json"));
//            APIUtils.getAPIService().start_appointment(start_appointment).enqueue(new Callback<CommonResponse>() {
//                @Override
//                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
//                    if (response.body() != null) {
//                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
//                            noDataFound.setVisibility(View.GONE);
//                            loader.setVisibility(View.GONE);
//                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
//                            final AlertDialog dialog = new AlertDialog.Builder(context)
//                                    .setCancelable(false)
//                                    .setView(dialog_view)
//                                    .show();
//
//                            if (dialog.getWindow() != null)
//                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);
//
//                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
//                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
//                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
//                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
//                                dialog.dismiss();
//                                activity.onBackPressed();
//                            });
//                        } else {
//                            loader.setVisibility(View.GONE);
//                            noDataFound.setVisibility(View.GONE);
//                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
//                            final AlertDialog dialog = new AlertDialog.Builder(context)
//                                    .setCancelable(false)
//                                    .setView(dialog_view)
//                                    .show();
//
//                            if (dialog.getWindow() != null)
//                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);
//
//                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
//                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
//                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
//                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
//                                dialog.dismiss();
//                            });
//                        }
//                    } else {
//                        noDataFound.setVisibility(View.GONE);
//                        loader.setVisibility(View.GONE);
//                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
//                        final AlertDialog dialog = new AlertDialog.Builder(context)
//                                .setCancelable(false)
//                                .setView(dialog_view)
//                                .show();
//
//                        if (dialog.getWindow() != null)
//                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);
//
//                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
//                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
//                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
//                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
//                            dialog.dismiss();
//                        });
//                    }
//                }
//
//                @Override
//                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                    t.printStackTrace();
//                    loader.setVisibility(View.GONE);
//                    noDataFound.setVisibility(View.GONE);
//                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
//                    final AlertDialog dialog = new AlertDialog.Builder(context)
//                            .setCancelable(false)
//                            .setView(dialog_view)
//                            .show();
//
//                    if (dialog.getWindow() != null)
//                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);
//
//                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
//                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
//                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
//                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
//                        dialog.dismiss();
//                    });
//                }
//            });
//        } else {
//            noDataFound.setVisibility(View.GONE);
//            loader.setVisibility(View.GONE);
//            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
//            final AlertDialog dialog = new AlertDialog.Builder(context)
//                    .setCancelable(false)
//                    .setView(dialog_view)
//                    .show();
//
//            if (dialog.getWindow() != null)
//                dialog.getWindow().getDecorView().getBackground().setAlpha(0);
//
//            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
//            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
//            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
//            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
//                dialog.dismiss();
//            });
//        }
//
//    }

}
