package com.essentials.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.essentials.R;
import com.essentials.activities.Notifications;
import com.essentials.activities.Search;
import com.essentials.data.models.webview.WebViewResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebViewFragment extends Fragment {

    private static WebViewFragment instance = null;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.web_view_container)
    LinearLayout webViewContainer;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.address_container)
    LinearLayout addressContainer;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.phone_container)
    LinearLayout phoneContainer;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.email_container)
    LinearLayout emailContainer;
    @BindView(R.id.website)
    TextView website;
    @BindView(R.id.website_container)
    LinearLayout websiteContainer;
    @BindView(R.id.contact_us_container)
    LinearLayout contactUsContainer;


    private AppManageInterface appManageInterface;
    private String path;
    private String type;
    private String contentType = "", pageTitle;
    private int assetGroupClassPath;
    private boolean isGenre, isGrid, isLanguage;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private boolean loadingInProgress;
    private Activity activity;

    public static synchronized WebViewFragment getInstance() {
        return instance;
    }

    public static synchronized WebViewFragment newInstance() {
        return instance = new WebViewFragment();
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.search)
    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_webview, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        contactUsContainer.setVisibility(View.GONE);
        webViewContainer.setVisibility(View.GONE);
        activity = getActivity();

        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
        }
        show_description();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void show_description() {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().call_web_view().enqueue(new Callback<WebViewResponse>() {
                @Override
                public void onResponse(@NonNull Call<WebViewResponse> call, @NonNull Response<WebViewResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (type.toLowerCase().equals("about".toLowerCase())) {
                                if (response.body().getAboutusData() != null && !response.body().getAboutusData().isEmpty()) {
                                    title.setText(getResources().getString(R.string.about_us));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        description.setText(Html.fromHtml(response.body().getAboutusData().get(0).getDescription(), Html.FROM_HTML_MODE_LEGACY));
                                    } else {
                                        description.setText(Html.fromHtml(response.body().getAboutusData().get(0).getDescription()));
                                    }
                                    scrollView.setVisibility(View.VISIBLE);
                                    description.setVisibility(View.VISIBLE);
                                    webViewContainer.setVisibility(View.VISIBLE);
                                    contactUsContainer.setVisibility(View.GONE);
                                    loader.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.GONE);
                                } else {
                                    scrollView.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.VISIBLE);
                                    loader.setVisibility(View.GONE);
                                }
                            } else if (type.toLowerCase().equals("privacy".toLowerCase())) {
                                if (response.body().getPrivacypolicyData() != null && !response.body().getPrivacypolicyData().isEmpty()) {

                                    title.setText(getResources().getString(R.string.privacy_policy));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        description.setText(Html.fromHtml(response.body().getPrivacypolicyData().get(0).getDescription(), Html.FROM_HTML_MODE_LEGACY));
                                    } else {
                                        description.setText(Html.fromHtml(response.body().getPrivacypolicyData().get(0).getDescription()));
                                    }
                                    scrollView.setVisibility(View.VISIBLE);
                                    description.setVisibility(View.VISIBLE);
                                    webViewContainer.setVisibility(View.VISIBLE);
                                    contactUsContainer.setVisibility(View.GONE);
                                    loader.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.GONE);
                                } else {
                                    scrollView.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.VISIBLE);
                                    loader.setVisibility(View.GONE);
                                }
                            }else if (type.toLowerCase().equals("Return Policy".toLowerCase())) {
                                if (response.body().getPrivacypolicyData() != null && !response.body().getPrivacypolicyData().isEmpty()) {

                                    title.setText(getResources().getString(R.string.return_policy));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        description.setText(Html.fromHtml(response.body().getPrivacypolicyData().get(0).getDescription(), Html.FROM_HTML_MODE_LEGACY));
                                    } else {
                                        description.setText(Html.fromHtml(response.body().getPrivacypolicyData().get(0).getDescription()));
                                    }
                                    scrollView.setVisibility(View.VISIBLE);
                                    description.setVisibility(View.VISIBLE);
                                    webViewContainer.setVisibility(View.VISIBLE);
                                    contactUsContainer.setVisibility(View.GONE);
                                    loader.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.GONE);
                                } else {
                                    scrollView.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.VISIBLE);
                                    loader.setVisibility(View.GONE);
                                }
                            } else {
                                if (response.body().getContactusData() != null && !response.body().getContactusData().isEmpty()) {
                                   address.setText(response.body().getContactusData().get(0).getAddress());
                                   phone.setText(response.body().getContactusData().get(0).getPhone());
                                   email.setText(response.body().getContactusData().get(0).getEmail());
                                   website.setText(response.body().getContactusData().get(0).getWebsite());
                                    webViewContainer.setVisibility(View.GONE);
                                    contactUsContainer.setVisibility(View.VISIBLE);
                                    scrollView.setVisibility(View.VISIBLE);
                                    loader.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.GONE);
                                } else {
                                    scrollView.setVisibility(View.GONE);
                                    noDataFound.setVisibility(View.VISIBLE);
                                    loader.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            scrollView.setVisibility(View.GONE);
                            noDataFound.setVisibility(View.VISIBLE);
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });

                        }
                    } else {
                        scrollView.setVisibility(View.GONE);
                        noDataFound.setVisibility(View.VISIBLE);
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<WebViewResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    scrollView.setVisibility(View.GONE);
                    noDataFound.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

}
