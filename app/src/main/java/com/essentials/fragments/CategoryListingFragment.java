package com.essentials.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.essentials.R;
import com.essentials.activities.Notifications;
import com.essentials.activities.Search;
import com.essentials.adapters.CategoryListingAdapter;
import com.essentials.adapters.ListingAdapter;
import com.essentials.data.models.asset.AssetListingResponse;
import com.essentials.data.models.category.CategoryResponse;
import com.essentials.data.models.category.CategoryResponseData;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.MySnapHelper;
import com.essentials.util.Prefs;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryListingFragment extends Fragment {

    private static CategoryListingFragment instance = null;
    @BindView(R.id.listing)
    RecyclerView listing;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    private String path, history_id, provider_id;
    private String type;
    private String  pageTitle;
    private boolean isCustomer, isRateReview, isCancelAppointment, isStartAppointment, isHistory, isAssetListing;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private Activity activity;
    List<CategoryResponseData> categoryResponse = new ArrayList<>();
    ArrayList<String> categoryList = new ArrayList<>();

    public static synchronized CategoryListingFragment getInstance() {
        return instance;
    }

    public static synchronized CategoryListingFragment newInstance() {
        return instance = new CategoryListingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_category_listing, container, false);
    }

    public void setNoDataFound() {
        noDataFound.setVisibility(View.VISIBLE);
        listing.setVisibility(View.GONE);
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.search)
    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);
        listing.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);
        activity = getActivity();
        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
            history_id = getArguments().getString("history_id");
            provider_id = getArguments().getString("provider_id");
            isCustomer = getArguments().getBoolean("isCustomer");
            isRateReview = getArguments().getBoolean("isRateReview");
            isCancelAppointment = getArguments().getBoolean("isCancelAppointment");
            isStartAppointment = getArguments().getBoolean("isStartAppointment");
            isHistory = getArguments().getBoolean("isHistory");
            isAssetListing = getArguments().getBoolean("isAssetListing");
        }
        getHealthCareCategory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void getHealthCareCategory() {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_category().enqueue(new Callback<CategoryResponse>() {
                @Override
                public void onResponse(@NonNull Call<CategoryResponse> call, @NonNull Response<CategoryResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
//                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
//                                categoryResponse = new ArrayList<>();
//                                categoryResponse = response.body().getData();
//                                categoryList = new ArrayList<>();
//                                categoryList.add("Select Essentials Category");
//                                for (int i = 0; i < response.body().getData().size(); i++) {
//                                    categoryList.add(response.body().getData().get(i).getType());
//                                }
//
//                            }

                            listing.setHasFixedSize(true);
                            listing.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
                            listing.setOnFlingListener(null);
                            listing.setAdapter(new CategoryListingAdapter(response.body().getData(), context, fragmentManager, isCustomer, false));
                            new MySnapHelper().attachToRecyclerView(listing);
                            listing.setVisibility(View.VISIBLE);
                            loader.setVisibility(View.GONE);
                            scrollView.setVisibility(View.VISIBLE);
                        }
                        else {
                            loader.setVisibility(View.GONE);
                            scrollView.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                activity.onBackPressed();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        scrollView.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CategoryResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            loader.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

}
