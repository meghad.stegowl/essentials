package com.essentials.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.activities.AssetListing;
import com.essentials.activities.Chat;
import com.essentials.activities.Notifications;
import com.essentials.activities.RequestAppointment;
import com.essentials.activities.Search;
import com.essentials.activities.SubmitAppointment;
import com.essentials.application.Essentials;
import com.essentials.data.models.asset.AssetDetailsResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.material.button.MaterialButton;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

public class RequestAppointmentFragment extends Fragment {

    private static RequestAppointmentFragment instance = null;
    String hourOfTheDay;
    int mHour, mMinute, hours;
    @BindView(R.id.asset_image)
    CircularImageView assetImage;
    @BindView(R.id.asset_name)
    TextView assetName;
    @BindView(R.id.business_name)
    TextView businessName;
    @BindView(R.id.asset_category)
    TextView assetCategory;
    @BindView(R.id.asset_rating)
    RatingBar assetRating;
    @BindView(R.id.asset_rating_count)
    TextView assetRatingCount;
    @BindView(R.id.calendarView)
    HorizontalCalendarView calendarView;
    @BindView(R.id.time_picker)
    TextView timePicker;
    @BindView(R.id.button_confirm)
    MaterialButton buttonConfirm;
    @BindView(R.id.calender_container)
    LinearLayout calenderContainer;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.no_data_found)
    TextView noDataFound;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.main_container)
    LinearLayout mainContainer;
    float rating;
    @BindView(R.id.button_call)
    ImageView buttonCall;
    @BindView(R.id.chat)
    ImageView chat;
    @BindView(R.id.chat_calling_container)
    LinearLayout chatCallingContainer;
    private AppManageInterface appManageInterface;
    private String path, type, pageTitle, time, appointmentDate, showDate, name,businessname, image, categoryName, ratingCount, day, healthCategoryId, provider_name;
    private String contentType = "";
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private Activity activity;
    private Boolean isCustomer;
    private double provider_lat, provider_long;
    private String mon_start, mon_end, tue_start, tue_end, wed_start, wed_end, thu_start, thu_end, fri_start, fri_end, sat_start, sat_end, sun_start, sun_end;
    private Boolean isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday, isSunday;

    @BindView(R.id.tv_mon_start)
    TextView tv_mon_start;
    @BindView(R.id.tv_tue_start)
    TextView tv_tue_start;
    @BindView(R.id.tv_wed_start)
    TextView tv_wed_start;
    @BindView(R.id.tv_thurs_start)
    TextView tv_thurs_start;
    @BindView(R.id.tv_fri_start)
    TextView tv_fri_start;
    @BindView(R.id.tv_sat_start)
    TextView tv_sat_start;
    @BindView(R.id.tv_sun_start)
    TextView tv_sun_start;
    @BindView(R.id.tv_mon_end)
    TextView tv_mon_end;
    @BindView(R.id.tv_tue_end)
    TextView tv_tue_end;
    @BindView(R.id.tv_wed_end)
    TextView tv_wed_end;
    @BindView(R.id.tv_thurs_end)
    TextView tv_thurs_end;
    @BindView(R.id.tv_fri_end)
    TextView tv_fri_end;
    @BindView(R.id.tv_sat_end)
    TextView tv_sat_end;
    @BindView(R.id.tv_sun_end)
    TextView tv_sun_end;


    public static synchronized RequestAppointmentFragment getInstance() {
        return instance;
    }

    public static synchronized RequestAppointmentFragment newInstance() {
        return instance = new RequestAppointmentFragment();
    }

    public void setNoDataFound() {
        noDataFound.setVisibility(View.VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_request_appointment, container, false);
    }

    @OnClick(R.id.button_confirm)
    void setButtonConfirm() {
        if (appointmentDate != null && !appointmentDate.isEmpty()) {
            if (time != null && !time.isEmpty()) {
                startActivity(new Intent(context, SubmitAppointment.class).putExtra("time", time).putExtra("date", appointmentDate)
                        .putExtra("showDate", showDate).putExtra("path", path).putExtra("assetName", name)
                        .putExtra("image", image).putExtra("rating", rating).putExtra("categoryName", categoryName)
                        .putExtra("ratingCount", ratingCount).putExtra("day", day).putExtra("isCustomer", isCustomer)
                        .putExtra("healthCategoryId", healthCategoryId));
            } else {
                AppUtil.show_Snackbar(context, calenderContainer, getResources().getString(R.string.please_select_time_request), false);
            }
        } else {
            AppUtil.show_Snackbar(context, calenderContainer, getResources().getString(R.string.please_select_date), false);
        }
    }

    @OnClick(R.id.back)
    void setBack() {
        activity.onBackPressed();
    }

    @OnClick(R.id.search)
    void setSearchClick() {
        startActivity(new Intent(context, Search.class));
    }

    @OnClick(R.id.time_picker)
    void setTimePicker() {
        if ((!isTuesday && day.toLowerCase().equals("tuesday")) || (!isMonday && day.toLowerCase().equals("monday"))
                || (!isWednesday && day.toLowerCase().equals("wednesday")) || (!isThursday && day.toLowerCase().equals("thursday"))
                || (!isFriday && day.toLowerCase().equals("friday")) || (!isSaturday && day.toLowerCase().equals("saturday"))
                || (!isSunday && day.toLowerCase().equals("sunday"))) {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);
            String message = provider_name + " " + getResources().getString(R.string.correct_time_selection_error);
            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(message);
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.choose_provider));
            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                dialog.dismiss();
                startActivity(new Intent(context, AssetListing.class).putExtra("isCustomer", isCustomer));
                ((RequestAppointment) context).finish();
            });
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        } else {
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {
                String formatTime = hourOfDay + ":" + minute;
                SimpleDateFormat fmt = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
                Date date = null;
                try {
                    date = fmt.parse(formatTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
                String formattedTime = fmtOut.format(date);
                time = formattedTime;
                timePicker.setText(formattedTime);
//            if (hourOfDay >= 12) {
//                if (hourOfDay != 12) hours = hourOfDay - 12;
//                else hours = hourOfDay;
//                hourOfTheDay = "PM";
//            } else {
//                if (hourOfDay != 0)
//                    hours = hourOfDay;
//                else hours = hourOfDay + 12;
//                hourOfTheDay = "AM";
//            }
//            time = hours + ":" + minute + " " + hourOfTheDay;
//            timePicker.setText(time);
            }, mHour, mMinute, false);
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
            Calendar calendar = Calendar.getInstance();
            Date d;
            if (day.toLowerCase().equals("monday")) {
                try {
                    d = sdf.parse(mon_start);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMinTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }
                    d = sdf.parse(mon_end);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMaxTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            } else if (day.toLowerCase().equals("tuesday")) {
                try {
                    d = sdf.parse(tue_start);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMinTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }

                    d = sdf.parse(tue_end);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMaxTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            } else if (day.toLowerCase().equals("wednesday")) {
                try {
                    d = sdf.parse(wed_start);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMinTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }

                    d = sdf.parse(wed_end);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMaxTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            } else if (day.toLowerCase().equals("thursday")) {
                try {
                    d = sdf.parse(thu_start);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMinTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }

                    d = sdf.parse(thu_end);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMaxTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            } else if (day.toLowerCase().equals("friday")) {
                try {
                    d = sdf.parse(fri_start);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMinTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }

                    d = sdf.parse(fri_end);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMaxTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            } else if (day.toLowerCase().equals("saturday")) {
                try {
                    d = sdf.parse(sat_start);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMinTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }

                    d = sdf.parse(sat_end);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMaxTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            } else {
                try {
                    d = sdf.parse(sun_start);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMinTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }

                    d = sdf.parse(sun_end);
                    if (d != null) {
                        calendar.setTime(d);
                        timePickerDialog.setMaxTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                    }
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
            timePickerDialog.show(getParentFragmentManager(), getTag());
        }
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.chat)
    void setChat() {
        startActivity(new Intent(context, Chat.class).putExtra("isCustomer", isCustomer).putExtra("path", path));
    }

    @OnClick(R.id.button_call)
    void setCall() {
        String[] PERMISSIONS = {Manifest.permission.CALL_PHONE};

        if (!AppUtil.hasPermissions(context, PERMISSIONS)) {
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.call_permission_request));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog.dismiss());
            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(v -> {
                dialog.dismiss();
                askPermission(RequestAppointmentFragment.this,
                        Manifest.permission.CALL_PHONE)
                        .onAccepted(result -> call_phone())
                        .onDenied(result -> {
                            View dialog_view1 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog1 = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view1)
                                    .show();

                            if (dialog1.getWindow() != null)
                                dialog1.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view1.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.call_permission_Deny));
                            ((Button) dialog_view1.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                            ((Button) dialog_view1.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

                            dialog_view1.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog1.dismiss());
                            dialog_view1.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog1.dismiss();
                                result.askAgain();
                            });
                        })
                        .onForeverDenied(result -> {
                            View dialog_view2 = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog2 = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view2)
                                    .show();

                            if (dialog2.getWindow() != null)
                                dialog2.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view2.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.call_permission_Forever_Deny));
                            ((Button) dialog_view2.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                            ((Button) dialog_view2.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.go_to_settings));

                            dialog_view2.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog2.dismiss());
                            dialog_view2.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog2.dismiss();
                                result.goToSettings();
                            });
                        })
                        .ask();
            });
        } else{
            call_phone();
        }
    }

    private void call_phone(){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" +Prefs.getPrefInstance().getValue(context, Const.PHONE_NUMBER, "")));
        startActivity(callIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);
        noDataFound.setVisibility(View.GONE);
        mainContainer.setVisibility(View.GONE);
        activity = getActivity();

        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            image = getArguments().getString("image");
            rating = getArguments().getFloat("rating");
            Log.d("mytag", "rating - " + rating);
            name = getArguments().getString("assetName");
            businessname = getArguments().getString("businessname");
            pageTitle = getArguments().getString("title");
            categoryName = getArguments().getString("categoryName");
            ratingCount = getArguments().getString("ratingCount");
            healthCategoryId = getArguments().getString("healthCategoryId");
            isCustomer = getArguments().getBoolean("isCustomer");
        }

        init(view);
    }

    private void init(View view) {
        getAssetDetails();

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 0);

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 11);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .defaultSelectedDate(Calendar.getInstance())
                .build();
        appointmentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Calendar.getInstance().getTime());
        getFormattedDate(Calendar.getInstance().getTime());
        Log.d("mytag", "date - "+showDate);
//        showDate = new SimpleDateFormat("MMMM ddd yyyy", Locale.US).format(Calendar.getInstance().getTime());

        day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(Calendar.getInstance().getTime());

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView, int dx, int dy) {
                super.onCalendarScroll(calendarView, dx, dy);
            }

            @Override
            public boolean onDateLongClicked(Calendar date, int position) {
                return super.onDateLongClicked(date, position);
            }

            @Override
            public void onDateSelected(Calendar date, int position) {
                appointmentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date.getTime());
//                showDate = new SimpleDateFormat("MMMM ddd yyyy", Locale.US).format(Calendar.getInstance().getTime());
                getFormattedDate(date.getTime());
                Log.d("mytag", "date - "+showDate);
                day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());

                if ((!isTuesday && day.toLowerCase().equals("tuesday")) || (!isMonday && day.toLowerCase().equals("monday"))
                        || (!isWednesday && day.toLowerCase().equals("wednesday")) || (!isThursday && day.toLowerCase().equals("thursday"))
                        || (!isFriday && day.toLowerCase().equals("friday")) || (!isSaturday && day.toLowerCase().equals("saturday"))
                        || (!isSunday && day.toLowerCase().equals("sunday"))) {
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);
                    String message = provider_name + " " + getResources().getString(R.string.correct_time_selection_error);
                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(message);
                    ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.choose_provider));
                    dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                        dialog.dismiss();
                        startActivity(new Intent(context, AssetListing.class).putExtra("isCustomer", isCustomer));
                        ((RequestAppointment) context).finish();
                    });
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            }
        });

        assetName.setText(name);
        businessName.setText(businessname);
        assetCategory.setText(categoryName);
        Log.d("mytag", "rating - " + rating);
        assetRating.setRating(rating);
        if (ratingCount != null && !ratingCount.isEmpty() && !ratingCount.equals("(0)")) {
            assetRatingCount.setVisibility(View.VISIBLE);
            assetRating.setVisibility(View.VISIBLE);
        } else {
            assetRatingCount.setVisibility(View.GONE);
            assetRating.setVisibility(View.GONE);
        }
        assetRatingCount.setText(ratingCount);
        assetCategory.setText(categoryName);

        Glide
                .with(Essentials.applicationContext)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                .thumbnail(0.25f)
                .error(R.drawable.ic_profile_icon)
                .into(assetImage);
        mainContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getAssetDetails() {
        if (AppUtil.isInternetAvailable(context)) {
            loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().get_asset_details(Prefs.getPrefInstance().getValue(context, Const.LATITUDE, ""), Prefs.getPrefInstance().getValue(context, Const.LONGITUDE, ""), path, 100, 1).enqueue(new Callback<AssetDetailsResponse>() {
                @Override
                public void onResponse(@NonNull Call<AssetDetailsResponse> call, @NonNull Response<AssetDetailsResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            provider_name = response.body().getProviderData().get(0).getFirstname() + " " + response.body().getProviderData().get(0).getLastname();
                            isMonday = response.body().getProviderData().get(0).getMonday().equals("1");
                            isTuesday = response.body().getProviderData().get(0).getTuesday().equals("1");
                            isWednesday = response.body().getProviderData().get(0).getWednesday().equals("1");
                            isThursday = response.body().getProviderData().get(0).getThursday().equals("1");
                            isFriday = response.body().getProviderData().get(0).getFriday().equals("1");
                            isSaturday = response.body().getProviderData().get(0).getSaturday().equals("1");
                            isSunday = response.body().getProviderData().get(0).getSunday().equals("1");

                            mon_start = response.body().getProviderData().get(0).getMondayStartTime();
                            mon_end = response.body().getProviderData().get(0).getMondayEndTime();
                            tue_start = response.body().getProviderData().get(0).getTuesdayStartTime();
                            tue_end = response.body().getProviderData().get(0).getTuesdayEndTime();
                            wed_start = response.body().getProviderData().get(0).getWednesdayStartTime();
                            wed_end = response.body().getProviderData().get(0).getWednesdayEndTime();
                            thu_start = response.body().getProviderData().get(0).getThursdayStartTime();
                            thu_end = response.body().getProviderData().get(0).getThursdayEndTime();
                            fri_start = response.body().getProviderData().get(0).getFridayStartTime();
                            fri_end = response.body().getProviderData().get(0).getFridayEndTime();
                            sat_start = response.body().getProviderData().get(0).getSaturdayStartTime();
                            sat_end = response.body().getProviderData().get(0).getSaturdayEndTime();
                            sun_start = response.body().getProviderData().get(0).getSundayStartTime();
                            sun_end = response.body().getProviderData().get(0).getSundayEndTime();

                            if(response.body().getProviderData().get(0).getMondayStartTime().equals("")){
                                tv_mon_start.setText("Not Available");
                                tv_mon_end.setText("Not Available");
                            }else{
                                tv_mon_start.setText(response.body().getProviderData().get(0).getMondayStartTime());
                                tv_mon_end.setText(response.body().getProviderData().get(0).getMondayEndTime());
                            }

                            if(response.body().getProviderData().get(0).getTuesdayStartTime().equals("")){
                                tv_tue_start.setText("Not Available");
                                tv_tue_end.setText("Not Available");
                            }else{
                                tv_tue_start.setText(response.body().getProviderData().get(0).getTuesdayStartTime());
                                tv_tue_end.setText(response.body().getProviderData().get(0).getTuesdayEndTime());
                            }

                            if(response.body().getProviderData().get(0).getWednesdayStartTime().equals("")){
                                tv_wed_start.setText("Not Available");
                                tv_wed_end.setText("Not Available");
                            }else{
                                tv_wed_start.setText(response.body().getProviderData().get(0).getWednesdayStartTime());
                                tv_wed_end.setText(response.body().getProviderData().get(0).getWednesdayEndTime());
                            }

                            if(response.body().getProviderData().get(0).getThursdayStartTime().equals("")){
                                tv_thurs_start.setText("Not Available");
                                tv_thurs_end.setText("Not Available");
                            }else{
                                tv_thurs_start.setText(response.body().getProviderData().get(0).getThursdayStartTime());
                                tv_thurs_end.setText(response.body().getProviderData().get(0).getThursdayEndTime());
                            }

                            if(response.body().getProviderData().get(0).getFridayStartTime().equals("")){
                                tv_fri_start.setText("Not Available");
                                tv_fri_end.setText("Not Available");
                            }else{
                                tv_fri_start.setText(response.body().getProviderData().get(0).getFridayStartTime());
                                tv_fri_end.setText(response.body().getProviderData().get(0).getFridayEndTime());
                            }

                            if(response.body().getProviderData().get(0).getSaturdayStartTime().equals("")){
                                tv_sat_start.setText("Not Available");
                                tv_sat_end.setText("Not Available");
                            }else{
                                tv_sat_start.setText(response.body().getProviderData().get(0).getSaturdayStartTime());
                                tv_sat_end.setText(response.body().getProviderData().get(0).getSaturdayEndTime());
                            }

                            if(response.body().getProviderData().get(0).getSundayStartTime().equals("")){
                                tv_sun_start.setText("Not Available");
                                tv_sun_end.setText("Not Available");
                            }else{
                                tv_sun_start.setText(response.body().getProviderData().get(0).getSundayStartTime());
                                tv_sun_end.setText(response.body().getProviderData().get(0).getSundayEndTime());
                            }

                            Glide
                                    .with(Essentials.applicationContext)
                                    .load(response.body().getProviderData().get(0).getImage())
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                                    .thumbnail(0.25f)
                                    .error(R.drawable.ic_profile_icon)
                                    .into(assetImage);
                            mainContainer.setVisibility(View.VISIBLE);

                            loader.setVisibility(View.GONE);
                        } else {
                            loader.setVisibility(View.GONE);
                            mainContainer.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(context)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        mainContainer.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AssetDetailsResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    mainContainer.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(context)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            mainContainer.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    public String getFormattedDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        //2nd of march 2015
        int day = cal.get(Calendar.DATE);

        switch (day % 10) {
            case 1:
                showDate = new SimpleDateFormat("MMMM d'st', yyyy", Locale.ENGLISH).format(date);
                break;
            case 2:
                showDate =  new SimpleDateFormat("MMMM d'nd', yyyy", Locale.ENGLISH).format(date);
                break;
            case 3:
                showDate =  new SimpleDateFormat("MMMM d'rd', yyyy", Locale.ENGLISH).format(date);
                break;
            default:
                showDate =  new SimpleDateFormat("MMMM d'th', yyyy", Locale.ENGLISH).format(date);
                break;
        }return showDate;
    }
}


