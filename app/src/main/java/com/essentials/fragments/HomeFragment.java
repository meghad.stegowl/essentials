package com.essentials.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import com.essentials.R;
import com.essentials.activities.AssetListing;
import com.essentials.activities.CategoryListing;
import com.essentials.activities.HomeScreen;
import com.essentials.activities.Notifications;
import com.essentials.activities.Search;
import com.essentials.adapters.CarousalAdapter;
import com.essentials.data.models.intro_slider.IntroSliderData;
import com.essentials.data.models.intro_slider.IntroSliderResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator3;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private static HomeFragment instance = null;
    @BindView(R.id.drawer)
    ImageView drawer;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.carousal)
    ViewPager2 carousal;
    @BindView(R.id.indicator)
    CircleIndicator3 indicator;
    @BindView(R.id.button_view_listing)
    MaterialButton buttonViewListing;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    ArrayList<IntroSliderData> data = new ArrayList<>();
    boolean isLogin, isCustomer;
    @BindView(R.id.search_input)
    EditText searchInput;
    @BindView(R.id.search)
    ImageView search;
    private AppManageInterface appManageInterface;
    private String path;
    private String type;
    private String contentType = "", pageTitle;
    private int assetGroupClassPath;
    private boolean isGenre, isGrid, isLanguage;
    private Context context = getContext();
    private FragmentManager fragmentManager;
    private boolean loadingInProgress;
    private CarousalAdapter carousalAdapter;
    private Handler handler;
    private String latitude, longitude;
    private Runnable runnable;

    public static synchronized HomeFragment getInstance() {
        return instance;
    }

    public static synchronized HomeFragment newInstance() {
        return instance = new HomeFragment();
    }

//    @OnClick(R.id.search)
//    void setSearchClicked() {
////        loader.setVisibility(View.VISIBLE);
//        if (searchInput.getText() == null) {
//            AppUtil.show_Snackbar(context, searchInput, getString(R.string.type_to_search), true);
//        } else if (searchInput.getText().toString().trim().length() < 1) {
//            AppUtil.show_Snackbar(context, searchInput, getString(R.string.type_to_search), true);
//        } else {
//            startActivity(new Intent(context, Search.class).putExtra("keyword", searchInput.getText().toString()));
//        }
//    }

    @OnClick(R.id.drawer)
    void setDrawer() {
        appManageInterface.lock_unlock_Drawer();
    }

    @OnClick(R.id.button_view_listing)
    void setButtonViewListing() {
//        startActivity(new Intent(context, AssetListing.class).putExtra("isCustomer", isCustomer)
//                .putExtra("isRateReview", false).putExtra("isCancelAppointment", false)
//                .putExtra("isStartAppointment", false)
//                .putExtra("isAssetListing", true));
        startActivity(new Intent(context, CategoryListing.class).putExtra("isCustomer", isCustomer)
                .putExtra("isRateReview", false).putExtra("isCancelAppointment", false)
                .putExtra("isStartAppointment", false)
                .putExtra("isAssetListing", true));
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(context, Notifications.class));
    }

    @OnClick(R.id.search_input)
    void setSearchInput() {
        startActivity(new Intent(context, Search.class));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        context = getContext();
        fragmentManager = getParentFragmentManager();
        loader.setVisibility(View.GONE);

        getCarousal();

        if (getArguments() != null) {
            path = getArguments().getString("path");
            type = getArguments().getString("type");
            pageTitle = getArguments().getString("title");
            latitude = getArguments().getString("latitude");
            longitude = getArguments().getString("longitude");
            isLogin = getArguments().getBoolean("isLogin");
            isCustomer = getArguments().getBoolean("isCustomer");
        }
//        searchContainer.setEndIconOnClickListener(v -> {
//            if (searchInput.getText() == null) {
//                searchContainer.setError(getResources().getString(R.string.type_to_search));
//            } else if (searchInput.getText().toString().trim().length() < 1) {
//                searchContainer.setError(getResources().getString(R.string.type_to_search));
//            } else {
//                startActivity(new Intent(context, Search.class).putExtra("keyword", searchInput.getText().toString()));
//            }
//        });
//
//        searchInput.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                searchContainer.setError("");
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = (HomeScreen) context;
        try {
            appManageInterface = (AppManageInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement Interface");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getCarousal() {
        if (AppUtil.isInternetAvailable(context)) {
            APIUtils.getAPIService().get_intro_slider(getResources().getString(R.string.home)).enqueue(new Callback<IntroSliderResponse>() {
                @Override
                public void onResponse(@NonNull Call<IntroSliderResponse> call, @NonNull Response<IntroSliderResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                carousalAdapter = new CarousalAdapter(context, response.body().getData(), fragmentManager);
                                carousal.setAdapter(carousalAdapter);
                                carousal.setOffscreenPageLimit(response.body().getData().size());
                                carousal.setCurrentItem(0, true);
                                carousal.getChildAt(0).setOverScrollMode(View.OVER_SCROLL_NEVER);
                                indicator.setVisibility(View.VISIBLE);
                                indicator.setViewPager(carousal);

                                final int speedScroll = 5000;
                                handler = new Handler();
                                runnable = new Runnable() {
                                    int count = 0;
                                    boolean flag = true;

                                    @Override
                                    public void run() {
                                        if (count < carousalAdapter.getItemCount()) {
                                            if (count == carousalAdapter.getItemCount() - 1) {
                                                flag = false;
                                            } else if (count == 0) {
                                                flag = true;
                                            }
                                            if (flag) count++;
                                            else count--;
                                            carousal.setCurrentItem(count, true);
                                            handler.postDelayed(this, speedScroll);
                                        }
                                    }
                                };
                                handler.postDelayed(runnable, speedScroll);
                                carousal.setVisibility(View.VISIBLE);
                            } else {
                                carousal.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        carousal.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<IntroSliderResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    carousal.setVisibility(View.GONE);
                }
            });
        } else {
            carousal.setVisibility(View.GONE);
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(context)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(context.getResources().getString(R.string.no_internet_connection));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);

            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                dialog.dismiss();
            });

        }
    }
}
