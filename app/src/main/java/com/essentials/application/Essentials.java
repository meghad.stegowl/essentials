package com.essentials.application;

import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Point;
import android.os.Handler;
import android.view.Display;
import android.view.WindowManager;

import com.essentials.R;
import com.essentials.activities.Login;
import com.essentials.util.Const;
import com.essentials.util.CrashHandler;
import com.essentials.util.NetworkStatusChangeReceiver;
import com.essentials.util.Prefs;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.stripe.android.PaymentConfiguration;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

public class Essentials extends Application {
    public static Context applicationContext = null;
    public static volatile Handler applicationHandler = null;
    public static Point displaySize = new Point();
    private static Tracker sTracker;
    private static GoogleAnalytics sAnalytics;
    public static NetworkStatusChangeReceiver networkStatusChangeReceiver;

    public static void checkDisplaySize() {
        try {
            WindowManager manager = (WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE);
            if (manager != null) {
                Display display = manager.getDefaultDisplay();
                if (display != null) {
                    display.getSize(displaySize);
                }
            }
        } catch (Exception e) {
        }
    }

    static synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sAnalytics = GoogleAnalytics.getInstance(this);
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/dejaVuSans.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        applicationContext = getApplicationContext();
        Prefs.getPrefInstance().setValue(applicationContext, Const.TIME, String.valueOf(System.currentTimeMillis()));

//        PaymentConfiguration.init(getApplicationContext(), "pk_test_zYnCgv6nLRc5ZCZvdnqR4fRS");
        PaymentConfiguration.init(getApplicationContext(), getApplicationContext().getResources().getString(R.string.stripe_key));
        Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(getApplicationContext()));
        applicationHandler = new Handler(applicationContext.getMainLooper());
        checkDisplaySize();
        networkStatusChangeReceiver = new NetworkStatusChangeReceiver();
        IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.registerReceiver(networkStatusChangeReceiver, filter);
    }
}
