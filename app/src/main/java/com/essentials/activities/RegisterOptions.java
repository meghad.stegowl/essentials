package com.essentials.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ContentLoadingProgressBar;

import com.essentials.R;
import com.essentials.data.models.payment.PaymentResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.fragments.SignUpFragment;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.view.CardInputListener;
import com.stripe.android.view.CardMultilineWidget;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterOptions extends AppCompatActivity {

    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.patient)
    MaterialCheckBox patient;
    @BindView(R.id.health_care_provider)
    MaterialCheckBox healthCareProvider;
    @BindView(R.id.button_get_started)
    MaterialButton buttonGetStarted;
    @BindView(R.id.layout_container)
    LinearLayout layoutContainer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.main_container)
    RelativeLayout mainContainer;
    @BindView(R.id.create_profile_text)
    TextView createProfileText;
    @BindView(R.id.create_profile_text_second)
    TextView createProfileTextSecond;
    boolean isLogin;
    @BindView(R.id.button_guest_login)
    MaterialButton buttonGuestLogin;

    private static String amount;

    @OnClick(R.id.back)
    public void setBack() {
        onBackPressed();
    }

    @OnClick(R.id.patient)
    public void setPatient() {
        healthCareProvider.setChecked(false);
    }

    @OnClick(R.id.button_guest_login)
    public void setButtonGuestLogin() {
        Prefs.getPrefInstance().setValue(RegisterOptions.this, Const.LOGIN_ACCESS, getString(R.string.skipped));
        startActivity(new Intent(RegisterOptions.this, HomeScreen.class).putExtra("isLogin", false).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }

    @OnClick(R.id.health_care_provider)
    public void setHealthCareProvider() {
        patient.setChecked(false);
    }

    @OnClick(R.id.button_get_started)
    public void setButtonGetStarted() {
        if (!healthCareProvider.isChecked() && !patient.isChecked()) {
            AppUtil.show_Snackbar(RegisterOptions.this, mainContainer, getResources().getString(R.string.check_at_least_one), false);
        } else {
            if (isLogin) {
                startActivity(new Intent(RegisterOptions.this, Login.class).putExtra("isCustomer", patient.isChecked()));
            } else {
                startActivity(new Intent(RegisterOptions.this, SignUp.class).putExtra("isCustomer", patient.isChecked()));
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_options);
        ButterKnife.bind(this);
        loader.setVisibility(View.GONE);
        isLogin = getIntent().getBooleanExtra("isLogin", false);
        getPaymentAmount();

        if (isLogin) {
            createProfileText.setText(getResources().getString(R.string.login_as));
            createProfileTextSecond.setText(getResources().getString(R.string.login_as_an));
            back.setVisibility(View.INVISIBLE);
        } else {
            createProfileText.setText(getResources().getString(R.string.create_a_profile_as));
            createProfileTextSecond.setText(getResources().getString(R.string.create_a_profile_as_an));
            back.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private void getPaymentAmount() {
        if (AppUtil.isInternetAvailable(RegisterOptions.this)) {
            loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().get_payment_amount().enqueue(new Callback<PaymentResponse>() {
                @Override
                public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            loader.setVisibility(View.GONE);
                            amount = response.body().getAmount();
                            Prefs.getPrefInstance().setValue(RegisterOptions.this, Const.PAYMENT_AMOUNT, amount);
                        } else {
                            loader.setVisibility(View.GONE);
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);

                }
            });
        } else {
            loader.setVisibility(View.GONE);
        }
    }
}