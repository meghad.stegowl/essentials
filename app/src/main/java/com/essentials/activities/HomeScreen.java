package com.essentials.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.application.Essentials;
import com.essentials.data.models.common.CommonResponse;
import com.essentials.data.models.user.ProfileResponse;
import com.essentials.data.models.webview.WebViewResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.fragments.HomeFragment;
import com.essentials.util.AppManageInterface;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.button.MaterialButton;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.github.florent37.runtimepermission.RuntimePermission.askPermission;

public class HomeScreen extends AppCompatActivity implements AppManageInterface, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "mytag";
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    RecyclerView.Adapter adapter = null;
    ActionBarDrawerToggle drawerToggle;
    @BindView(R.id.frame_layout)
    FrameLayout frameLayout;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.about_us)
    ImageView aboutUs;
    @BindView(R.id.view_local_listing)
    ImageView viewLocalListing;
    @BindView(R.id.share_our_app)
    ImageView shareOurApp;
    @BindView(R.id.social_media)
    ImageView socialMedia;
    @BindView(R.id.privacy_policy)
    ImageView privacyPolicy;
    @BindView(R.id.button_post_service)
    MaterialButton buttonPostService;
    @BindView(R.id.layout)
    ConstraintLayout layout;
    @BindView(R.id.profile_image)
    CircularImageView profileImage;
    @BindView(R.id.profile_title)
    TextView profileTitle;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.edit_profile)
    ImageView editProfile;
    @BindView(R.id.profile_container)
    LinearLayout profileContainer;
    @BindView(R.id.drawer)
    LinearLayout drawer;
    @BindView(R.id.drawerContainer)
    DrawerLayout drawerContainer;
    boolean isGuestLogin;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.logout)
    LinearLayout logout;
    @BindView(R.id.logout_text)
    TextView logoutText;
    @BindView(R.id.about_us_container)
    LinearLayout aboutUsContainer;
    @BindView(R.id.view_listing_container)
    LinearLayout viewListingContainer;
    @BindView(R.id.share_our_app_container)
    LinearLayout shareOurAppContainer;
    @BindView(R.id.social_media_container)
    LinearLayout socialMediaContainer;
    @BindView(R.id.privacy_policy_container)
    LinearLayout privacyPolicyContainer;
    @BindView(R.id.return_policy_container)
    LinearLayout return_policy_container;
    @BindView(R.id.profile_name_container)
    LinearLayout profileNameContainer;
    @BindView(R.id.post_your_service_container)
    LinearLayout postYourServiceContainer;
    @BindView(R.id.change_password)
    ImageView changePassword;
    @BindView(R.id.change_password_container)
    LinearLayout changePasswordContainer;
    @BindView(R.id.contact_us)
    ImageView contactUs;
    @BindView(R.id.contact_us_container)
    LinearLayout contactUsContainer;
    @BindView(R.id.chat_container)
    LinearLayout chatContainer;
    @BindView(R.id.history_container)
    LinearLayout historyContainer;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    String mLastUpdateTime, shareDetails;
    private String latitude, longitude;
    private LocationListener locationListener;
    String type;
    Boolean isCustomer;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        context = HomeScreen.this;
        ButterKnife.bind(this);
        if (!Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.LOGIN_ACCESS, "").equals(Essentials.applicationContext.getResources().getString(R.string.logged_in)) && !Prefs.getPrefInstance().getValue(Essentials.applicationContext, Const.LOGIN_ACCESS, "").equals(Essentials.applicationContext.getResources().getString(R.string.skipped))) {
            startActivity(new Intent(this, SplashScreen.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        }

        isCustomer = getIntent().getBooleanExtra("isCustomer", false);

        if (savedInstanceState == null) {
            Fragment fragment = HomeFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isLogin", getIntent().getBooleanExtra("isLogin", false));
            bundle.putBoolean("isCustomer", getIntent().getBooleanExtra("isCustomer", false));
            bundle.putString("latitude", latitude);
            bundle.putString("longitude", longitude);
            fragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .commitNow();
        }

        init();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @OnClick(R.id.back)
    void setBackClick() {
        go_back();
    }

    @OnClick(R.id.notification)
    void setNotification() {
        startActivity(new Intent(HomeScreen.this, Notifications.class));
    }

    @OnClick(R.id.search)
    void setSearch() {
        startActivity(new Intent(HomeScreen.this, Search.class).putExtra("isCustomer", isCustomer).putExtra("keyword", ""));
    }

    @OnClick(R.id.edit_profile)
    void setEditProfile() {
        if (isCustomer)
            startActivity(new Intent(HomeScreen.this, EditProfile.class).putExtra("isCustomer", true));
        else
            startActivity(new Intent(HomeScreen.this, EditProfile.class).putExtra("isCustomer", false));
    }

    @OnClick(R.id.view_listing_container)
    void setViewLocalListing() {
//        startActivity(new Intent(HomeScreen.this, AssetListing.class).putExtra("isCustomer", isCustomer)
//                .putExtra("isRateReview", false)
//                .putExtra("isCancelAppointment", false)
//                .putExtra("isStartAppointment", false)
//                .putExtra("isAssetListing", true));
        startActivity(new Intent(context, CategoryListing.class).putExtra("isCustomer", isCustomer)
                .putExtra("isRateReview", false).putExtra("isCancelAppointment", false)
                .putExtra("isStartAppointment", false)
                .putExtra("isAssetListing", true));
    }

    @OnClick(R.id.chat_container)
    void setChatContainer() {
        if (Prefs.getPrefInstance().getValue(HomeScreen.this, Const.LOGIN_ACCESS, "").equals(getString(R.string.logged_in))) {
            startActivity(new Intent(HomeScreen.this, ChatListing.class).putExtra("isCustomer", isCustomer));
        } else {
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(HomeScreen.this.getResources().getString(R.string.please_customer_login_continue));
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(HomeScreen.this.getResources().getString(R.string.sign_in));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(HomeScreen.this.getResources().getString(R.string.cancel));

            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                dialog.dismiss();
                startActivity(new Intent(HomeScreen.this, RegisterOptions.class).putExtra("isLogin", true));
            });

            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    @OnClick(R.id.change_password_container)
    void setChangePassword() {
        startActivity(new Intent(HomeScreen.this, ChangePassword.class).putExtra("isCustomer", isCustomer));
    }

    @OnClick(R.id.about_us_container)
    void setAboutUs() {
        startActivity(new Intent(HomeScreen.this, WebView.class).putExtra("type", "about"));
    }

    @OnClick(R.id.contact_us_container)
    void setContactUs() {
        startActivity(new Intent(HomeScreen.this, WebView.class).putExtra("type", "contact_us"));
    }

    @OnClick(R.id.privacy_policy_container)
    void setPrivacyPolicy() {
        startActivity(new Intent(HomeScreen.this, WebView.class).putExtra("type", "privacy"));
    }

    @OnClick(R.id.button_post_service)
    void setButtonPostService() {

        View dialog_view = LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.simple_dialog_text_button), null);
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setCancelable(false)
                .setView(dialog_view)
                .show();

        if (dialog.getWindow() != null)
            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.you_need_to_login_with_essential_provider));
//        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("Cancel");
        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
            dialog.dismiss();
        });

        ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText("Sign in");
        dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
            startActivity(new Intent(HomeScreen.this, Login.class).putExtra("isCustomer", isCustomer).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
        });

//        startActivity(new Intent(HomeScreen.this, SignUp.class).putExtra("isCustomer", false));
    }

    @OnClick(R.id.logout)
    void setLogout() {
        if (Prefs.getPrefInstance().getValue(HomeScreen.this, Const.LOGIN_ACCESS, "").equals(getString(R.string.logged_in))) {
            lock_unlock_Drawer();
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(HomeScreen.this.getResources().getString(R.string.sign_out_text));
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(HomeScreen.this.getResources().getString(R.string.yes));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(HomeScreen.this.getResources().getString(R.string.cancel));

            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                dialog.dismiss();
                callLogout();
            });

            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        } else {
            startActivity(new Intent(HomeScreen.this, RegisterOptions.class).putExtra("isLogin", true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        }
    }

    @OnClick(R.id.social_media_container)
    void setSocialMediaContainer() {
        startActivity(new Intent(HomeScreen.this, SocialMedia.class));
    }

    @OnClick(R.id.history_container)
    void setHistoryContainer() {
        startActivity(new Intent(HomeScreen.this, AppointmentHistory.class).putExtra("isCustomer", isCustomer).putExtra("path", ""));
    }

    @OnClick(R.id.share_our_app_container)
    void setShareOurApp() {
        show_description();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareDetails);
        sendIntent.setType("text/plain");
        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }

    public void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    private void init() {

        return_policy_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeScreen.this, WebView.class).putExtra("type", "Return Policy"));
            }
        });

        if (Prefs.getPrefInstance().getValue(this, Const.LOGIN_ACCESS, "").equals(getResources().getString(R.string.logged_in))) {
            logoutText.setText(getResources().getString(R.string.logout));
            if (isCustomer) getProfileCustomer();
            else getProfileBusiness();
        } else {
            profileNameContainer.setVisibility(View.INVISIBLE);
            logoutText.setText(getResources().getString(R.string.login));
        }
        drawerToggle = new ActionBarDrawerToggle(this, drawerContainer, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
//                setWindowFlag(HomeScreen.this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
//                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
//                drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
//                setWindowFlag(HomeScreen.this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
//                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
//                drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
//                    if (slideOffset <= 1.0 && slideOffset > 0.999)
//                        if (Prefs.getPrefInstance().getValue(HomeScreen.this, Const.LOGIN_ACCESS, "").equals(getResources().getString(R.string.logged_in)))
//                        get_Profile();
//                frameLayout.setTranslationX(slideOffset * drawerView.getWidth());
//                drawerContainer.bringChildToFront(drawerView);
//                drawerContainer.requestLayout();
            }
        };
        drawerContainer.addDrawerListener(drawerToggle);
//        drawerContainer.setScrimColor(getResources().getColor(R.color.colorPrimaryDark));
        drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerToggle.syncState();

        if (Prefs.getPrefInstance().getValue(HomeScreen.this, Const.LOGIN_ACCESS, "").equals(getString(R.string.logged_in))){
            changePasswordContainer.setVisibility(View.VISIBLE);
            historyContainer.setVisibility(View.VISIBLE);
            chatContainer.setVisibility(View.VISIBLE);
        }
        else {
            changePasswordContainer.setVisibility(View.GONE);
            historyContainer.setVisibility(View.GONE);
            chatContainer.setVisibility(View.GONE);
        }

        if (isCustomer) postYourServiceContainer.setVisibility(View.VISIBLE);
        else postYourServiceContainer.setVisibility(View.GONE);

        if (isCustomer) {
            String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
            if (!AppUtil.hasPermissions(HomeScreen.this, PERMISSIONS)) {
                setPermission();
            } else {
                getLocation();
            }
        }
        String access = getIntent().getStringExtra("access");
        String path = getIntent().getStringExtra("path");
        if (access != null && !access.isEmpty()) {
            if (access.equals("chat")) {
                startActivity(new Intent(HomeScreen.this, Chat.class).putExtra("isCustomer", isCustomer).putExtra("path", path));
            } else  if (access.equals("submit_appointment")){
                startActivity(new Intent(HomeScreen.this, AppointmentHistory.class).putExtra("isCustomer", isCustomer).putExtra("path", path));
            }else  if (access.equals("cancel_appointment")){
                startActivity(new Intent(HomeScreen.this, AppointmentHistory.class).putExtra("isCustomer", isCustomer).putExtra("path", path));
            }else  if (access.equals("completed_appointment")){
                startActivity(new Intent(HomeScreen.this, AppointmentHistory.class).putExtra("isCustomer", isCustomer).putExtra("path", path));
            }else  if (access.equals("rate_review_appointment")){
                startActivity(new Intent(HomeScreen.this, AppointmentHistory.class).putExtra("isCustomer", isCustomer).putExtra("path", path));
            }
        }
    }

    private void setPermission() {
        String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

        if (!AppUtil.hasPermissions(HomeScreen.this, PERMISSIONS)) {
            View dialog_view = LayoutInflater.from(this).inflate(AppUtil.setLanguage(this, R.layout.simple_dialog_text_button), null);
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.location_permission_request));
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog.dismiss());
            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(v -> {
                dialog.dismiss();
                askPermission(HomeScreen.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        .onAccepted(result -> getLocation())
                        .onDenied(result -> {
                            View dialog_view1 = LayoutInflater.from(this).inflate(AppUtil.setLanguage(this, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog1 = new AlertDialog.Builder(this)
                                    .setCancelable(false)
                                    .setView(dialog_view1)
                                    .show();

                            if (dialog1.getWindow() != null)
                                dialog1.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view1.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.permission_Deny));
                            ((Button) dialog_view1.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                            ((Button) dialog_view1.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.set_permission));

                            dialog_view1.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog1.dismiss());
                            dialog_view1.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog1.dismiss();
                                result.askAgain();
                            });
                        })
                        .onForeverDenied(result -> {
                            View dialog_view2 = LayoutInflater.from(this).inflate(AppUtil.setLanguage(this, R.layout.simple_dialog_text_button), null);
                            AlertDialog dialog2 = new AlertDialog.Builder(this)
                                    .setCancelable(false)
                                    .setView(dialog_view2)
                                    .show();

                            if (dialog2.getWindow() != null)
                                dialog2.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view2.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.location_permission_Forever_Deny));
                            ((Button) dialog_view2.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.cancel));
                            ((Button) dialog_view2.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.go_to_settings));

                            dialog_view2.findViewById(R.id.dialog_cancel).setOnClickListener(v1 -> dialog2.dismiss());
                            dialog_view2.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                dialog2.dismiss();
                                result.goToSettings();
                            });
                        })
                        .ask();
            });
        } else getLocation();
    }

    private void getLocation() {
        if (!isGooglePlayServicesAvailable()) {
            View dialog_view2 = LayoutInflater.from(this).inflate(AppUtil.setLanguage(this, R.layout.simple_dialog_text_button), null);
            AlertDialog dialog2 = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setView(dialog_view2)
                    .show();

            if (dialog2.getWindow() != null)
                dialog2.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view2.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
            ((Button) dialog_view2.findViewById(R.id.dialog_cancel)).setVisibility(View.GONE);
            ((Button) dialog_view2.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.ok));

            dialog_view2.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                dialog2.dismiss();
            });
        }
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, locationListener);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();

        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GoogleApiAvailability.getInstance().getErrorDialog(this, status, 0).show();
            return false;
        }
    }

    private void setHamburgerMenu() {
        drawer.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {

        if (drawerContainer.isDrawerOpen(GravityCompat.START)) {
            drawerContainer.closeDrawer(GravityCompat.START);
            drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            return;
        }

//        int count = getSupportFragmentManager().getBackStackEntryCount();
//        String name = getSupportFragmentManager().getBackStackEntryAt(count - 1).getName();
        View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
        final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                .setCancelable(false)
                .setView(dialog_view)
                .show();

        if (dialog.getWindow() != null)
            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.exit_message));
        ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.yes));
        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.no));

        dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
            dialog.dismiss();
            finish();
        });

        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
            dialog.dismiss();
        });
    }

    @Override
    public void go_back() {
        onBackPressed();
    }

    @Override
    public void lock_unlock_Drawer() {
        if (drawerContainer.isDrawerOpen(GravityCompat.START)) {
            new Handler().postDelayed(() -> {
                drawerContainer.closeDrawer(GravityCompat.START);
                drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }, 500);
        } else {
            drawerContainer.openDrawer(GravityCompat.START);
            drawerContainer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NotNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) Objects.requireNonNull(this.getSystemService(Context.INPUT_METHOD_SERVICE))).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    private void callLogout() {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            APIUtils.getAPIService().call_logout(Prefs.getPrefInstance().getValue(HomeScreen.this, Const.USER_ID, "")).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_NAME);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_FIRST_NAME);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_LAST_NAME);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.PHONE_NUMBER);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.SET_PROFILE_IMAGE);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.LOGIN_ACCESS);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_ID);
                            startActivity(new Intent(HomeScreen.this, Login.class).putExtra("isCustomer", isCustomer).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        } else {
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_FIRST_NAME);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_LAST_NAME);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_NAME);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.PHONE_NUMBER);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.SET_PROFILE_IMAGE);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.LOGIN_ACCESS);
                            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_ID);
                            startActivity(new Intent(HomeScreen.this, Login.class).putExtra("isCustomer", isCustomer).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        }
                    } else {
                        Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_NAME);
                        Prefs.getPrefInstance().remove(HomeScreen.this, Const.PHONE_NUMBER);
                        Prefs.getPrefInstance().remove(HomeScreen.this, Const.SET_PROFILE_IMAGE);
                        Prefs.getPrefInstance().remove(HomeScreen.this, Const.LOGIN_ACCESS);
                        Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_ID);
                        startActivity(new Intent(HomeScreen.this, Login.class).putExtra("isCustomer", isCustomer).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {

                    Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_NAME);
                    Prefs.getPrefInstance().remove(HomeScreen.this, Const.PHONE_NUMBER);
                    Prefs.getPrefInstance().remove(HomeScreen.this, Const.SET_PROFILE_IMAGE);
                    Prefs.getPrefInstance().remove(HomeScreen.this, Const.LOGIN_ACCESS);
                    Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_ID);
                    startActivity(new Intent(HomeScreen.this, Login.class).putExtra("isCustomer", isCustomer).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                }
            });

        } else {

            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_NAME);
            Prefs.getPrefInstance().remove(HomeScreen.this, Const.PHONE_NUMBER);
            Prefs.getPrefInstance().remove(HomeScreen.this, Const.SET_PROFILE_IMAGE);
            Prefs.getPrefInstance().remove(HomeScreen.this, Const.LOGIN_ACCESS);
            Prefs.getPrefInstance().remove(HomeScreen.this, Const.USER_ID);
            startActivity(new Intent(HomeScreen.this, Login.class).putExtra("isCustomer", isCustomer).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        }
    }

    private void getProfileCustomer() {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            APIUtils.getAPIService().get_profile(getResources().getString(R.string.customers), getResources().getString(R.string.type_details), Prefs.getPrefInstance().getValue(HomeScreen.this, Const.USER_ID, "")).enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<ProfileResponse> call, @NonNull Response<ProfileResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.USER_NAME, response.body().getData().get(0).getUsername());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.EMAIL, response.body().getData().get(0).getEmail());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.SET_PROFILE_IMAGE, response.body().getData().get(0).getImage());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.USER_ID, response.body().getData().get(0).getUserId());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.PHONE_NUMBER, response.body().getData().get(0).getPhone());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.USER_FIRST_NAME, response.body().getData().get(0).getFirstname());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.USER_LAST_NAME, response.body().getData().get(0).getLastname());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.COUNTRY, response.body().getData().get(0).getCountry());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.STATE, response.body().getData().get(0).getState());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.CITY, response.body().getData().get(0).getCity());

                                Glide
                                        .with(Essentials.applicationContext)
                                        .load(Prefs.getPrefInstance().getValue(HomeScreen.this, Const.SET_PROFILE_IMAGE, ""))
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.logo_with_name)
                                        .signature(new ObjectKey(Prefs.getPrefInstance().getValue(HomeScreen.this, Const.TIME, "")))
                                        .thumbnail(0.25f)
                                        .error(R.drawable.ic_profile_icon)
                                        .into(profileImage);
                                String name = response.body().getData().get(0).getFirstname() + " " + response.body().getData().get(0).getLastname();
                                profileTitle.setText(name);

                                String profileAddress = response.body().getData().get(0).getCity() + " " + response.body().getData().get(0).getState();
                                address.setText(profileAddress);

//                                String date = "";
//                                try {
//                                    Date englishDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(Prefs.getPrefInstance().getValue(HomeScreen.this, Const.USER_DOB, ""));
//                                    if (englishDate != null) {
//                                        date = new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(englishDate.getTime());
//                                    }
//                                } catch (ParseException e) {
//                                    date = "";
//                                    e.printStackTrace();
//                                }

                            } else {

                            }
                        } else {

                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(@NonNull Call<ProfileResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();

                }
            });
        } else {
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(HomeScreen.this.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(HomeScreen.this.getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

    private void getProfileBusiness() {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            APIUtils.getAPIService().get_profile(getResources().getString(R.string.providers), getResources().getString(R.string.type_details), Prefs.getPrefInstance().getValue(HomeScreen.this, Const.USER_ID, "")).enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<ProfileResponse> call, @NonNull Response<ProfileResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData() != null && !response.body().getData().isEmpty()) {
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.USER_NAME, response.body().getData().get(0).getUsername());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.EMAIL, response.body().getData().get(0).getEmail());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.SET_PROFILE_IMAGE, response.body().getData().get(0).getImage());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.USER_ID, response.body().getData().get(0).getUserId());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.PHONE_NUMBER, response.body().getData().get(0).getPhone());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.USER_FIRST_NAME, response.body().getData().get(0).getFirstname());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.USER_LAST_NAME, response.body().getData().get(0).getLastname());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.COUNTRY, response.body().getData().get(0).getCountry());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.ADDRESS, response.body().getData().get(0).getAddress());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.STATE, response.body().getData().get(0).getState());
                                Prefs.getPrefInstance().setValue(HomeScreen.this, Const.CITY, response.body().getData().get(0).getCity());
                                Glide
                                        .with(Essentials.applicationContext)
                                        .load(Prefs.getPrefInstance().getValue(HomeScreen.this, Const.SET_PROFILE_IMAGE, ""))
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.logo_with_name)
                                        .signature(new ObjectKey(Prefs.getPrefInstance().getValue(HomeScreen.this, Const.TIME, "")))
                                        .thumbnail(0.25f)
                                        .error(R.drawable.ic_profile_icon)
                                        .into(profileImage);
                                String name = response.body().getData().get(0).getFirstname() + " " + response.body().getData().get(0).getLastname();
                                profileTitle.setText(name);

                                String profileAddress = response.body().getData().get(0).getCity() + " " + response.body().getData().get(0).getState();
                                address.setText(profileAddress);

                                String date = "";
                                try {
                                    Date englishDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(Prefs.getPrefInstance().getValue(HomeScreen.this, Const.USER_DOB, ""));
                                    if (englishDate != null) {
                                        date = new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(englishDate.getTime());
                                    }
                                } catch (ParseException e) {
                                    date = "";
                                    e.printStackTrace();
                                }

                            } else {

                            }
                        } else {

                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(@NonNull Call<ProfileResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();

                }
            });
        } else {
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(HomeScreen.this.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(HomeScreen.this.getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        locationListener = location -> {
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            Prefs.getPrefInstance().setValue(HomeScreen.this, Const.LONGITUDE, longitude);
            Prefs.getPrefInstance().setValue(HomeScreen.this, Const.LATITUDE, latitude);
            mCurrentLocation = location;
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void show_description() {
        if (AppUtil.isInternetAvailable(HomeScreen.this)) {
            APIUtils.getAPIService().call_web_view().enqueue(new Callback<WebViewResponse>() {
                @Override
                public void onResponse(@NonNull Call<WebViewResponse> call, @NonNull Response<WebViewResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getShareourappData() != null && !response.body().getShareourappData().isEmpty()) {
                                shareDetails = response.body().getShareourappData().get(0).getAndroidLink();
                            }
                        } else {
                            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<WebViewResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(HomeScreen.this).inflate(AppUtil.setLanguage(HomeScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(HomeScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }
}
