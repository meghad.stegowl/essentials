package com.essentials.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.core.widget.NestedScrollView;

import com.essentials.R;
import com.essentials.data.models.webview.WebViewResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppUtil;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermCondition extends AppCompatActivity {

    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.no_data_found)
    TextView no_data_found;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.back)
    LinearLayout back;

    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    
    
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_condition);
        ButterKnife.bind(this);
        loader.setVisibility(View.GONE);
        init();
    }

    private void init() {
        show_description();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TermCondition.this, Login.class);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }


    private void show_description() {
        if (AppUtil.isInternetAvailable(TermCondition.this)) {
            loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().call_web_view().enqueue(new Callback<WebViewResponse>() {
                @Override
                public void onResponse(@NonNull Call<WebViewResponse> call, @NonNull Response<WebViewResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                                if (response.body().getTermsandconditionsData() != null && !response.body().getTermsandconditionsData().isEmpty()) {
                                    title.setText(getResources().getString(R.string.terms_and_conditions));
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        description.setText(Html.fromHtml(response.body().getTermsandconditionsData().get(0).getDescription(), Html.FROM_HTML_MODE_LEGACY));
                                    } else {
                                        description.setText(Html.fromHtml(response.body().getTermsandconditionsData().get(0).getDescription()));
                                    }
                                    scrollView.setVisibility(View.VISIBLE);
                                    loader.setVisibility(View.GONE);
                                    no_data_found.setVisibility(View.GONE);
                                } else {
                                    scrollView.setVisibility(View.GONE);
                                    no_data_found.setVisibility(View.VISIBLE);
                                    loader.setVisibility(View.GONE);
                                }
                        } else {
                            scrollView.setVisibility(View.GONE);
                            no_data_found.setVisibility(View.VISIBLE);
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(TermCondition.this).inflate(AppUtil.setLanguage(TermCondition.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(TermCondition.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });

                        }
                    } else {
                        scrollView.setVisibility(View.GONE);
                        no_data_found.setVisibility(View.VISIBLE);
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(TermCondition.this).inflate(AppUtil.setLanguage(TermCondition.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(TermCondition.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<WebViewResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    scrollView.setVisibility(View.GONE);
                    no_data_found.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(TermCondition.this).inflate(AppUtil.setLanguage(TermCondition.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(TermCondition.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText("Something went wrong! Please try again.");
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(TermCondition.this).inflate(AppUtil.setLanguage(TermCondition.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(TermCondition.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText("OK");
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }
}