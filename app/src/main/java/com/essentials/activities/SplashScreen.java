package com.essentials.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.essentials.R;
import com.essentials.application.Essentials;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Objects;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

import static com.essentials.util.AppUtil.setWindowFlag;

public class SplashScreen extends AppCompatActivity {

    Handler handler;
    Runnable runnable;
    String access = "", path = "";
    private Tracker mTracker;
    private Boolean isCustomer;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                if (getIntent().hasExtra("notiType")) {
                    String notiType = getIntent().getExtras().getString("notiType");
                    if (Objects.equals(notiType, "3")) {
                        access = "chat";
                        path = getIntent().getExtras().getString("sender_id");
                    } else if (Objects.equals(notiType, "2")) {
                        access = "submit_appointment";
                        path = "pending";
                    } else if (Objects.equals(notiType, "4")) {
                        access = "cancel_appointment";
                        path = "cancelled";
                    } else if (Objects.equals(notiType, "5")) {
                        access = "completed_appointment";
                        path = "completed";
                    } else if (Objects.equals(notiType, "6")) {
                        access = "rate_review_appointment";
                        path = "rateReview";
                    }
                }
            }
        }
        // Make Full Screen - Hide StatusBar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Make UI Full Screen
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        // Set StatusBar Color Transparent
        setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        getWindow().setStatusBarColor(Color.TRANSPARENT);

        mTracker = Essentials.getDefaultTracker();
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        isCustomer = Prefs.getPrefInstance().getValue(SplashScreen.this, Const.CUSTOMER_LOGIN, "").equals("1");

        handler = new Handler();
        runnable = () -> {
            if (Prefs.getPrefInstance().getValue(SplashScreen.this, Const.APP_STATUS, "").equals("")) {
                startActivity(new Intent(SplashScreen.this, IntroSlider.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK));
            } else {
                if (Prefs.getPrefInstance().getValue(this, Const.LOGIN_ACCESS, "").equals(getString(R.string.logged_in)) ||
                        Prefs.getPrefInstance().getValue(this, Const.LOGIN_ACCESS, "").equals(getString(R.string.skipped)))
                    startActivity(new Intent(SplashScreen.this, HomeScreen.class).putExtra("isCustomer",
                            isCustomer).putExtra("access", access).putExtra("path", path));
                else
                    startActivity(new Intent(SplashScreen.this, RegisterOptions.class).putExtra("isLogin",
                            true));
            }
            finish();
        };
        if (AppUtil.isInternetAvailable(SplashScreen.this)) {
            handler.postDelayed(runnable, 3000);
        } else {
            View dialog_view = LayoutInflater.from(SplashScreen.this).inflate(AppUtil.setLanguage(SplashScreen.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(SplashScreen.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            dialog_view.findViewById(R.id.dialog_cancel).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> dialog.dismiss());
        }

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Splash");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, bundle);

        FirebaseApp.initializeApp(SplashScreen.this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                return;
            }

            // Get new Instance ID token
            String token = Objects.requireNonNull(task.getResult()).getToken();
            Log.d("mytag", "fcm_id - " + token);
            Prefs.getPrefInstance().setValue(this, Const.FCM_ID, token);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (handler != null && runnable != null) {
            handler.postDelayed(runnable, 3000);
            mTracker.setScreenName("Screen - " + "Splash");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }

    @Override
    protected void onPause() {
        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler.removeCallbacksAndMessages(null);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
