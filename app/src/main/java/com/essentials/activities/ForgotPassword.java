package com.essentials.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.ContentLoadingProgressBar;

import com.essentials.R;
import com.essentials.application.Essentials;
import com.essentials.data.models.common.CommonResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppUtil;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {


    @BindView(R.id.email_input)
    TextInputEditText emailInput;
    @BindView(R.id.email_container)
    TextInputLayout emailContainer;
    @BindView(R.id.button_submit)
    MaterialButton buttonSubmit;
    @BindView(R.id.layout_container)
    LinearLayout layoutContainer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.main_container)
    CoordinatorLayout mainContainer;
    private Tracker mTracker;
    private Boolean isCustomer;

    @OnClick(R.id.back)
    public void setBack() {
        onBackPressed();
    }

    @OnClick(R.id.button_submit)
    public void onSubmitClicked() {
        if (emailInput.getText() == null) {
            emailContainer.setError(getResources().getString(R.string.email_empty));
        } else if (emailInput.getText().toString().trim().length() == 0) {
            emailContainer.setError(getResources().getString(R.string.email_empty));
        } else if (!TextUtils.isDigitsOnly(emailInput.getText()) && !Patterns.EMAIL_ADDRESS.matcher(emailInput.getText()).matches()) {
            emailContainer.setError(getResources().getString(R.string.email_not_valid));
        } else {
            userForgotPassword(emailInput.getText().toString());
            emailInput.clearFocus();
        }
    }

    @OnFocusChange(R.id.email_input)
    public void onEmailInputFocusChanged(boolean isFocused) {
        if (isFocused) {
            emailContainer.setError("");
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mTracker = Essentials.getDefaultTracker();
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Forgot Password");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void init() {
        if (getIntent() !=null)
            isCustomer = getIntent().getBooleanExtra("isCustomer", false);

        loader.setVisibility(View.GONE);

        emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private void userForgotPassword(String email) {
        if (AppUtil.isInternetAvailable(ForgotPassword.this)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("email", email);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody forgot_password = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().forgot_password(forgot_password).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals(200)) {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(ForgotPassword.this).inflate(AppUtil.setLanguage(ForgotPassword.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(ForgotPassword.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                startActivity(new Intent(ForgotPassword.this, Login.class).putExtra("isCustomer", isCustomer).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            });

                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(ForgotPassword.this).inflate(AppUtil.setLanguage(ForgotPassword.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(ForgotPassword.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });

                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(ForgotPassword.this).inflate(AppUtil.setLanguage(ForgotPassword.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(ForgotPassword.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(ForgotPassword.this).inflate(AppUtil.setLanguage(ForgotPassword.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(ForgotPassword.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(ForgotPassword.this).inflate(AppUtil.setLanguage(ForgotPassword.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(ForgotPassword.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

}
