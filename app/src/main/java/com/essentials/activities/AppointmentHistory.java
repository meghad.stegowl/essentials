package com.essentials.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.essentials.R;
import com.essentials.fragments.AppointmentHistoryFragment;
import com.essentials.fragments.AssetDetailFragment;
import com.essentials.notifications.NotificationManager;
import com.essentials.util.Const;
import com.essentials.util.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

import static com.essentials.notifications.NotificationManager.didAppointmentAccepted;
import static com.essentials.notifications.NotificationManager.didAppointmentCancelled;
import static com.essentials.notifications.NotificationManager.didAppointmentCompleted;
import static com.essentials.notifications.NotificationManager.didAppointmentEnded;
import static com.essentials.notifications.NotificationManager.didAppointmentStarted;
import static com.essentials.notifications.NotificationManager.didAppointmentStatusChanged;
import static com.essentials.notifications.NotificationManager.didAppointmentSubmitted;

public class AppointmentHistory extends AppCompatActivity implements NotificationManager.NotificationCenterDelegate {

    @BindView(R.id.frame_container)
    FrameLayout frameContainer;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            Fragment fragment = AppointmentHistoryFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString("path", getIntent().getStringExtra("path"));
            bundle.putString("pending", getIntent().getStringExtra("pending"));
            bundle.putBoolean("isCustomer", Prefs.getPrefInstance().getValue(getApplicationContext(), Const.CUSTOMER_LOGIN, "").equals("1"));
            fragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commitNow();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        addObserver();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    public void addObserver() {
        NotificationManager.getInstance().addObserver(this, didAppointmentStatusChanged);
        NotificationManager.getInstance().addObserver(this, NotificationManager.didAppointmentSubmitted);
        NotificationManager.getInstance().addObserver(this, NotificationManager.didAppointmentStarted);
        NotificationManager.getInstance().addObserver(this, NotificationManager.didAppointmentCancelled);
        NotificationManager.getInstance().addObserver(this, NotificationManager.didAppointmentEnded);
        NotificationManager.getInstance().addObserver(this, NotificationManager.didAppointmentCompleted);
        NotificationManager.getInstance().addObserver(this, NotificationManager.didAppointmentReviewed);
        NotificationManager.getInstance().addObserver(this, NotificationManager.didAppointmentAccepted);
    }

    public void removeObserver() {
        NotificationManager.getInstance().removeObserver(this, didAppointmentStatusChanged);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.didAppointmentSubmitted);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.didAppointmentStarted);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.didAppointmentCancelled);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.didAppointmentEnded);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.didAppointmentCompleted);
        NotificationManager.getInstance().removeObserver(this, NotificationManager.didAppointmentAccepted);
    }

    @Override
    protected void onDestroy() {
        removeObserver();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeObserver();
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {
        String path = "", notiType = "";
        boolean isCustomer = false;
        if (id == didAppointmentStatusChanged) {
            notiType = (String) args[0];
            path = (String) args[1];
            isCustomer = (boolean) args[2];
            AppointmentHistoryFragment.getInstance().setNotificationData(id, notiType, path, isCustomer);
        } else if (id == didAppointmentSubmitted) {
            notiType = (String) args[0];
            path = (String) args[1];
            isCustomer = (boolean) args[2];
            AppointmentHistoryFragment.getInstance().setNotificationData(id, notiType, path, isCustomer);
        } else if (id == didAppointmentStarted) {
            notiType = (String) args[0];
            path = (String) args[1];
            isCustomer = (boolean) args[2];
            AppointmentHistoryFragment.getInstance().setNotificationData(id, notiType, path, isCustomer);
        } else if (id == didAppointmentAccepted) {
            notiType = (String) args[0];
            path = (String) args[1];
            isCustomer = (boolean) args[2];
            AppointmentHistoryFragment.getInstance().setNotificationData(id, notiType, path, isCustomer);
        }else if (id == didAppointmentCancelled) {
            notiType = (String) args[0];
            path = (String) args[1];
            isCustomer = (boolean) args[2];
            AppointmentHistoryFragment.getInstance().setNotificationData(id, notiType, path, isCustomer);
        } else if (id == didAppointmentEnded) {
            notiType = (String) args[0];
            path = (String) args[1];
            isCustomer = (boolean) args[2];
            AppointmentHistoryFragment.getInstance().setNotificationData(id, notiType, path, isCustomer);
        } else if (id == didAppointmentCompleted) {
            notiType = (String) args[0];
            path = (String) args[1];
            isCustomer = (boolean) args[2];
            AppointmentHistoryFragment.getInstance().setNotificationData(id, notiType, path, isCustomer);
        } else { // didAppointmentReviewed
            notiType = (String) args[0];
            path = (String) args[1];
            isCustomer = (boolean) args[2];
            AppointmentHistoryFragment.getInstance().setNotificationData(id, notiType, path, isCustomer);
        }
    }
}