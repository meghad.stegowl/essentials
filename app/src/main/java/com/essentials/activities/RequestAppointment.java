package com.essentials.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.essentials.R;
import com.essentials.fragments.AssetDetailFragment;
import com.essentials.fragments.RequestAppointmentFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class RequestAppointment extends AppCompatActivity {

    @BindView(R.id.frame_container)
    FrameLayout frameContainer;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            Fragment fragment = RequestAppointmentFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString("path", getIntent().getStringExtra("path"));
            bundle.putString("assetName", getIntent().getStringExtra("assetName"));
            bundle.putString("businessname", getIntent().getStringExtra("businessname"));
            bundle.putString("image", getIntent().getStringExtra("image"));
            bundle.putFloat("rating", getIntent().getFloatExtra("rating", 0));
            bundle.putString("categoryName", getIntent().getStringExtra("categoryName"));
            bundle.putString("ratingCount", getIntent().getStringExtra("ratingCount"));
            bundle.putString("healthCategoryId", getIntent().getStringExtra("healthCategoryId"));
            bundle.putBoolean("isCustomer", getIntent().getBooleanExtra("isCustomer", false));
            fragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commitNow();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }
}