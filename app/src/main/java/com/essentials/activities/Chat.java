package com.essentials.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.essentials.R;
import com.essentials.data.models.chat.MessageHistoryData;
import com.essentials.fragments.AssetDetailFragment;
import com.essentials.fragments.ChatFragment;
import com.essentials.fragments.ChatListingFragment;
import com.essentials.notifications.NotificationManager;
import com.essentials.util.Const;
import com.essentials.util.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class Chat extends AppCompatActivity implements NotificationManager.NotificationCenterDelegate {

    @BindView(R.id.frame_container)
    FrameLayout frameContainer;
    String path;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);

        path = "";
        boolean isCustomer = Prefs.getPrefInstance().getValue(Chat.this, Const.CUSTOMER_LOGIN, "").equals("1");
        if (getIntent() != null){
            if (getIntent().getExtras() != null){
                    path = (String) getIntent().getExtras().get("path");
                    getIntent().getExtras().remove("path");
                    Log.d("mytag", " path - " + path + " -- isc" + isCustomer);
                    Log.d("mytag", " path - " + path + " -- isc");
                }
        }
        
        if (savedInstanceState == null) {
            Fragment fragment = ChatFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putString("path", path);
            bundle.putString("title", getIntent().getStringExtra("title"));
            bundle.putBoolean("isCustomer", isCustomer);
            fragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commitNow();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        addObserver();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {
        MessageHistoryData data = (MessageHistoryData) args[0];
        if (data.getSendUserId().equals(path)) ChatFragment.getInstance().gotNewMessage(data);
    }

    @Override
    protected void onDestroy() {
        removeObserver();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeObserver();
    }

    public void addObserver() {
        NotificationManager.getInstance().addObserver(this, NotificationManager.didReceivedNewMessages);
    }

    public void removeObserver() {
        NotificationManager.getInstance().removeObserver(this, NotificationManager.didReceivedNewMessages);
    }
}