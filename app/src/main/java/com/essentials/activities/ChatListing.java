package com.essentials.activities;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.essentials.R;
import com.essentials.data.models.chat.MessageHistoryData;
import com.essentials.fragments.AssetDetailFragment;
import com.essentials.fragments.ChatListingFragment;
import com.essentials.notifications.NotificationManager;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class ChatListing extends AppCompatActivity implements NotificationManager.NotificationCenterDelegate {

    @BindView(R.id.frame_container)
    FrameLayout frameContainer;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            Fragment fragment = ChatListingFragment.newInstance();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isCustomer", getIntent().getBooleanExtra("isCustomer", false));
            fragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commitNow();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        addObserver();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void didReceivedNotification(int id, Object... args) {
        MessageHistoryData data = (MessageHistoryData) args[0];
        ChatListingFragment.getInstance().gotNewMessage(data);
    }

    @Override
    protected void onDestroy() {
        removeObserver();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeObserver();
    }

    public void addObserver() {
        NotificationManager.getInstance().addObserver(this, NotificationManager.didReceivedNewMessages);
    }

    public void removeObserver() {
        NotificationManager.getInstance().removeObserver(this, NotificationManager.didReceivedNewMessages);
    }

}