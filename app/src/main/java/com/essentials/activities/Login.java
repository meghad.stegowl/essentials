package com.essentials.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.ContentLoadingProgressBar;

import com.essentials.R;
import com.essentials.data.models.payment.OrderResponse;
import com.essentials.data.models.payment.PaymentResponse;
import com.essentials.data.models.user.LoginUserResponse;
import com.essentials.data.models.webview.WebViewResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.stripe.android.Stripe;
import com.stripe.android.exception.APIConnectionException;
import com.stripe.android.exception.APIException;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.exception.CardException;
import com.stripe.android.exception.InvalidRequestException;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputListener;
import com.stripe.android.view.CardMultilineWidget;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {


    private static Stripe stripe;
    private static String amount;
    Boolean isCustomer;
    @BindView(R.id.email_input)
    TextInputEditText emailInput;
    @BindView(R.id.email_container)
    TextInputLayout emailContainer;
    @BindView(R.id.password_input)
    TextInputEditText passwordInput;
    @BindView(R.id.password_container)
    TextInputLayout passwordContainer;
    @BindView(R.id.remember_me)
    MaterialCheckBox rememberMe;
    @BindView(R.id.button_login)
    MaterialButton buttonLogin;
    @BindView(R.id.forgot_password)
    TextView forgotPassword;
    @BindView(R.id.button_register)
    MaterialButton buttonRegister;
    @BindView(R.id.button_guest_login)
    MaterialButton buttonGuestLogin;
    @BindView(R.id.privacy_policy)
    TextView privacyPolicy;
    @BindView(R.id.layout_container)
    LinearLayout layoutContainer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.main_container)
    CoordinatorLayout mainContainer;
    private String image = "", latitude, longitude;
    @BindView(R.id.term_cond)
    TextView term_cond;

    @OnClick(R.id.button_login)
    public void onSignInClicked() {
        if (emailInput.getText() == null) {
            emailContainer.setError(getResources().getString(R.string.email_empty));
        } else if (emailInput.getText().toString().trim().length() == 0) {
            emailContainer.setError(getResources().getString(R.string.email_empty));
        } else if (passwordInput.getText() == null) {
            passwordContainer.setError(getResources().getString(R.string.password_empty));
        } else if (passwordInput.getText().toString().trim().length() == 0) {
            passwordContainer.setError(getResources().getString(R.string.password_empty));
        } else {
//            Prefs.getPrefInstance().setValue(Login.this, Const.LOGIN_ACCESS, getString(R.string.logged_in));
//            startActivity(new Intent(Login.this, RegisterOptions.class).putExtra("isLogin", true));
            Prefs.getPrefInstance().setValue(Login.this, Const.LOGIN_ACCESS, getString(R.string.logged_in));
            if (isCustomer)
                callLogIn(getResources().getString(R.string.customers), emailInput.getText().toString(), passwordInput.getText().toString());
            else
                callLogIn(getResources().getString(R.string.providers), emailInput.getText().toString(), passwordInput.getText().toString());
        }
    }

    @OnClick(R.id.button_register)
    public void setButtonRegister() {
        startActivity(new Intent(Login.this, RegisterOptions.class).putExtra("isLogin", false));
    }

    @OnClick(R.id.button_guest_login)
    public void setButtonGuestLogin() {
        Prefs.getPrefInstance().setValue(Login.this, Const.LOGIN_ACCESS, getString(R.string.skipped));
        startActivity(new Intent(Login.this, HomeScreen.class).putExtra("isLogin", false).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }

    @OnClick(R.id.back)
    void setBack() {
        startActivity(new Intent(Login.this, RegisterOptions.class).putExtra("isLogin", true));
    }

    @OnClick(R.id.forgot_password)
    void setForgotPassword() {
        startActivity(new Intent(Login.this, ForgotPassword.class).putExtra("isCustomer", isCustomer));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loader.setVisibility(View.GONE);
        isCustomer = getIntent().getBooleanExtra("isCustomer", false);
        if (isCustomer) {
            latitude = "-34";
            longitude = "151";
        } else {
            latitude = "33.990873";
            longitude = "150.991734";
        }
        init();
    }

    private void init() {
        emailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (Prefs.getPrefInstance().getValue(Login.this, Const.KEEP_USER_LOGGED_IN, "").equals("1")) {
            emailInput.setText(Prefs.getPrefInstance().getValue(Login.this, Const.EMAIL, ""));
            passwordInput.setText(Prefs.getPrefInstance().getValue(Login.this, Const.PASSWORD, ""));
            rememberMe.setChecked(true);
        }
        term_cond.setPaintFlags(term_cond.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        term_cond.setText(getString(R.string.terms_and_conditions));

        term_cond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this, TermCondition.class);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private void callLogIn(String type, String email, String password) {
        if (AppUtil.isInternetAvailable(Login.this)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("login_id", email);
                jsonObject.put("password", password);
                jsonObject.put("fcm_id", Prefs.getPrefInstance().getValue(Login.this, Const.FCM_ID, ""));
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody user_logging_in = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().user_login(type, user_logging_in).enqueue(new Callback<LoginUserResponse>() {
                @Override
                public void onResponse(@NonNull Call<LoginUserResponse> call, @NonNull Response<LoginUserResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if (response.body().getData().get(0).getPaymentStatus()) {
                                if (rememberMe.isChecked()) {
                                    Prefs.getPrefInstance().setValue(Login.this, Const.PASSWORD, password);
                                    Prefs.getPrefInstance().setValue(Login.this, Const.KEEP_USER_LOGGED_IN, "1");
                                } else {
                                    Prefs.getPrefInstance().setValue(Login.this, Const.PASSWORD, "");
                                    Prefs.getPrefInstance().setValue(Login.this, Const.KEEP_USER_LOGGED_IN, "0");
                                }
                                Prefs.getPrefInstance().setValue(Login.this, Const.EMAIL, email);
                                Prefs.getPrefInstance().setValue(Login.this, Const.USER_ID, response.body().getData().get(0).getUserId());
                                Prefs.getPrefInstance().setValue(Login.this, Const.USER_NAME, response.body().getData().get(0).getUsername());
                                Prefs.getPrefInstance().setValue(Login.this, Const.USER_FIRST_NAME, response.body().getData().get(0).getFirstname());
                                Prefs.getPrefInstance().setValue(Login.this, Const.USER_LAST_NAME, response.body().getData().get(0).getLastname());
                                Prefs.getPrefInstance().setValue(Login.this, Const.ADDRESS, response.body().getData().get(0).getAddress());
                                Prefs.getPrefInstance().setValue(Login.this, Const.COUNTRY, response.body().getData().get(0).getCountry());
                                Prefs.getPrefInstance().setValue(Login.this, Const.STATE, response.body().getData().get(0).getState());
                                Prefs.getPrefInstance().setValue(Login.this, Const.CITY, response.body().getData().get(0).getCity());
                                Prefs.getPrefInstance().setValue(Login.this, Const.HEALTH_CARE_CATEGORY_ID, response.body().getData().get(0).getHealthcarecategoryId());
                                Prefs.getPrefInstance().setValue(Login.this, Const.PHONE_NUMBER, response.body().getData().get(0).getPhone());
                                Prefs.getPrefInstance().setValue(Login.this, Const.ZIP_CODE, response.body().getData().get(0).getZipcode());
                                Prefs.getPrefInstance().setValue(Login.this, Const.LONGITUDE, longitude);
                                Prefs.getPrefInstance().setValue(Login.this, Const.LATITUDE, latitude);
                                Prefs.getPrefInstance().setValue(Login.this, Const.LATITUDE, latitude);
                                Prefs.getPrefInstance().setValue(Login.this, Const.KEY, response.body().getData().get(0).getRole());
                                if (isCustomer)
                                    Prefs.getPrefInstance().setValue(Login.this, Const.CUSTOMER_LOGIN, "1");
                                else
                                    Prefs.getPrefInstance().setValue(Login.this, Const.CUSTOMER_LOGIN, "0");
                                loader.setVisibility(View.GONE);
                                AppUtil.show_Snackbar(Login.this, mainContainer, response.body().getMessage(), true);
                                if (isCustomer)
                                    startActivity(new Intent(Login.this, HomeScreen.class).putExtra("isCustomer", true));
                                else
                                    startActivity(new Intent(Login.this, HomeScreen.class).putExtra("isCustomer", false));
                            } else {
                                loader.setVisibility(View.GONE);
                                View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                                final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                        .setCancelable(false)
                                        .setView(dialog_view)
                                        .show();

                                if (dialog.getWindow() != null)
                                    dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(String.format(getResources().getString(R.string.payment_text_login), Prefs.getPrefInstance().getValue(Login.this, Const.PAYMENT_AMOUNT, "")));
                                ((Button) dialog_view.findViewById(R.id.dialog_ok)).setText(getResources().getString(R.string.pay));
                                dialog_view.findViewById(R.id.dialog_ok).setOnClickListener(view -> {
                                    dialog.dismiss();
                                    getPaymentAmount(response.body().getData().get(0).getUserId());
                                });

                                dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                    dialog.dismiss();
                                });
                            }


                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LoginUserResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    private void setTerms_PrivacyText() {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder("Please read Privacy Along with");

        spanTxt.append(" ");
        spanTxt.append(getString(R.string.terms_and_conditions));

        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startActivity(new Intent(Login.this, TermCondition.class));
            }
        }, spanTxt.length() - getString(R.string.terms_and_conditions).length(), spanTxt.length(), 0);
        spanTxt.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), spanTxt.length() - getString(R.string.terms_and_conditions).length(), spanTxt.length(), 0);

//        privacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        privacyPolicy.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }

    private void getPaymentAmount(String user_id) {
        if (AppUtil.isInternetAvailable(Login.this)) {
            loader.setVisibility(View.VISIBLE);
            APIUtils.getAPIService().get_payment_amount().enqueue(new Callback<PaymentResponse>() {
                @Override
                public void onResponse(@NonNull Call<PaymentResponse> call, @NonNull Response<PaymentResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            amount = response.body().getAmount();
                            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.payment_dialog), null);
                            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            CardMultilineWidget cardInput = dialog_view.findViewById(R.id.cardInput);
                            cardInput.setCardInputListener(new CardInputListener() {
                                @Override
                                public void onFocusChange(@NotNull FocusField focusField) {

                                }

                                @Override
                                public void onCardComplete() {
                                }

                                @Override
                                public void onExpirationComplete() {

                                }

                                @Override
                                public void onCvcComplete() {

                                }

                            });

                            Button cancel = dialog_view.findViewById(R.id.cancel);
                            cancel.setOnClickListener(v12 -> dialog.dismiss());
                            loader.setVisibility(View.GONE);
                            Button pay = dialog_view.findViewById(R.id.payButton);
                            pay.setOnClickListener(v1 -> {
                                loader.setVisibility(View.VISIBLE);
                                Card card = cardInput.getCard();
                                if (card != null && card.validateCard()) {
//                                    stripe = new Stripe(Login.this, "pk_test_zYnCgv6nLRc5ZCZvdnqR4fRS");
                                    stripe = new Stripe(Login.this, Login.this.getResources().getString(R.string.stripe_key));
                                    try {
                                        String token = new getToken(card).execute().get();
                                        if (token != null) {
                                            dialog.dismiss();
                                            createOrder(token, user_id);
                                        } else {
                                            loader.setVisibility(View.GONE);
                                            View dialog_view1 = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                                            final AlertDialog dialog1 = new AlertDialog.Builder(Login.this)
                                                    .setCancelable(false)
                                                    .setView(dialog_view1)
                                                    .show();

                                            if (dialog1.getWindow() != null)
                                                dialog1.getWindow().getDecorView().getBackground().setAlpha(0);

                                            ((TextView) dialog_view1.findViewById(R.id.dialog_text)).setText(Login.this.getResources().getString(R.string.something_went_wrong));
                                            (dialog_view1.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                                            ((Button) dialog_view1.findViewById(R.id.dialog_cancel)).setText(Login.this.getResources().getString(R.string.ok));
                                            dialog_view1.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                                dialog1.dismiss();
                                                dialog.dismiss();
                                            });
                                        }
                                    } catch (ExecutionException | InterruptedException e) {
                                        e.printStackTrace();
                                        loader.setVisibility(View.GONE);
                                    }
                                } else {
                                    loader.setVisibility(View.GONE);
                                }
                            });


                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PaymentResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }

    private void createOrder(String token, String user_id) {
        if (AppUtil.isInternetAvailable(Login.this)) {
            loader.setVisibility(View.GONE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", user_id);
                jsonObject.put("amount", amount);
                jsonObject.put("token", token);
            } catch (JSONException e) {
                loader.setVisibility(View.GONE);
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody create_order = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().create_order(create_order).enqueue(new Callback<OrderResponse>() {
                @Override
                public void onResponse(@NonNull Call<OrderResponse> call, @NonNull Response<OrderResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                callLogIn(getResources().getString(R.string.providers), emailInput.getText().toString(), passwordInput.getText().toString());
                            });


                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(response.body().getMessage());
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                            });
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                            dialog.dismiss();
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<OrderResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                        dialog.dismiss();
                    });
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(Login.this).inflate(AppUtil.setLanguage(Login.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(Login.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }

    }

    static class getToken extends AsyncTask<Void, Void, String> {

        Card card;

        getToken(Card card) {
            this.card = card;
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                Token token = stripe.createCardTokenSynchronous(card);
                return token != null ? token.getId() : null;
            } catch (AuthenticationException | CardException | APIException | APIConnectionException | InvalidRequestException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}