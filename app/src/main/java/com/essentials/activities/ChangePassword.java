package com.essentials.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.ContentLoadingProgressBar;

import com.essentials.R;
import com.essentials.application.Essentials;
import com.essentials.data.models.common.CommonResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity {

    Handler handler;
    @BindView(R.id.current_password_input)
    TextInputEditText currentPasswordInput;
    @BindView(R.id.current_password_container)
    TextInputLayout currentPasswordContainer;
    @BindView(R.id.new_password_input)
    TextInputEditText newPasswordInput;
    @BindView(R.id.new_password_container)
    TextInputLayout newPasswordContainer;
    @BindView(R.id.confirm_password_input)
    TextInputEditText confirmPasswordInput;
    @BindView(R.id.confirm_password_container)
    TextInputLayout confirmPasswordContainer;
    @BindView(R.id.button_submit)
    MaterialButton buttonSubmit;
    @BindView(R.id.layout_container)
    LinearLayout layoutContainer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.loader)
    ContentLoadingProgressBar loader;
    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.main_container)
    CoordinatorLayout mainContainer;
    private Tracker mTracker;
    Boolean isCustomer;

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @OnClick(R.id.back)
    public void setBack() {
        onBackPressed();
    }

    @OnClick(R.id.button_submit)
    public void onSubmitClicked() {
        if (currentPasswordInput.getText() == null) {
            currentPasswordContainer.setError(getResources().getString(R.string.current_password_empty));
        } else if (currentPasswordInput.getText().toString().trim().length() == 0) {
            currentPasswordContainer.setError(getResources().getString(R.string.current_password_empty));
        } else if (newPasswordInput.getText() == null) {
            newPasswordContainer.setError(getResources().getString(R.string.new_password_empty));
        } else if (newPasswordInput.getText().toString().trim().length() == 0) {
            newPasswordContainer.setError(getResources().getString(R.string.new_password_empty));
        } else if (confirmPasswordInput.getText() == null) {
            confirmPasswordContainer.setError(getResources().getString(R.string.confirm_password_empty));
        } else if (confirmPasswordInput.getText().toString().trim().length() == 0) {
            confirmPasswordContainer.setError(getResources().getString(R.string.confirm_password_empty));
        } else if (!confirmPasswordInput.getText().toString().equals(newPasswordInput.getText().toString())) {
            confirmPasswordContainer.setError(getResources().getString(R.string.confirm_password_not_valid));
        } else {
            changePassword(currentPasswordInput.getText().toString(), confirmPasswordInput.getText().toString());
            currentPasswordInput.clearFocus();
            newPasswordInput.clearFocus();
            confirmPasswordInput.clearFocus();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        mTracker = Essentials.getDefaultTracker();
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Screen - " + "Change Password");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
//        showKeyboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        hideKeyboard();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void init() {

        isCustomer = getIntent().getBooleanExtra("isCustomer", false);
        loader.setVisibility(View.GONE);
        handler = new Handler();

        currentPasswordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                currentPasswordContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        newPasswordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                newPasswordContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        confirmPasswordInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                confirmPasswordContainer.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private void changePassword(String currentPassword, String confirmPassword) {
        if (AppUtil.isInternetAvailable(ChangePassword.this)) {
            loader.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", Prefs.getPrefInstance().getValue(ChangePassword.this, Const.USER_ID, ""));
                jsonObject.put("current_password", currentPassword);
                jsonObject.put("new_password", confirmPassword);
                jsonObject.put("confirm_password", confirmPassword);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String params = jsonObject.toString();
            final RequestBody change_password = RequestBody.create(params, MediaType.parse("application/json"));
            APIUtils.getAPIService().change_password(change_password).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            loader.setVisibility(View.GONE);
                            currentPasswordInput.setText("");
                            currentPasswordInput.clearFocus();
                            newPasswordInput.setText("");
                            newPasswordInput.clearFocus();
                            confirmPasswordInput.setText("");
                            confirmPasswordInput.clearFocus();
                            View dialog_view = LayoutInflater.from(ChangePassword.this).inflate(AppUtil.setLanguage(ChangePassword.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(ChangePassword.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(ChangePassword.this.getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                                dialog.dismiss();
                                startActivity(new Intent(ChangePassword.this, Login.class).putExtra("isCustomer", isCustomer).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            });
                        } else {
                            loader.setVisibility(View.GONE);
                            View dialog_view = LayoutInflater.from(ChangePassword.this).inflate(AppUtil.setLanguage(ChangePassword.this, R.layout.simple_dialog_text_button), null);
                            final AlertDialog dialog = new AlertDialog.Builder(ChangePassword.this)
                                    .setCancelable(false)
                                    .setView(dialog_view)
                                    .show();

                            if (dialog.getWindow() != null)
                                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(response.body().getMessage()));
                            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(ChangePassword.this.getResources().getString(R.string.ok));
                            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        View dialog_view = LayoutInflater.from(ChangePassword.this).inflate(AppUtil.setLanguage(ChangePassword.this, R.layout.simple_dialog_text_button), null);
                        final AlertDialog dialog = new AlertDialog.Builder(ChangePassword.this)
                                .setCancelable(false)
                                .setView(dialog_view)
                                .show();

                        if (dialog.getWindow() != null)
                            dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                        if (response.errorBody() != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(Html.fromHtml(jsonObject.getString("message")));
                            } catch (JSONException | IOException e) {
                                ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(ChangePassword.this.getResources().getString(R.string.something_went_wrong));
                                e.printStackTrace();
                            }
                        } else {
                            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(ChangePassword.this.getResources().getString(R.string.something_went_wrong));
                        }
                        (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                        ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(ChangePassword.this.getResources().getString(R.string.ok));
                        dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    loader.setVisibility(View.GONE);
                    View dialog_view = LayoutInflater.from(ChangePassword.this).inflate(AppUtil.setLanguage(ChangePassword.this, R.layout.simple_dialog_text_button), null);
                    final AlertDialog dialog = new AlertDialog.Builder(ChangePassword.this)
                            .setCancelable(false)
                            .setView(dialog_view)
                            .show();

                    if (dialog.getWindow() != null)
                        dialog.getWindow().getDecorView().getBackground().setAlpha(0);

                    ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(ChangePassword.this.getResources().getString(R.string.something_went_wrong));
                    (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
                    ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(ChangePassword.this.getResources().getString(R.string.ok));
                    dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
                }
            });
        } else {
            loader.setVisibility(View.GONE);
            View dialog_view = LayoutInflater.from(ChangePassword.this).inflate(AppUtil.setLanguage(ChangePassword.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(ChangePassword.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(ChangePassword.this.getResources().getString(R.string.no_internet_connection));
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(ChangePassword.this.getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> dialog.dismiss());
        }
    }

}
