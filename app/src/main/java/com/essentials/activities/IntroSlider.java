package com.essentials.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;

import com.essentials.R;
import com.essentials.adapters.IntroSliderAdapter;
import com.essentials.data.models.intro_slider.IntroSliderResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.essentials.util.AppUtil.setWindowFlag;

public class IntroSlider extends AppCompatActivity {

    private static String TAG = "TAG --> " + IntroSlider.class.getSimpleName();

    @BindView(R.id.slider)
    ViewPager slider;
    @BindView(R.id.indicator)
    CircleIndicator indicator;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    void setWatchNow() {
        Prefs.getPrefInstance().setValue(IntroSlider.this, Const.APP_STATUS, "1");
        startActivity(new Intent(IntroSlider.this, RegisterOptions.class).putExtra("isLogin", true));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(AppUtil.setLanguage(this, R.layout.activity_intro_slider));
        ButterKnife.bind(this);
        getIntroSlider();

        // Make Full Screen - Hide StatusBar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Make UI Full Screen
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        // Set StatusBar Color Transparent
        setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ViewCompat.setOnApplyWindowInsetsListener(getWindow().getDecorView(), (view, insets) ->
                ViewCompat.onApplyWindowInsets(getWindow().getDecorView(),
                        insets.replaceSystemWindowInsets(insets.getSystemWindowInsetLeft(), 0,
                                insets.getSystemWindowInsetRight(), insets.getSystemWindowInsetBottom()))
        );
//        mTracker.setScreenName("Screen Name ~ " + "Introductory Slider");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void getIntroSlider() {
        if (AppUtil.isInternetAvailable(this)) {
            APIUtils.getAPIService().get_intro_slider(getResources().getString(R.string.introduction)).enqueue(new Callback<IntroSliderResponse>() {
                @Override
                public void onResponse(@NonNull Call<IntroSliderResponse> call, @NonNull Response<IntroSliderResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getStatus() != null && response.body().getStatus().equals(200)) {
                            if(response.body().getData() != null && !response.body().getData().isEmpty()) {
                                slider.setAdapter(new IntroSliderAdapter(IntroSlider.this, response.body().getData(), slider));
                                indicator.setViewPager(slider);
                            } else {
                                setWatchNow();
                            }
                        } else {
                            setWatchNow();
                        }
                    } else {
                        setWatchNow();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<IntroSliderResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    setWatchNow();
                }
            });
        } else {
            View dialog_view = LayoutInflater.from(IntroSlider.this).inflate(AppUtil.setLanguage(IntroSlider.this, R.layout.simple_dialog_text_button), null);
            final AlertDialog dialog = new AlertDialog.Builder(IntroSlider.this)
                    .setCancelable(false)
                    .setView(dialog_view)
                    .show();

            if (dialog.getWindow() != null)
                dialog.getWindow().getDecorView().getBackground().setAlpha(0);

            ((TextView) dialog_view.findViewById(R.id.dialog_text)).setText(R.string.no_internet_connection);
            (dialog_view.findViewById(R.id.dialog_ok)).setVisibility(View.GONE);
            ((Button) dialog_view.findViewById(R.id.dialog_cancel)).setText(getResources().getString(R.string.ok));
            dialog_view.findViewById(R.id.dialog_cancel).setOnClickListener(view -> {
                dialog.dismiss();
            });
        }
    }
}
