package com.essentials.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.essentials.R;
import com.essentials.data.models.asset.AssetRateAndReview;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {


    ArrayList<AssetRateAndReview> data;
    private Context context;
    private FragmentManager fragmentManager;

    public ReviewAdapter(ArrayList<AssetRateAndReview> data, Context context, FragmentManager fragmentManager) {
        this.context = context;
        this.data = data;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_reviews_listing, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.comment.setText(data.get(position).getReview());
        holder.reviewsAssetRating.setRating(data.get(position).getRate());
        holder.name.setText(data.get(position).getName());
        holder.reviewDate.setText(data.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.reviews_asset_rating)
        RatingBar reviewsAssetRating;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.comment)
        TextView comment;
        @BindView(R.id.review_date)
        TextView reviewDate;
        @BindView(R.id.reviews_container)
        LinearLayout reviewsContainer;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
