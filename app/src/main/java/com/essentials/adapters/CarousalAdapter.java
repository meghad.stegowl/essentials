package com.essentials.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.essentials.data.models.intro_slider.IntroSliderData;
import com.essentials.R;
import com.essentials.application.Essentials;
import com.essentials.util.AppUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class CarousalAdapter extends RecyclerView.Adapter<CarousalAdapter.ViewHolder> {

    Context context;
    ArrayList<IntroSliderData> content;
    FragmentManager fragmentManager;


    public CarousalAdapter(Context context, ArrayList<IntroSliderData> content, FragmentManager fragmentManager) {
        this.context = context;
        this.content = content;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(AppUtil.setLanguage(context, R.layout.row_carousal), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Glide
                .with(Essentials.applicationContext)
                .load(content.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .thumbnail(0.25f)
                .transition(withCrossFade())
                .error(R.drawable.logo)
                .into(holder.assetImage);

    }

    @Override
    public int getItemCount() {
        return content.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.asset_image)
        ImageView assetImage;
//        @BindView(R.id.asset_overflow)
//        ImageView assetOverflow;
        @BindView(R.id.container)
        RelativeLayout container;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
