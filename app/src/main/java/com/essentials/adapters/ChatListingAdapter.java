package com.essentials.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.activities.Chat;
import com.essentials.activities.HomeScreen;
import com.essentials.activities.Search;
import com.essentials.application.Essentials;
import com.essentials.data.models.chat.ChatListingData;
import com.essentials.fragments.SearchFragment;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatListingAdapter extends RecyclerView.Adapter<ChatListingAdapter.ViewHolder> {


    ArrayList<ChatListingData> data;
    private Context context;
    private Boolean isCustomer;
    private FragmentManager fragmentManager;

    public ChatListingAdapter(ArrayList<ChatListingData> data, Context context, FragmentManager fragmentManager,Boolean isCustomer) {
        this.context = context;
        this.data = data;
        this.isCustomer = isCustomer;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_chat_listing, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String fullName = data.get(position).getFirstName() + " " + data.get(position).getLastName();
        holder.name.setText(fullName);
        holder.messageCount.setVisibility(data.get(position).getReadStatus() == 1 ? View.GONE : View.VISIBLE);
        holder.message.setText(data.get(position).getLastMsg());
        holder.time.setText(data.get(position).getDisplayTime());
        if (data.get(position).getImage() != null && !data.get(position).getImage().isEmpty()) {
            holder.nameImage.setVisibility(View.GONE);
            holder.userImage.setVisibility(View.VISIBLE);
            Glide
                    .with(Essentials.applicationContext)
                    .load(data.get(position).getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                    .thumbnail(0.25f)
                    .error(R.drawable.ic_profile_icon)
                    .into(holder.userImage);
        } else {
            holder.nameImage.setVisibility(View.VISIBLE);
            holder.userImage.setVisibility(View.GONE);
            String nameInitials = data.get(position).getFirstName().substring(0,1) + data.get(position).getLastName().substring(0,1);
            holder.nameImage.setText(nameInitials);
        }

        holder.mainContainer.setOnClickListener(v -> {
            Log.d("OkHttpClient","here is----------------------"+data.get(position).getUserId());
            context.startActivity(new Intent(context, Chat.class).putExtra("isCustomer", isCustomer)
                    .putExtra("path", data.get(position).getUserId())
                    .putExtra("title", fullName));
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_image)
        TextView nameImage;
        @BindView(R.id.user_image)
        CircularImageView userImage;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.message_count)
        TextView messageCount;
        @BindView(R.id.asset_container)
        LinearLayout assetContainer;
        @BindView(R.id.main_container)
        CardView mainContainer;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
