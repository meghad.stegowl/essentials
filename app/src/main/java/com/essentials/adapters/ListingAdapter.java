package com.essentials.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.activities.AssetDetail;
import com.essentials.activities.AssetListing;
import com.essentials.activities.Chat;
import com.essentials.application.Essentials;
import com.essentials.data.models.asset.AssetListingData;
import com.essentials.data.models.common.CommonResponse;
import com.essentials.data.remote.APIUtils;
import com.essentials.fragments.AppointmentHistoryFragment;
import com.essentials.util.AppUtil;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.material.button.MaterialButton;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListingAdapter extends RecyclerView.Adapter<ListingAdapter.ViewHolder> {

    private Context context;
    private FragmentManager fragmentManager;
    private ArrayList<AssetListingData> data;
    private Boolean isCustomer, isHistory;

    public ListingAdapter(ArrayList<AssetListingData> data, Context context, FragmentManager fragmentManager, Boolean isCustomer, Boolean isHistory) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.data = data;
        this.isCustomer = isCustomer;
        this.isHistory = isHistory;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_listing, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String assetName;
        String time;

        if (isHistory) {
            if (isCustomer) {
                assetName = data.get(position).getProviderName();
                if (!data.get(position).getRateandreviewsStatus() && data.get(position).getAppointmentStatus().equals("2"))
                    holder.buttonRateReview.setVisibility(View.VISIBLE);
                else holder.buttonRateReview.setVisibility(View.GONE);
            } else {
                assetName = data.get(position).getCustomerName();
                holder.buttonRateReview.setVisibility(View.VISIBLE);

                if (data.get(position).getAppointmentStatus().equals("0") && data.get(position).getAcceptStatus().equals("0")) {
                    holder.buttonAcceptAppointment.setVisibility(View.VISIBLE);
                    holder.buttonRateReview.setVisibility(View.GONE);
                }
                else if(data.get(position).getAppointmentStatus().equals("0") && data.get(position).getAcceptStatus().equals("1"))
                    holder.buttonRateReview.setText(context.getResources().getString(R.string.start_appointment));
                else if (data.get(position).getAppointmentStatus().equals("1"))
                    holder.buttonRateReview.setText(context.getResources().getString(R.string.end_appointment));
                else holder.buttonRateReview.setVisibility(View.GONE);
            }
            holder.distance.setVisibility(View.GONE);
            holder.chatCallingContainer.setVisibility(View.VISIBLE);
            holder.buttonCall.setOnClickListener(v -> {
                AppointmentHistoryFragment.getInstance().setCall(data.get(position).getPhone());
            });
            if (data.get(position).getAppointmentStatus().equals("0"))
                holder.buttonMoreInfo.setVisibility(View.VISIBLE);
            else holder.buttonMoreInfo.setVisibility(View.GONE);
            holder.buttonMoreInfo.setText(context.getResources().getString(R.string.cancel_appointment));
            holder.appointmentDate.setVisibility(View.VISIBLE);
            holder.appointmentDay.setVisibility(View.VISIBLE);
            holder.appointmentTime.setVisibility(View.VISIBLE);
            holder.appointmentDateInput.setText(data.get(position).getDate());
            holder.appointmentDayInput.setText(data.get(position).getDay());
            if (data.get(position).getEndTime() != null && !data.get(position).getEndTime().isEmpty())
                time = data.get(position).getStartTime() + " - " + data.get(position).getEndTime();
            else time = data.get(position).getStartTime();
            holder.appointmentTimeInput.setText(time);
            holder.assetRating.setRating(data.get(position).getRate());
            holder.profileImage.setVisibility(View.GONE);
            holder.assetRatingCount.setVisibility(View.GONE);
        }
        else {
            if (!isCustomer) {
                if (Prefs.getPrefInstance().getValue(context, Const.USER_ID, "").equals(data.get(position).getUserId())) {
                    holder.distance.setVisibility(View.GONE);
                } else {
                    String distanceMiles = data.get(position).getDistance() + " " + "mi";
                    if (Prefs.getPrefInstance().getValue(context, Const.LOGIN_ACCESS, "").equals(context.getResources().getString(R.string.logged_in))) {
                        if (data.get(position).getDistance() != null && !data.get(position).getDistance().isEmpty()) {
                            holder.distance.setText(distanceMiles);
                            holder.distance.setVisibility(View.VISIBLE);
                        } else holder.distance.setVisibility(View.GONE);
                    } else holder.distance.setVisibility(View.GONE);
                }
                holder.chatCallingContainer.setVisibility(View.GONE);
                holder.buttonMoreInfo.setText(context.getResources().getString(R.string.more_info));
                holder.buttonRateReview.setVisibility(View.GONE);
                holder.appointmentDate.setVisibility(View.GONE);
                holder.appointmentDay.setVisibility(View.GONE);
                holder.appointmentTime.setVisibility(View.GONE);
                assetName = data.get(position).getFirstname() + " " + data.get(position).getLastname();
                holder.assetRating.setRating(data.get(position).getRateAverage());
                String totalRating = "(" + data.get(position).getTotalRateandreview() + ")";
                if (data.get(position).getTotalRateandreview() != null && !data.get(position).getTotalRateandreview().isEmpty() && !data.get(position).getTotalRateandreview().equals("0")) {
                    holder.assetRatingCount.setVisibility(View.VISIBLE);
                    holder.assetRatingCount.setText(totalRating);
                    holder.assetRating.setVisibility(View.VISIBLE);
                } else {
                    holder.assetRatingCount.setVisibility(View.GONE);
                    holder.assetRating.setVisibility(View.GONE);
                }
            } else {
                holder.distance.setVisibility(View.VISIBLE);
                holder.chatCallingContainer.setVisibility(View.GONE);
                holder.buttonMoreInfo.setText(context.getResources().getString(R.string.more_info));
                holder.buttonRateReview.setVisibility(View.GONE);
                holder.appointmentDate.setVisibility(View.GONE);
                holder.appointmentDay.setVisibility(View.GONE);
                holder.appointmentTime.setVisibility(View.GONE);
                assetName = data.get(position).getFirstname() + " " + data.get(position).getLastname();
                holder.assetRating.setRating(data.get(position).getRateAverage());
                String totalRating = "(" + data.get(position).getTotalRateandreview() + ")";
                if (data.get(position).getTotalRateandreview() != null && !data.get(position).getTotalRateandreview().isEmpty() && !data.get(position).getTotalRateandreview().equals("0")) {
                    holder.assetRatingCount.setVisibility(View.VISIBLE);
                    holder.assetRatingCount.setText(totalRating);
                    holder.assetRating.setVisibility(View.VISIBLE);
                } else {
                    holder.assetRatingCount.setVisibility(View.GONE);
                    holder.assetRating.setVisibility(View.GONE);
                }
                String distanceMiles = data.get(position).getDistance() + " " + "mi";
                if (Prefs.getPrefInstance().getValue(context, Const.LOGIN_ACCESS, "").equals(context.getResources().getString(R.string.logged_in))) {
                    if (data.get(position).getDistance() != null && !data.get(position).getDistance().isEmpty()) {
                        holder.distance.setText(distanceMiles);
                        holder.distance.setVisibility(View.VISIBLE);
                    } else holder.distance.setVisibility(View.GONE);
                } else holder.distance.setVisibility(View.GONE);
            }
        }
        holder.name.setText(assetName);
        if(data.get(position).getTitle() != null && data.get(position).getTitle().isEmpty()){
            holder.titleName.setVisibility(View.GONE);
        }else{
            holder.titleName.setText(data.get(position).getTitle());
        }
        if(data.get(position).getLang() != null && data.get(position).getLang().isEmpty()){
            holder.languageName.setVisibility(View.GONE);
        }else{
            holder.languageName.setText(data.get(position).getLang());
        }
        if (data.get(position).getHealthcarecategory() != null && !data.get(position).getHealthcarecategory().isEmpty()) {
            holder.businessName.setVisibility(View.VISIBLE);
            holder.businessName.setText(data.get(position).getHealthcarecategory());
        } else holder.businessName.setVisibility(View.GONE);
        holder.address.setText(data.get(position).getAddress());

        Glide
                .with(Essentials.applicationContext)
                .load(data.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                .thumbnail(0.25f)
                .error(R.drawable.ic_profile_icon)
                .into(holder.profileImage);

        holder.buttonMoreInfo.setOnClickListener(v -> {
            if (isHistory) {
                context.startActivity(new Intent(context, AssetListing.class).putExtra("history_id", data.get(position).getHistoryId())
                        .putExtra("isCustomer", isCustomer)
                        .putExtra("isHistory", isHistory)
                        .putExtra("isCancelAppointment", true)
                        .putExtra("isRateReview", false)
                        .putExtra("isStartAppointment", false)
                        .putExtra("provider_id", data.get(position).getProviderId()));

            } else
                context.startActivity(new Intent(context, AssetDetail.class).putExtra("path", data.get(position).getUserId())
                        .putExtra("isCustomer", isCustomer));
        });

        holder.buttonRateReview.setOnClickListener(v -> {
            if (isCustomer)
                context.startActivity(new Intent(context, AssetListing.class).putExtra("history_id", data.get(position).getHistoryId())
                        .putExtra("isCustomer", isCustomer)
                        .putExtra("isHistory", isHistory)
                        .putExtra("isRateReview", true)
                        .putExtra("isCancelAppointment", false)
                        .putExtra("isStartAppointment", false)
                        .putExtra("isAssetListing", false)
                        .putExtra("provider_id", data.get(position).getProviderId()));
            else {
                if (data.get(position).getAppointmentStatus().equals("0"))
                    context.startActivity(new Intent(context, AssetListing.class).putExtra("history_id", data.get(position).getHistoryId())
                            .putExtra("isCustomer", isCustomer)
                            .putExtra("isHistory", isHistory)
                            .putExtra("isCancelAppointment", false)
                            .putExtra("isRateReview", false)
                            .putExtra("isAssetListing", false)
                            .putExtra("isStartAppointment", true)
                            .putExtra("type", "start")
                            .putExtra("provider_id", data.get(position).getProviderId()));
                else if (data.get(position).getAppointmentStatus().equals("1"))
                    context.startActivity(new Intent(context, AssetListing.class).putExtra("history_id", data.get(position).getHistoryId())
                            .putExtra("isCustomer", isCustomer)
                            .putExtra("isHistory", isHistory)
                            .putExtra("isCancelAppointment", false)
                            .putExtra("isRateReview", false)
                            .putExtra("isAssetListing", false)
                            .putExtra("isStartAppointment", true)
                            .putExtra("type", "end")
                            .putExtra("provider_id", data.get(position).getProviderId()));
            }
        });
        holder.chat.setOnClickListener(v -> {
            Log.d("OkHttpClient","user id is-------------------"+Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""));

            if(Prefs.getPrefInstance().getValue(context, Const.USER_ID, "").equals(data.get(position).getProviderId())){
                context.startActivity(new Intent(context, Chat.class).putExtra("isCustomer", isCustomer).
                        putExtra("path", data.get(position).getCustomerId()));
            }else {
                context.startActivity(new Intent(context, Chat.class).putExtra("isCustomer", isCustomer).
                        putExtra("path", data.get(position).getProviderId()));
            }
        });

        holder.buttonAcceptAppointment.setOnClickListener(v -> {
            AppointmentHistoryFragment.getInstance().getAcceptAppointment(data.get(position).getHistoryId(), data.get(position).getProviderId());
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_image)
        CircularImageView profileImage;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.distance)
        TextView distance;
        @BindView(R.id.business_name)
        TextView businessName;
        @BindView(R.id.title_name)
        TextView titleName;
        @BindView(R.id.language_name)
        TextView languageName;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.appointment_date_input)
        TextView appointmentDateInput;
        @BindView(R.id.appointment_date)
        LinearLayout appointmentDate;
        @BindView(R.id.appointment_day_input)
        TextView appointmentDayInput;
        @BindView(R.id.appointment_day)
        LinearLayout appointmentDay;
        @BindView(R.id.appointment_time_input)
        TextView appointmentTimeInput;
        @BindView(R.id.appointment_time)
        LinearLayout appointmentTime;
        @BindView(R.id.asset_rating)
        RatingBar assetRating;
        @BindView(R.id.asset_rating_count)
        TextView assetRatingCount;
        @BindView(R.id.button_call)
        ImageView buttonCall;
        @BindView(R.id.chat)
        ImageView chat;
        @BindView(R.id.chat_calling_container)
        LinearLayout chatCallingContainer;
        @BindView(R.id.button_rate_review)
        MaterialButton buttonRateReview;
        @BindView(R.id.button_more_info)
        MaterialButton buttonMoreInfo;
        @BindView(R.id.main_container)
        CardView mainContainer;
        @BindView(R.id.button_accept_appointment)
        MaterialButton buttonAcceptAppointment;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
