package com.essentials.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.essentials.data.models.notifications.NotificationsData;
import com.essentials.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {



    private Context context;
    private ArrayList<NotificationsData> data;
    private FragmentManager fragmentManager;


    public NotificationsAdapter(Context context, ArrayList<NotificationsData> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_notifications, parent, false), viewType);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.notificationMessage.setText(data.get(position).getMessage());
        holder.notificationTitle.setText(data.get(position).getTitle());
        holder.notificationTime.setText(data.get(position).getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.notification_title)
        TextView notificationTitle;
        @BindView(R.id.asset_container)
        LinearLayout assetContainer;
        @BindView(R.id.main_container)
        CardView mainContainer;
        @BindView(R.id.notification_time)
        TextView notificationTime;
        @BindView(R.id.notification_message)
        TextView notificationMessage;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
