package com.essentials.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.activities.AssetDetail;
import com.essentials.activities.AssetListing;
import com.essentials.activities.Chat;
import com.essentials.application.Essentials;
import com.essentials.data.models.asset.AssetListingData;
import com.essentials.data.models.category.CategoryResponseData;
import com.essentials.fragments.AppointmentHistoryFragment;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.material.button.MaterialButton;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryListingAdapter extends RecyclerView.Adapter<CategoryListingAdapter.ViewHolder> {

    private Context context;
    private FragmentManager fragmentManager;
    private ArrayList<CategoryResponseData> data;
    private Boolean isCustomer, isHistory;

    public CategoryListingAdapter(ArrayList<CategoryResponseData> data, Context context, FragmentManager fragmentManager, Boolean isCustomer, Boolean isHistory) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.data = data;
        this.isCustomer = isCustomer;
        this.isHistory = isHistory;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_category_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.title.setText(data.get(position).getType());
        String healthcarecategory_id = String.valueOf(data.get(position).getHealthcarecategoryId());

        holder.main_container.setOnClickListener(v -> {
//                context.startActivity(new Intent(context, AssetListing.class).putExtra("history_id", data.get(position).getHistoryId())
//                        .putExtra("isCustomer", isCustomer)
//                        .putExtra("isHistory", isHistory)
//                        .putExtra("isCancelAppointment", true)
//                        .putExtra("isRateReview", false)
//                        .putExtra("isStartAppointment", false)
//                        .putExtra("provider_id", data.get(position).getProviderId()));
            Log.d("mytag", "here is healthcarecategory_id is--------------------" + healthcarecategory_id);
            context.startActivity(new Intent(context, AssetListing.class)
                    .putExtra("isCustomer", isCustomer)
                    .putExtra("healthcarecategory_id", healthcarecategory_id)
                    .putExtra("isRateReview", false)
                    .putExtra("isCancelAppointment", false)
                    .putExtra("isStartAppointment", false)
                    .putExtra("isAssetListing", true));
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.main_container)
        RelativeLayout main_container;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
