package com.essentials.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.essentials.R;
import com.essentials.activities.AssetDetail;
import com.essentials.activities.Search;
import com.essentials.data.models.search.RecentSearchData;
import com.essentials.fragments.SearchFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecentSearchAdapter extends RecyclerView.Adapter<RecentSearchAdapter.ViewHolder> {


    ArrayList<RecentSearchData> data;
    private Context context;
    private FragmentManager fragmentManager;
    private Boolean isCustomer;
    private String type;

    public RecentSearchAdapter(Context context, ArrayList<RecentSearchData> data, FragmentManager fragmentManager, Boolean isCustomer, String type) {
        this.context = context;
        this.data = data;
        this.type = type;
        this.isCustomer = isCustomer;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (type.toLowerCase().equals(context.getResources().getString(R.string.type_listing).toLowerCase()))
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_filter_search, parent, false), viewType);
        else
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_search, parent, false), viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (type.toLowerCase().equals(context.getResources().getString(R.string.type_suggestions).toLowerCase()) || type.toLowerCase().equals(context.getResources().getString(R.string.type_listing).toLowerCase()))
            holder.title.setText(data.get(position).getType());
        else holder.title.setText(data.get(position).getText());

        holder.mainContainer.setOnClickListener(v -> {
            SearchFragment.getInstance().setSearchResult(holder.title.getText().toString());
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void clear() {
        int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.main_container)
        CardView mainContainer;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
