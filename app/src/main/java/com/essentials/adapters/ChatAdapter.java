package com.essentials.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.essentials.R;
import com.essentials.data.models.chat.MessageHistoryData;
import com.essentials.util.Const;
import com.essentials.util.Prefs;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {


    ArrayList<MessageHistoryData> data;
    private Context context;
    private FragmentManager fragmentManager;

    public ChatAdapter(ArrayList<MessageHistoryData> data, Context context, FragmentManager fragmentManager) {
        this.context = context;
        this.data = data;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.message_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (data.get(position).getSendUserId().equals(Prefs.getPrefInstance().getValue(context, Const.USER_ID, ""))) {
            holder.nonUserItemContainer.setVisibility(View.GONE);
            holder.userItem.setText(data.get(position).getMessage());
            holder.userItemTime.setText(data.get(position).getDisplayTime());
            holder.userItemContainer.setVisibility(View.VISIBLE);
        } else {
            holder.userItemContainer.setVisibility(View.GONE);
            holder.nonUserItem.setText(data.get(position).getMessage());
            holder.nonUserItemTime.setText(data.get(position).getDisplayTime());
            holder.nonUserItemContainer.setVisibility(View.VISIBLE);

        }
        if (data.get(position).getReadStatus() !=null && data.get(position).getReadStatus().equals("1"))
            holder.seenStatus.setImageResource(R.drawable.ic_checked);
        else
            holder.seenStatus.setImageResource(R.drawable.ic_unchecked);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void add(MessageHistoryData messageData) {
        this.data.add(data.size(), messageData);
        notifyItemInserted(data.size());
    }

    public void clear() {
        int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void add(ArrayList<MessageHistoryData> messageHistory, boolean canNotify) {
        int previousDataSize = this.data.size();
        this.data.addAll(0, messageHistory);
        if (canNotify)
            notifyItemRangeInserted(0, messageHistory.size());
        else notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.non_user_item)
        TextView nonUserItem;
        @BindView(R.id.non_user_item_time)
        TextView nonUserItemTime;
        @BindView(R.id.non_user_item_container)
        LinearLayout nonUserItemContainer;
        @BindView(R.id.user_item)
        TextView userItem;
        @BindView(R.id.user_item_time)
        TextView userItemTime;
        @BindView(R.id.user_item_container)
        LinearLayout userItemContainer;
        @BindView(R.id.seen_status)
        ImageView seenStatus;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
