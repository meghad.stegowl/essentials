package com.essentials.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;
import com.essentials.R;
import com.essentials.activities.AssetDetail;
import com.essentials.application.Essentials;
import com.essentials.data.models.search.SearchData;
import com.essentials.util.Const;
import com.essentials.util.Prefs;
import com.google.android.material.button.MaterialButton;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private Context context;
    private FragmentManager fragmentManager;
    ArrayList<SearchData> data;
    private Boolean isCustomer;

    public SearchAdapter(Context context, ArrayList<SearchData> data, FragmentManager fragmentManager, Boolean isCustomer) {
        this.context = context;
        this.data = data;
        this.isCustomer = isCustomer;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_listing, parent, false), viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String assetName = data.get(position).getFirstname() + " " + data.get(position).getLastname();
        holder.name.setText(assetName);
        if (data.get(position).getHealthcarecategory() != null && !data.get(position).getHealthcarecategory().isEmpty())
//            holder.businessName.setText(data.get(position).getHealthcarecategory());
            holder.businessName.setText(data.get(position).getBusinessname());
        else holder.businessName.setVisibility(View.GONE);
        holder.address.setText(data.get(position).getAddress());
        holder.assetRating.setRating(data.get(position).getRateAverage());
        if (data.get(position).getTotalRateandreview() != null && !data.get(position).getTotalRateandreview().isEmpty() && !data.get(position).getTotalRateandreview().equals("0")) {
            holder.assetRatingCount.setVisibility(View.VISIBLE);
            holder.assetRating.setVisibility(View.VISIBLE);
        } else {
            holder.assetRatingCount.setVisibility(View.GONE);
            holder.assetRating.setVisibility(View.GONE);
        }
        String totalRating = "(" + data.get(position).getTotalRateandreview() + ")";
        holder.assetRatingCount.setText(totalRating);
        String distanceMiles = data.get(position).getDistance() + " " + "mi";
        if (Prefs.getPrefInstance().getValue(context, Const.LOGIN_ACCESS, "").equals(context.getResources().getString(R.string.logged_in))) {
            if (data.get(position).getDistance() != null && !data.get(position).getDistance().isEmpty()) {
                holder.distance.setText(distanceMiles);
                holder.distance.setVisibility(View.VISIBLE);
            }
            else holder.distance.setVisibility(View.GONE);
        }else holder.distance.setVisibility(View.GONE);

        Glide
                .with(Essentials.applicationContext)
                .load(data.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .signature(new ObjectKey(Prefs.getPrefInstance().getValue(context, Const.TIME, "")))
                .thumbnail(0.25f)
                .error(R.drawable.ic_profile_icon)
                .into(holder.profileImage);

        holder.buttonMoreInfo.setOnClickListener(v -> {
            context.startActivity(new Intent(context, AssetDetail.class).putExtra("path", data.get(position).getUserId()).putExtra("isCustomer", isCustomer));
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_image)
        CircularImageView profileImage;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.distance)
        TextView distance;
        @BindView(R.id.business_name)
        TextView businessName;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.asset_rating)
        RatingBar assetRating;
        @BindView(R.id.asset_rating_count)
        TextView assetRatingCount;
        @BindView(R.id.button_more_info)
        MaterialButton buttonMoreInfo;
        @BindView(R.id.main_container)
        CardView mainContainer;

        ViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
